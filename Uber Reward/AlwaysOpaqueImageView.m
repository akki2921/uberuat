//
//  AlwaysOpaqueImageView.m
//  
//
//  Created by Arvind on 04/04/18.
//

#import "AlwaysOpaqueImageView.h"

@implementation AlwaysOpaqueImageView
- (void)setAlpha:(CGFloat)alpha {
    [super setAlpha:1.0];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
