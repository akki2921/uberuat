//
//  AppDelegate.h
//  Uber Reward
//
//  Created by Arvind Seth on 22/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>
#import <Google/Analytics.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate,UIDocumentInteractionControllerDelegate,UNUserNotificationCenterDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic)  NSString * ViewWebTypeStr;

@end

