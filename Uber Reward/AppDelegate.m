//
//  AppDelegate.m
//  Uber Reward
//
//  Created by Arvind Seth on 22/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import "AppDelegate.h"
#import "UtillClass.h"
#import "HomeViewController.h"
#import "NotiViewController.h"
#import "PrivilageViewController.h"
#import "LoginViewController.h"
#import "SplashbeforeRegViewController.h"
@interface AppDelegate ()

@end

@implementation AppDelegate
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)


@synthesize ViewWebTypeStr;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    

    UIApplication *app = [UIApplication sharedApplication];
    NSArray *subviews = [[[app valueForKey:@"statusBar"] valueForKey:@"foregroundView"] subviews];
    NSString *dataNetworkItemView = nil;
    NSString *signalStrengthView = nil;
    
    for (id subview in subviews) {
        NSLog(@"Class - %@", NSStringFromClass([subview class]));
        
        if([subview isKindOfClass:[NSClassFromString(@"UIStatusBarSignalStrengthItemView") class]]) {
            signalStrengthView = subview;
        }
        
        if([subview isKindOfClass:[NSClassFromString(@"UIStatusBarDataNetworkItemView") class]]) {
            dataNetworkItemView = subview;
        }
    }
    
    int signalStrength = [[signalStrengthView valueForKey:@"signalStrengthBars"] intValue];
    NSLog(@"signal %d", signalStrength);
    
    int wifiStrength = [[dataNetworkItemView valueForKey:@"wifiStrengthRaw"] intValue];
    NSLog(@"wifi %d", wifiStrength);
    
    
    
  
    GAI *gai = [GAI sharedInstance];
    [gai trackerWithTrackingId:@"UA-118120130-1"];
    
    // Optional: automatically report uncaught exceptions.
    gai.trackUncaughtExceptions = YES;
    
    // Optional: set Logger to VERBOSE for debug information.
    // Remove before app release.
    gai.logger.logLevel = kGAILogLevelVerbose;
    
    
  if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"Userloginornot"] isEqualToString:@"Loggedin"]) {
      self.window = [[UIWindow alloc]initWithFrame:[[UIScreen mainScreen] bounds]];
        HomeViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
      self.window.rootViewController =vc;
      
      [self.window makeKeyAndVisible];

    }
    
    if (@available(iOS 10.0, *)) {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error)
         {
             if( !error )
             {
                 [[UIApplication sharedApplication] registerForRemoteNotifications];
                 // required to get the app to do anything at all about push notifications
                 NSLog( @"Push registration success." );
             }
             else
             {
                 NSLog( @"Push registration FAILED" );
                 NSLog( @"ERROR: %@ - %@", error.localizedFailureReason, error.localizedDescription );
                 NSLog( @"SUGGESTIONS: %@ - %@", error.localizedRecoveryOptions, error.localizedRecoverySuggestion );
             }
         }];
    }else{
        
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound |    UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
        if( optind != nil )
        {
            NSLog( @"registerForPushWithOptions:" );
        }
        
    }
    
    
    NSDictionary *remoteNotif = [launchOptions objectForKey: UIApplicationLaunchOptionsRemoteNotificationKey];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    if (remoteNotif)
    {
        [self application:application didReceiveRemoteNotification:remoteNotif];
        return YES;
    }
    
    // Override point for customization after application launch.
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center
      willPresentNotification:(UNNotification *)notification
        withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler NS_AVAILABLE_IOS(10_0);
{
    completionHandler(UNNotificationPresentationOptionAlert);
    
    
    
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)(void))completionHandler NS_AVAILABLE_IOS(10_0);
{
    NSLog( @"Handle push from background or closed" );
    // if you set a member variable in didReceiveRemoteNotification, you  will know if this is from closed or background
    NSLog(@"%@", response.notification.request.content.body);
    //UNNotificationContent *c= response
    
    NSInteger value = [UIApplication sharedApplication].applicationIconBadgeNumber;
    value--;
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    NSDictionary *dict= response.notification.request.content.userInfo;
    
    NSString*str= [NSString stringWithFormat:@"%@",[dict objectForKey:@"type"]];
    if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"Userloginornot"] isEqualToString:@"Loggedin"]) {
        
        self.window = [[UIWindow alloc]initWithFrame:[[UIScreen mainScreen] bounds]];
        LoginViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"LoginViewController"];
        self.window.rootViewController =vc;
        
        [self.window makeKeyAndVisible];
             return;
        
       
    }
    if([str isEqualToString:@"1"]){
        
        UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"NotiViewController"];
       self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        self.window.rootViewController = vc;
        [self.window makeKeyAndVisible];
    }
    else if([str isEqualToString:@"2"]){
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController * vc = [sb instantiateViewControllerWithIdentifier:@"PreviousMsgViewController"];
        self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        self.window.rootViewController = vc;
        [self.window makeKeyAndVisible];
    }

  
}
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    [application registerForRemoteNotifications];
}
- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    
    NSString*MobileToken=[NSString stringWithFormat:@"%@",deviceToken];
    MobileToken=[MobileToken stringByReplacingOccurrencesOfString:@"<" withString:@""];
    MobileToken=[MobileToken stringByReplacingOccurrencesOfString:@">" withString:@""];
    MobileToken=[MobileToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    if ([MobileToken isEqualToString:@""]) {
        MobileToken=@"1";
    }
    [defaults setObject:MobileToken forKey:@"notificationid"];
    [defaults synchronize];
    
    
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [self application:application didReceiveRemoteNotification:userInfo fetchCompletionHandler:^(UIBackgroundFetchResult result) {
    }];
}
-(void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    if( SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO( @"10.0" ) )
    {
        NSLog( @"iOS version >= 10. Let NotificationCenter handle this one." );
        
        return;
    }
    NSLog( @"HANDLE PUSH, didReceiveRemoteNotification: %@", userInfo );
    
    @try{
    NSString*str= [NSString stringWithFormat:@"%@",[userInfo objectForKey:@"type"]];
    
    if([str isEqualToString:@"1"]){
        
        UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"NotiViewController"];
        self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        self.window.rootViewController = vc;
        [self.window makeKeyAndVisible];
    }
    else if([str isEqualToString:@"2"]){
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController * vc = [sb instantiateViewControllerWithIdentifier:@"PreviousMsgViewController"];
        self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        self.window.rootViewController = vc;
        [self.window makeKeyAndVisible];
    }
    }
    @catch(NSException *e){
        
    }
    @finally{
        
    }
    
    
    
}
- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    //NSLog((@"Failed to get token, error: %@", error);
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
   
    [defaults setObject:[NSString stringWithFormat:@"%@",[defaults objectForKey:@"notificationid"]] forKey:@"notificationid"];
    [defaults synchronize];
    
    
}

@end
