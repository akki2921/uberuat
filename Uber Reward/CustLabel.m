//
//  CustLabel.m
//  Uber Reward
//
//  Created by Arvind on 27/03/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import "CustLabel.h"

@implementation CustLabel


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
 
 UIEdgeInsets insets = {0, 5, 0, 5};
 [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
    
 }


@end
