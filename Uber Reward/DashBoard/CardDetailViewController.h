//
//  CardDetailViewController.h
//  Uber Reward
//
//  Created by Arvind Seth on 09/04/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlidingViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface CardDetailViewController : UIViewController<slideViewControllerDelegate>{
    
     SlidingViewController *slideViewMenu;
}
@property (strong, nonatomic) NSString *strPrevID;
@property (strong, nonatomic) NSString *strimgheader;
@property (strong, nonatomic) NSString *strimgprofname;
@property (strong, nonatomic) NSString *strpartnertype;
@property (strong, nonatomic) NSString *strbadgeimagename;
@property (strong, nonatomic) NSString *strprofimageurl;


@property (strong, nonatomic) IBOutlet UIView *view_image;
@property (strong, nonatomic) IBOutlet UIScrollView *scrcarddetail;
@property (strong, nonatomic) IBOutlet UILabel *lbl_6;
@property (strong, nonatomic) IBOutlet UILabel *lbl_5;
@property (strong, nonatomic) IBOutlet UILabel *lbl_4;
@property (strong, nonatomic) IBOutlet UILabel *lbl_1;
@property (strong, nonatomic) IBOutlet UILabel *lbl_2;
@property (strong, nonatomic) IBOutlet UILabel *lbl_3;
@property (strong, nonatomic) IBOutlet UIImageView *img_1;
@property (strong, nonatomic) IBOutlet UIImageView *img_prof;
@property (strong, nonatomic) IBOutlet UILabel *lbl_name;
@property (strong, nonatomic) IBOutlet UILabel *lbl_parttype;
@property (strong, nonatomic) IBOutlet UIImageView *img_badge;
@property (strong, nonatomic) IBOutlet UILabel *lbl_header_image;

@end
