//
//  CardDetailViewController.m
//  Uber Reward
//
//  Created by Arvind Seth on 09/04/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import "CardDetailViewController.h"
#import "UtillClass.h"

@interface CardDetailViewController ()

@end

@implementation CardDetailViewController
@synthesize strimgheader,strPrevID,strimgprofname,strpartnertype,strprofimageurl,strbadgeimagename;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // Do any additional setup after loading the view.
}
- (IBAction)select_sidemenu:(id)sender {
    
    
    
    if (slideViewMenu.view) {
        [self HideSlideMenu];
    } else {
        slideViewMenu = [[UtillClass sharedInstance] CallSlideMenuView:self yVal:0.0f];
    }
}
- (IBAction)select_back:(id)sender{
    [[UtillClass sharedInstance] hidebottom_items];
   
    
    
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"PrivilageViewController"];
    [self presentViewController:vc animated:false completion:nil];
    
    
}
#pragma mark - slideView Delegate

-(void) slideViewControllerDone:(SlidingViewController *)sender
{
    if (sender) {
        [self HideSlideMenu];
    }
}
-(void) HideSlideMenu
{
    [UIView animateWithDuration:0.50 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        //code with animation
        // slideViewMenu.view.frame = CGRectMake(-self.view.frame.size.width, 0.0f, self.view.frame.size.width, self.view.frame.size.height - 0.0f);
        [slideViewMenu.view removeFromSuperview];
    } completion:^(BOOL finished) {
        //code for completion
        [slideViewMenu.view removeFromSuperview];
        slideViewMenu = nil;
        
    }];
}
-(void)viewDidAppear:(BOOL)animated{
    [[UtillClass sharedInstance]HideLoader];
    [self HideSlideMenu];
    [[UtillClass sharedInstance] showbottom_items];
    [self Getcarddetail];
}
-(void)Getcarddetail{
    [self.view endEditing:YES];
    if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        };
        
    }
    else{
        [[UtillClass sharedInstance] ShowLoaderOn:self.parentViewController withtext:@"Please wait"];
        
        [[UtillClass sharedInstance] getresponsefromservice:[NSString stringWithFormat:@"Account/GetPrivilegesDetail?CultureId=%@&CustomerId=%@&altId=0&PrivilegeId=%@",[[UtillClass sharedInstance] getcultureID],[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"Login Details"]objectForKey:@"CustomerId"],strPrevID] withbearer:[[UtillClass sharedInstance] getapp_sessiontoken] response:^(NSDictionary * dict_response) {
            [[UtillClass sharedInstance] HideLoader];
            if (dict_response ==nil) {
                [[UtillClass sharedInstance] showerrortoastmsg:@"Server error, Please try after sometime."];
                
            }
            else{
                if ([[dict_response objectForKey:@"Status"] integerValue] == 1) {
                    
                    _view_image.hidden=NO;
                      _lbl_1.text=[dict_response objectForKey:@"MainHeading"];
                      _lbl_2.text=[dict_response objectForKey:@"MainContent"];
                    _lbl_3.text=[dict_response objectForKey:@"Heading1"];
                    
                    if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                         _lbl_4.text=[[dict_response objectForKey:@"Content1"] stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                    }
                    else{
                    _lbl_4.text=[[dict_response objectForKey:@"Content1"] stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
                    }
                      _lbl_5.text=[dict_response objectForKey:@"Heading2"];
                      _lbl_6.text=[dict_response objectForKey:@"Content2"];
                    
                    
                    float f1 =[self getLabelHeight:_lbl_1];
                    float f2 =[self getLabelHeight:_lbl_2];
                    float f3 =[self getLabelHeight:_lbl_3];
                    float f4 =[self getLabelHeight:_lbl_4];
                    float f5 =[self getLabelHeight:_lbl_5];
                    float f6 =[self getLabelHeight:_lbl_6];
                    
                    
                    [_lbl_1 setFrame:CGRectMake(_lbl_1.frame.origin.x, _lbl_1.frame.origin.y, _lbl_1.frame.size.width, f1)];
                    
                     [_view_image setFrame:CGRectMake(_view_image.frame.origin.x, _lbl_1.frame.origin.y+f1+20, _view_image.frame.size.width, _view_image.frame.size.height)];
                    
                     [_lbl_2 setFrame:CGRectMake(_lbl_2.frame.origin.x, _view_image.frame.origin.y+_view_image.frame.size.height+20, _lbl_2.frame.size.width, f2)];
                    
                    if (!_lbl_3.text.length) {
                        [_lbl_3 setFrame:CGRectMake(_lbl_3.frame.origin.x, _lbl_2.frame.origin.y+f2+20, _lbl_3.frame.size.width, 0)];
                        
                        [_lbl_4 setFrame:CGRectMake(_lbl_4.frame.origin.x, _lbl_3.frame.origin.y+f3+20, _lbl_4.frame.size.width, 0)];
                        
                         _scrcarddetail.contentSize = CGSizeMake(0, _lbl_2.frame.origin.y+_lbl_2.frame.size.height+50) ;
                        
                   
                    }else{
                     [_lbl_3 setFrame:CGRectMake(_lbl_3.frame.origin.x, _lbl_2.frame.origin.y+f2+20, _lbl_3.frame.size.width, f3)];
                    
                    [_lbl_4 setFrame:CGRectMake(_lbl_4.frame.origin.x, _lbl_3.frame.origin.y+f3+20, _lbl_4.frame.size.width, f4)];
                        
                          _scrcarddetail.contentSize = CGSizeMake(0, _lbl_4.frame.origin.y+_lbl_4.frame.size.height+50) ;
      }
                     if (!_lbl_3.text.length) {
                        
                     }else{
                     if (!_lbl_5.text.length) {
                         
                    [_lbl_5 setFrame:CGRectMake(_lbl_5.frame.origin.x, _lbl_4.frame.origin.y+f4+20, _lbl_4.frame.size.width, 0)];
                    
                    [_lbl_6 setFrame:CGRectMake(_lbl_6.frame.origin.x, _lbl_5.frame.origin.y+f5+20, _lbl_6.frame.size.width, 0)];
                         
                     _scrcarddetail.contentSize = CGSizeMake(0, _lbl_4.frame.origin.y+_lbl_4.frame.size.height+50) ;
                     }
                     else{
                         [_lbl_5 setFrame:CGRectMake(_lbl_5.frame.origin.x, _lbl_4.frame.origin.y+f4+20, _lbl_4.frame.size.width, f5)];
                         
                         [_lbl_6 setFrame:CGRectMake(_lbl_6.frame.origin.x, _lbl_5.frame.origin.y+f5+20, _lbl_6.frame.size.width, f6)];
                          _scrcarddetail.contentSize = CGSizeMake(0, _lbl_6.frame.origin.y+_lbl_6.frame.size.height+50) ;
                      
                     }
                     }
                    
                    _lbl_header_image.text=strimgheader;
                    _lbl_name.text=strimgprofname;
                    
                    if ([strprofimageurl isKindOfClass:[NSNull class]] || [strprofimageurl isEqualToString:@""] ) {
                        [_img_prof sd_setImageWithURL:[NSURL URLWithString:@""]
                                         placeholderImage:[UIImage imageNamed:@"Pro.png"]];
                        
                    }else{
                        
                        [_img_prof sd_setImageWithURL:[NSURL URLWithString:strprofimageurl]
                                         placeholderImage:[UIImage imageNamed:@"Pro.png"]];
                    }
                    
                    
                    
                    if ([strpartnertype integerValue] ==1) {
                        [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"partnertype"];
                        _img_badge.image = [UIImage imageNamed:@"gold.png"];
                        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                            _lbl_parttype.text=@"Gold Partner";
                        }
                        else{
                             _lbl_parttype.text=@"شريك الفئة الذهبية";
                        }
                    }
                    else if ([strpartnertype integerValue] ==2) {
                        [[NSUserDefaults standardUserDefaults] setValue:@"2" forKey:@"partnertype"];
                        _img_badge.image = [UIImage imageNamed:@"silver.png"];
                        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                            _lbl_parttype.text=@"Silver Partner";
                        }
                        else{
                            _lbl_parttype.text=@"شريك الفئة الفضية";
                        }
                    }
                    else if ([strpartnertype  integerValue] ==3) {
                        [[NSUserDefaults standardUserDefaults] setValue:@"3" forKey:@"partnertype"];
                       _img_badge.image = [UIImage imageNamed:@"bronze.png"];
                        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                            _lbl_parttype.text=@"Bronze Partner";
                        }
                        else{
                            _lbl_parttype.text=@"شريك الفئة البرونزية";
                        }
                    }
                    else {
                        [[NSUserDefaults standardUserDefaults] setValue:@"4" forKey:@"partnertype"];
                        _img_badge.image = [UIImage imageNamed:@"platinum.png"];
                        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                            _lbl_parttype.text=@"Platinum Partner";
                        }
                        else{
                            _lbl_parttype.text=@"شريك فئة البلاتينيوم";
                        }
                    }
                    _img_prof.clipsToBounds=YES;
                 _img_prof.layer.cornerRadius=_img_prof.frame.size.width/2;
                  
                    
                    if ([strpartnertype  integerValue] ==1 || [strpartnertype  integerValue]==4) {
                        
                        if ([strpartnertype  integerValue] ==1) {
                            _img_1.image=[UIImage imageNamed:@"p2.jpg"];
                        }
                        else if ([strpartnertype  integerValue] ==4) {
                            _img_1.image=[UIImage imageNamed:@"p3.jpg"];
                        }
                        
                    }
                   
                }
                else{
                    [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
                    
                }
            }
        }];
    }
}

- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
