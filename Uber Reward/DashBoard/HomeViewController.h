//
//  HomeViewController.h
//  Uber Reward
//
//  Created by Arvind Seth on 22/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UtillClass.h"
#import "SlidingViewController.h"
@interface HomeViewController : UIViewController<slideViewControllerDelegate,UIDocumentInteractionControllerDelegate,UINavigationControllerDelegate>{
    SlidingViewController *slideViewMenu;
}
@property (strong, nonatomic) IBOutlet UILabel *lbl_arpwof;
@property (strong, nonatomic) IBOutlet UILabel *lbl_arbpo;
@property (strong, nonatomic) IBOutlet UILabel *lbl_arbsp;
@property (strong, nonatomic) IBOutlet UIButton *btn_sp;
@property (strong, nonatomic) IBOutlet UIButton *btn_po;
@property (strong, nonatomic) IBOutlet UIButton *btn_wof;


@property (weak, nonatomic) IBOutlet UILabel *lbl_profilename;
@property (weak, nonatomic) IBOutlet UIImageView *img_profile;
@property (weak, nonatomic) IBOutlet UIImageView *img_badge;
@property (weak, nonatomic) IBOutlet UILabel *lbl_points;
@property (weak, nonatomic) IBOutlet UILabel *lbl_partnertype;

@end
