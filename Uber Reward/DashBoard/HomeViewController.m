//
//  HomeViewController.m
//  Uber Reward
//
//  Created by Arvind Seth on 22/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import "HomeViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "noLOginWebViewViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:NO];
    
    [[UtillClass sharedInstance] hidebottom_items];
}
-(void)viewDidAppear:(BOOL)animated{
     [super viewDidAppear:NO];
    [[UtillClass sharedInstance]HideLoader];
    [self HideSlideMenu];
   [self GetHOmesummury];
    
}
- (IBAction)hereaboutUber:(id)sender {
    AppDelegate*app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    app.ViewWebTypeStr = @"About";
    
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"ios  HomeScreen"];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"HomeScreen"
                                                          action:@"Click here Press"
                                                           label:@"About Uber Platinum"
                                                           value:nil] build]];
    
    
    noLOginWebViewViewController *controller = [[noLOginWebViewViewController alloc] initWithNibName:[[UtillClass sharedInstance] getnologinxib] bundle:nil];
    [self presentViewController:controller animated:false completion:nil];
    
    
}
-(void)GetHOmesummury{
    [self.view endEditing:YES];
    if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        };
        
    }
    else{
        [[UtillClass sharedInstance] ShowLoaderOn:self.parentViewController withtext:@"Please wait"];
        
       [[UtillClass sharedInstance] getresponsefromservice:[NSString stringWithFormat:@"Statement/getHomeSummary?CultureId=%@&CustomerId=%@&altId=0",[[UtillClass sharedInstance] getcultureID],[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"Login Details"]objectForKey:@"CustomerId"]] withbearer:[[UtillClass sharedInstance] getapp_sessiontoken] response:^(NSDictionary * dict_response) {
            [[UtillClass sharedInstance] HideLoader];
            if (dict_response ==nil) {
                [[UtillClass sharedInstance] showerrortoastmsg:@"Server error, Please try after sometime."];
                
            }
            else{
                if ([[dict_response objectForKey:@"Status"] integerValue] == 1) {
                    
                    if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                        
                         _lbl_profilename.text= [NSString stringWithFormat:@"Welcome %@",[dict_response objectForKey:@"Name"]];
                    }else{
                        
                    _lbl_profilename.text= [NSString stringWithFormat:@"أهلاً بك %@",[dict_response objectForKey:@"Name"]];
                    
                    }
                    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
                    [tracker set:kGAIScreenName value:@"ios  HomeScreen"];
                    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"HomeScreen"
                                                                          action:@"User Name"
                                                                           label:_lbl_profilename.text
                                                                           value:nil] build]];
                    
                    
                    if ([[dict_response objectForKey:@"UserImage"] isKindOfClass:[NSNull class]] || [[dict_response objectForKey:@"UserImage"] isEqualToString:@""] ) {
                        [_img_profile sd_setImageWithURL:[NSURL URLWithString:@""]
                                     placeholderImage:[UIImage imageNamed:@"Pro.png"]];
                    }else{
                        
                        [_img_profile sd_setImageWithURL:[NSURL URLWithString:[dict_response objectForKey:@"UserImage"]]
                                     placeholderImage:[UIImage imageNamed:@"Pro.png"]];
                        
                    }
                    
                    
              //  [_img_profile sd_setImageWithURL:[NSURL URLWithString:[dict_response objectForKey:@"UserImage"]]
                                // placeholderImage:[UIImage imageNamed:@"Pro.png"]];
                    
                    if ([[dict_response objectForKey:@"TierId"] integerValue] ==1) {
                         [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"partnertype"];
                        _img_badge.image = [UIImage imageNamed:@"gold.png"];
                    }
                    else if ([[dict_response objectForKey:@"TierId"] integerValue] ==2) {
                        [[NSUserDefaults standardUserDefaults] setValue:@"2" forKey:@"partnertype"];
                        _img_badge.image = [UIImage imageNamed:@"silver.png"];
                    }
                    else if ([[dict_response objectForKey:@"TierId"]  integerValue] ==3) {
                        [[NSUserDefaults standardUserDefaults] setValue:@"3" forKey:@"partnertype"];
                        _img_badge.image = [UIImage imageNamed:@"bronze.png"];
                    }
                    else {
                        [[NSUserDefaults standardUserDefaults] setValue:@"4" forKey:@"partnertype"];
                        _img_badge.image = [UIImage imageNamed:@"platinum.png"];
                    }
                   
                      if (![[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                          NSString *text;
                          NSString *strboldtext;
                   
                   
                   if ([[dict_response objectForKey:@"TierId"]  integerValue]==1) {
                               strboldtext = @"";
                            
                          text = @"التصنيف الخاص بك هو الفئة الذهبية";
                        }
                        else if([[dict_response objectForKey:@"TierId"]  integerValue]==2){
                               strboldtext = @"";
                           text = @"التصنيف الخاص بك هو ضمن الفئة الفضية";
                            
                        }
                        else if ([[dict_response objectForKey:@"TierId"]  integerValue]==3){
                               strboldtext = @"";
                           text = @"التصنيف الخاص بك هو ضمن الفئة البرونزية";
                            
                        }
                                 else{
                                        strboldtext = @"";
                                     text = @"التصنيف الخاص بك هو فئة البلاتينيوم";
                                     
                     }
                        
                     

                    NSDictionary *attribs = @{
                                              NSForegroundColorAttributeName:[UIColor blackColor],
                                              NSFontAttributeName: [UIFont fontWithName:@"ClanPro-Book" size:10.6]
                                              };
                    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text attributes:attribs];
                    UIFont *boldFont = [UIFont fontWithName:@"ClanPro-Medium" size:10.6];
                    NSRange range = [text rangeOfString:strboldtext];
                    [attributedText setAttributes:@{NSForegroundColorAttributeName: [UIColor blackColor],
                                                    NSFontAttributeName:boldFont} range:range];
                    
                    _lbl_partnertype.attributedText=attributedText;
                    
                      }
                    
                      else{
                          NSString *str1 = @"Your Tier status is";
                          NSString *strboldtext = [NSString stringWithFormat:@" %@",[dict_response objectForKey:@"TierName"]];
                          NSString*str2= @"";
                          NSString *text = [NSString stringWithFormat:@"%@ %@ %@",str1,strboldtext,str2];
                          
                          NSDictionary *attribs = @{
                                                    NSForegroundColorAttributeName:[UIColor blackColor],
                                                    NSFontAttributeName: [UIFont fontWithName:@"ClanPro-Book" size:10.6]
                                                    };
                          NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text attributes:attribs];
                          UIFont *boldFont = [UIFont fontWithName:@"ClanPro-Medium" size:10.6];
                          NSRange range = [text rangeOfString:strboldtext];
                          [attributedText setAttributes:@{NSForegroundColorAttributeName: [UIColor blackColor],
                                                          NSFontAttributeName:boldFont} range:range];
                          
                          _lbl_partnertype.attributedText=attributedText;
                          
                      }
                    
                    if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
             _lbl_points.attributedText = [self getattributedstringfrom:[[UtillClass sharedInstance] getpointformatofint:[[dict_response objectForKey:@"BalancePoint"] intValue]] andwithnormaltext:@"Your Points balance is "];
                        
                    }
                    else{
                        
                        
                        _lbl_points.attributedText = [self getattributedstringfrom:[[UtillClass sharedInstance] getpointformatofint:[[dict_response objectForKey:@"BalancePoint"] intValue]] andwithnormaltext:@" رصيد نقاطك هو"];
                    }
                    
                    [[NSUserDefaults standardUserDefaults] setValue:[[UtillClass sharedInstance] getpointformatofint:[[dict_response objectForKey:@"BalancePoint"] intValue]] forKey:@"BalancePoints"];
                    
                    
                    
             }
                else{
                    
                    
                    if ([dict_response isKindOfClass:[NSDictionary class]]) {
                        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                            NSString *strAppNoti= [[NSUserDefaults standardUserDefaults] objectForKey:@"notificationid"];
                            
                            if ([[dict_response objectForKey:@"Status"]integerValue] ==0 && [[dict_response objectForKey:@"msg"]isEqualToString:@"Session Expired"]) {
                                 [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
                                NSString*str= [[NSUserDefaults standardUserDefaults] objectForKey:@"applanguage"];
                                NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
                                [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
                                
                                [[NSUserDefaults standardUserDefaults] setObject:strAppNoti forKey:@"notificationid"];
                                if ([str isEqualToString:@"English"]) {
                                    [[NSUserDefaults standardUserDefaults] setObject:@"English" forKey:@"applanguage"];
                                } else {
                                    [[NSUserDefaults standardUserDefaults] setObject:@"Arabic" forKey:@"applanguage"];
                                }
                                
                                UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance] getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"LoginViewController"];
                                [self presentViewController:vc animated:false completion:nil];
                                
                            }
                            else{
                                [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
                                
                            }
                        }
                        else{
                            NSString *strAppNoti= [[NSUserDefaults standardUserDefaults] objectForKey:@"notificationid"];
                            
                            if ([[dict_response objectForKey:@"Status"]integerValue]==0 && [[dict_response objectForKey:@"msg"]isEqualToString:@"انتهت الجلسة"]) {
                               [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
                                NSString*str= [[NSUserDefaults standardUserDefaults] objectForKey:@"applanguage"];
                                NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
                                [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
                                
                                [[NSUserDefaults standardUserDefaults] setObject:strAppNoti forKey:@"notificationid"];
                                if ([str isEqualToString:@"English"]) {
                                    [[NSUserDefaults standardUserDefaults] setObject:@"English" forKey:@"applanguage"];
                                } else {
                                    [[NSUserDefaults standardUserDefaults] setObject:@"Arabic" forKey:@"applanguage"];
                                }
                                
                                UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance] getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"LoginViewController"];
                                [self presentViewController:vc animated:false completion:nil];
                            }
                            else{
                               [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
                                
                            }
                            
                        }
                        
                    }
                    
                    
                   
                }
            }
        }];
    }
    
    
    /*
     UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"RegEmailViewController"];
     [self presentViewController:vc animated:false completion:nil];
     */
}
-(NSMutableAttributedString*) getattributedstringfrom: (NSString *)strboldtext andwithnormaltext:(NSString *)strnormaltext{
    
    
    NSString *text = [NSString stringWithFormat:@"%@%@",strnormaltext,strboldtext];
    
    NSDictionary *attribs = @{
                              NSForegroundColorAttributeName:[UIColor blackColor],
                              NSFontAttributeName: [UIFont fontWithName:@"ClanPro-Book" size:10.6]
                              };
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text attributes:attribs];
    UIFont *boldFont = [UIFont fontWithName:@"ClanPro-Medium" size:10.6];
    NSRange range = [text rangeOfString:strboldtext];
    [attributedText setAttributes:@{NSForegroundColorAttributeName: [UIColor blackColor],
                                    NSFontAttributeName:boldFont} range:range];
    return attributedText;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    if([UIScreen mainScreen].bounds.size.height==812 ){
        [_img_profile setFrame:CGRectMake(_img_profile.frame.origin.x, _img_profile.frame.origin.y, 127, 127)];
        [_img_badge setFrame:CGRectMake(_img_badge.frame.origin.x, _img_badge.frame.origin.y-4, 40, 52)];
        
        
        [_btn_sp setFrame:CGRectMake(_btn_sp.frame.origin.x, _btn_sp.frame.origin.y+4, _btn_sp.frame.size.width, 63)];
        [_btn_po setFrame:CGRectMake(_btn_po.frame.origin.x, _btn_po.frame.origin.y+4, _btn_po.frame.size.width, 63)];
        [_btn_wof setFrame:CGRectMake(_btn_wof.frame.origin.x, _btn_wof.frame.origin.y+4, _btn_wof.frame.size.width, 63)];
        
         [_lbl_arbsp setFrame:CGRectMake(_lbl_arbsp.frame.origin.x, _lbl_arbsp.frame.origin.y+4, _lbl_arbsp.frame.size.width, 63)];
        [_lbl_arbpo setFrame:CGRectMake(_lbl_arbpo.frame.origin.x, _lbl_arbpo.frame.origin.y+4, _lbl_arbpo.frame.size.width, 63)];
        
        [_lbl_arpwof setFrame:CGRectMake(_lbl_arpwof.frame.origin.x, _lbl_arpwof.frame.origin.y+4, _lbl_arpwof.frame.size.width, 63)];
        
    }
    _img_profile.clipsToBounds=YES;
    _img_profile.layer.cornerRadius= _img_profile.frame.size.width/2;
    [_img_profile.layer setMasksToBounds:YES];
    

    
    
    // Do any additional setup after loading the view.
}

- (IBAction)select_sidemenu:(id)sender {
        
        if (slideViewMenu.view) {
            [self HideSlideMenu];
        } else {
            slideViewMenu = [[UtillClass sharedInstance] CallSlideMenuView:self yVal:0.0f];
        }
    }
    
#pragma mark - slideView Delegate
    
    -(void) slideViewControllerDone:(SlidingViewController *)sender
    {
        if (sender) {
            [self HideSlideMenu];
        }
    }
    -(void) HideSlideMenu
    {
        [UIView animateWithDuration:0.50 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
            //code with animation
           // slideViewMenu.view.frame = CGRectMake(-self.view.frame.size.width, 0.0f, self.view.frame.size.width, self.view.frame.size.height - 0.0f);
            [slideViewMenu.view removeFromSuperview];
        } completion:^(BOOL finished) {
            //code for completion
            [slideViewMenu.view removeFromSuperview];
            slideViewMenu = nil;
            
        }];
    }
- (IBAction)select_privilage:(id)sender {
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    app.ViewWebTypeStr=@"Privilage";
    
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"ios  HomeScreen"];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"HomeScreen"
                                                          action:@"button_press"
                                                           label:@"Privilages & Offers"
                                                           value:nil] build]];
    
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"PrivilageViewController"];
    [self presentViewController:vc animated:false completion:nil];
    

    /*
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Alert!!"
                                 message:@"In Progress."
                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                   
                                }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Cancel"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                               }];
    
   
    
    [alert addAction:yesButton];
   // [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
*/
}


- (IBAction)select_spend_points:(id)sender {
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    app.ViewWebTypeStr= @"sped";
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"ios  HomeScreen"];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"HomeScreen"
                                                          action:@"button_press"
                                                           label:@"Spend Point"
                                                           value:nil] build]];
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"SpedPontViewController"];
    [self presentViewController:vc animated:false completion:nil];
}

- (IBAction)select_walloffame:(id)sender {
//    UIAlertController * alert = [UIAlertController
//                                 alertControllerWithTitle:@"Alert!!"
//                                 message:@"In Progress."
//                                 preferredStyle:UIAlertControllerStyleAlert];
//    UIAlertAction* yesButton = [UIAlertAction
//                                actionWithTitle:@"OK"
//                                style:UIAlertActionStyleDefault
//                                handler:^(UIAlertAction * action) {
//                                    //Handle your yes please button action here
//
//                                }];
//
//    UIAlertAction* noButton = [UIAlertAction
//                               actionWithTitle:@"Cancel"
//                               style:UIAlertActionStyleDefault
//                               handler:^(UIAlertAction * action) {
//                                   //Handle no, thanks button
//                               }];
//
//
//
//    [alert addAction:yesButton];
//    // [alert addAction:noButton];
//
//    [self presentViewController:alert animated:YES completion:nil];
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    app.ViewWebTypeStr= @"Walloffame";
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"ios  HomeScreen"];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"HomeScreen"
                                                          action:@"button_press"
                                                           label:@"Wall of Fame"
                                                           value:nil] build]];
   UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"WalloffameViewController"];
    [self presentViewController:vc animated:false completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
