//
//  NotiTableViewCell.h
//  Uber Reward
//
//  Created by Arvind on 23/03/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotiTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lbl_date;
@property (strong, nonatomic) IBOutlet UILabel *lbl_msg1;
@end
