//
//  NotiViewController.m
//  Uber Reward
//
//  Created by Arvind on 23/03/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import "NotiViewController.h"
#import "NotiTableViewCell.h"
@interface NotiViewController ()

@end

@implementation NotiViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arr_date=[[NSMutableArray alloc]init];
    arr_msg1=[[NSMutableArray alloc]init];
    arr_msg2=[[NSMutableArray alloc]init];
    arr_msg3=[[NSMutableArray alloc]init];
    
    
    // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated{
    [[UtillClass sharedInstance]HideLoader];
    [self HideSlideMenu];
    
    [self Getprevoiusmsg];
    
}
-(IBAction)backregtowel:(id)sender{
    
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
    [self presentViewController:vc animated:false completion:nil];
}
-(void)Getprevoiusmsg{
    [self.view endEditing:YES];
    if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        };
        
    }
    else{
        [[UtillClass sharedInstance] ShowLoaderOn:self.parentViewController withtext:@"Please wait"];
        
        NSMutableDictionary*dicts = [[NSMutableDictionary alloc]init];
        [dicts setValue:[[UtillClass sharedInstance] getcultureID] forKey:@"CultureId"];
        
        
        [[UtillClass sharedInstance] getresponsefromservice:[NSString stringWithFormat:@"Account/GetNotificationDetails?CultureId=%@&CustomerId=%@",[[UtillClass sharedInstance] getcultureID],[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"Login Details"]objectForKey:@"CustomerId"]] withbearer:[[UtillClass sharedInstance] getapp_sessiontoken] response:^(NSDictionary * dict_response) {
            [[UtillClass sharedInstance] HideLoader];
            if (dict_response ==nil) {
                [[UtillClass sharedInstance] showerrortoastmsg:@"Server error, Please try after sometime."];
                
            }
            else{
                if ([[dict_response objectForKey:@"Status"] integerValue] == 1) {
                    NSArray *Arr = [dict_response objectForKey:@"ListData"];
                    if (![Arr isKindOfClass:[NSNull class]]) {
                    for (int i=0; i<[Arr count]; i++) {
                        
                        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                        formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
                        
                        NSDate *date = [formatter dateFromString:[[[[Arr objectAtIndex:i]objectForKey:@"NotificationDate"] stringByReplacingOccurrencesOfString:@"T" withString:@" "] substringToIndex:19]];
                        
                        formatter.dateFormat = @"dd-MM-yyyy hh:mm: a";
                        NSString *strloc;
                        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                            strloc =@"en";
                        }
                        else{
                            strloc =@"ar";
                        }
                        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:strloc];
                        [formatter setLocale:usLocale];
                        NSString *dateString = [formatter stringFromDate:date];
                        
                       [arr_date addObject:dateString];
                
                     [arr_msg1 addObject:[[Arr objectAtIndex:i]objectForKey:@"ShortMessage"]];
                       
                        
                    }
                    [_tbl_prvmsg reloadData];
                    }
                    
                }
                else{
                    [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
                }
            }
        }];
    }
    
    
    /*
     UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"RegEmailViewController"];
     [self presentViewController:vc animated:false completion:nil];
     */
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [arr_date count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"NotiTableViewCell";
    
    cell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier forIndexPath: indexPath];
    
    cell.lbl_date.text = [arr_date objectAtIndex:indexPath.section];
    
    
    cell.lbl_msg1.text = [arr_msg1 objectAtIndex:indexPath.section];
    
    
    cell.selectionStyle =UITableViewCellSelectionStyleNone;
    
    cell.layer.borderWidth= 1;
    cell.layer.borderColor=[UIColor colorWithRed:192/255.0f green:192/255.0f blue:192/255.0f alpha:1].CGColor;
    cell.layer.cornerRadius=1;
    return cell;
    
}
- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section
{
    //Set the background color of the View
    view.tintColor = [UIColor clearColor];
}
- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
  NSString *str =  [arr_msg1 objectAtIndex:indexPath.section];
//    UILabel *lbl =[[UILabel alloc] init];
//
//    lbl.text= str;
//    float f1 =[self getLabelHeight:lbl]+30;
//    NSLog(@"length%f---%lu",f1,lbl.text.length);
//    return f1;
    
//    if (str.length<55) {
//        return 50;
//    }
//
//    else if(str.length>56 && str.length<120) {
//
//        return 60;
//    }
//    else if(str.length>121 && str.length<150) {
//
//        return 80;
//    }
//    else {
//
//        return 110;
//    }
   
    
  //  NSLog(@"text : %@", self.cell.comment.text);
    
    NSString *text = [arr_msg1 objectAtIndex:indexPath.section];
    CGFloat width = cell.frame.size.width;
    
    UIFont *font = [UIFont fontWithName:@"ClanPro-Book" size:10.6];
    NSAttributedString *attributedText =
    [[NSAttributedString alloc] initWithString:text
                                    attributes:@{NSFontAttributeName: font}];
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){width, CGFLOAT_MAX}
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:nil];
    CGSize size = rect.size;
    CGFloat height = ceilf(size.height);
    
    return height+30;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 8;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
   
    
    
}
- (IBAction)select_side_menu:(id)sender {
    if (slideViewMenu.view) {
        [self HideSlideMenu];
    } else {
        slideViewMenu = [[UtillClass sharedInstance] CallSlideMenuView:self yVal:0.0f];
        //slideViewMenu.Str_isLogIn = @"noLogin";
    }
}


#pragma mark - slideView Delegate

-(void) slideViewControllerDone:(SlidingViewController *)sender
{
    if (sender) {
        [self HideSlideMenu];
    }
}
-(void) HideSlideMenu
{
    [UIView animateWithDuration:0.50 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        //code with animation
        // slideViewMenu.view.frame = CGRectMake(-self.view.frame.size.width, 0.0f, self.view.frame.size.width, self.view.frame.size.height - 0.0f);
        [slideViewMenu.view removeFromSuperview];
    } completion:^(BOOL finished) {
        //code for completion
        [slideViewMenu.view removeFromSuperview];
        slideViewMenu = nil;
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
