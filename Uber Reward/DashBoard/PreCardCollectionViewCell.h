//
//  PreCardCollectionViewCell.h
//  Uber Reward
//
//  Created by Arvind on 09/04/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PreCardCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *img_hidelay;
@property (strong, nonatomic) IBOutlet UIImageView *img_bg;
@property (strong, nonatomic) IBOutlet UILabel *lbl_headerbold;
@property (strong, nonatomic) IBOutlet UILabel *lbl_profname;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Partner;
@property (strong, nonatomic) IBOutlet UIImageView *img_prof;
@property (strong, nonatomic) IBOutlet UIImageView *img_badge;

@end
