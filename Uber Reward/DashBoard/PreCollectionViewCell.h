//
//  PreCollectionViewCell.h
//  Uber Reward
//
//  Created by Arvind on 09/04/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PreCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *img_partDisc;
@property (strong, nonatomic) IBOutlet UILabel *lbl_offerheading;

@end
