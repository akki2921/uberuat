//
//  PrevDiscDetailViewController.h
//  Uber Reward
//
//  Created by Arvind Seth on 09/04/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlidingViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "CustomeLabel.h"

@interface PrevDiscDetailViewController : UIViewController<slideViewControllerDelegate>{
    
    SlidingViewController *slideViewMenu;
}
@property (strong, nonatomic) IBOutlet CustomeLabel *lbl_terms;
@property (strong, nonatomic) NSString *strOfferid;
@property (strong, nonatomic) NSString *strurl;
@property (strong, nonatomic) NSString *strimageurl;
@property (strong, nonatomic) IBOutlet UILabel *lbl_percentoff;
@property (strong, nonatomic) IBOutlet UIView *view_validity;
@property (strong, nonatomic) IBOutlet UILabel *lbl_offdetail;
@property (strong, nonatomic) IBOutlet UIImageView *img_brand;
@property (strong, nonatomic) IBOutlet UILabel *lbl_validitydate;
@property (strong, nonatomic) IBOutlet UIView *view_termsCond;
@property (strong, nonatomic) IBOutlet UIScrollView *scr_predicdet;
@property (strong, nonatomic) IBOutlet UIButton *btn_website;
@end
