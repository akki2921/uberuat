//
//  PrevDiscDetailViewController.m
//  Uber Reward
//
//  Created by Arvind Seth on 09/04/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import "PrevDiscDetailViewController.h"
#import "UtillClass.h"
@interface PrevDiscDetailViewController ()

@end

@implementation PrevDiscDetailViewController
@synthesize strOfferid,strimageurl;
- (IBAction)select_sidemenu:(id)sender {
    
   
    if (slideViewMenu.view) {
        [self HideSlideMenu];
    } else {
        slideViewMenu = [[UtillClass sharedInstance] CallSlideMenuView:self yVal:0.0f];
    }
}
- (IBAction)select_back:(id)sender{
    [[UtillClass sharedInstance] hidebottom_items];
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"PrivilageViewController"];
    [self presentViewController:vc animated:false completion:nil];
    
    
}
#pragma mark - slideView Delegate

-(void) slideViewControllerDone:(SlidingViewController *)sender
{
    if (sender) {
        [self HideSlideMenu];
    }
}
-(void) HideSlideMenu
{
    [UIView animateWithDuration:0.50 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        //code with animation
        // slideViewMenu.view.frame = CGRectMake(-self.view.frame.size.width, 0.0f, self.view.frame.size.width, self.view.frame.size.height - 0.0f);
        [slideViewMenu.view removeFromSuperview];
    } completion:^(BOOL finished) {
        //code for completion
        [slideViewMenu.view removeFromSuperview];
        slideViewMenu = nil;
        
    }];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
  
    // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated{
    [[UtillClass sharedInstance]HideLoader];
    [self HideSlideMenu];
    [[UtillClass sharedInstance] showbottom_items];
    [self Getoffdetail];
}
-(void)Getoffdetail{
    [self.view endEditing:YES];
    if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        };
        
    }
    else{
        [[UtillClass sharedInstance] ShowLoaderOn:self.parentViewController withtext:@"Please wait"];
        
        [[UtillClass sharedInstance] getresponsefromservice:[NSString stringWithFormat:@"Account/GetOfferssDetail?CultureId=%@&CustomerId=%@&altId=0&OfferId=%@",[[UtillClass sharedInstance] getcultureID],[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"Login Details"]objectForKey:@"CustomerId"],strOfferid] withbearer:[[UtillClass sharedInstance] getapp_sessiontoken] response:^(NSDictionary * dict_response) {
            [[UtillClass sharedInstance] HideLoader];
            if (dict_response ==nil) {
                [[UtillClass sharedInstance] showerrortoastmsg:@"Server error, Please try after sometime."];
                
            }
            else{
                if ([[dict_response objectForKey:@"Status"] integerValue] == 1) {
                    _img_brand.image= [UIImage imageNamed:@"Pro.png"];
                    
                    if ([strimageurl isKindOfClass:[NSNull class]] || [strimageurl isEqualToString:@""] ) {
                        [_img_brand sd_setImageWithURL:[NSURL URLWithString:@""]
                                         placeholderImage:[UIImage imageNamed:@"Pro.png"]];
                        
                    }else{
                        
                        [_img_brand sd_setImageWithURL:[NSURL URLWithString:strimageurl]
                                         placeholderImage:[UIImage imageNamed:@"Pro.png"]];
                    }
                    
                    
                    _lbl_percentoff.text=[dict_response objectForKey:@"MainHeading"];
                    _lbl_offdetail.text=[dict_response objectForKey:@"MainContent"];
                    _lbl_validitydate.text=[dict_response objectForKey:@"Validity"];
                    
                    _lbl_terms.text=[dict_response objectForKey:@"TermsnConditions"];
                    _strurl = [dict_response objectForKey:@"Url"];
                    NSString *str1 = [dict_response objectForKey:@"Url_Text"];
                  
                    NSString *text = [NSString stringWithFormat:@"%@",str1];
                    
                
//                    
                    NSMutableAttributedString *titleString = [[NSMutableAttributedString alloc] initWithString:text];
                    _btn_website.tintColor =[UIColor colorWithRed:197/255.0f green:21/255.0f blue:74/255.0f alpha:1];
                    // making text property to underline text-
                    [titleString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [titleString length])];
                    
                    // using text on button
                    [_btn_website setAttributedTitle: titleString forState:UIControlStateNormal];
                 
                    //[_btn_website setTitle:linkAttributes forState:UIControlStateNormal];
                    
                    float f0 = [self getLabelHeight:_lbl_percentoff];
                    float f1 = [self getLabelHeight:_lbl_offdetail];
                    float f2 = [self getLabelHeight:_lbl_terms];
                    
                      [_lbl_percentoff setFrame:CGRectMake(_lbl_percentoff.frame.origin.x, _lbl_percentoff.frame.origin.y, _lbl_percentoff.frame.size.width, f0)];
                    
                    [_lbl_offdetail setFrame:CGRectMake(_lbl_offdetail.frame.origin.x, _lbl_percentoff.frame.origin.y+f0+15, _lbl_offdetail.frame.size.width, f1)];
                    
                    [_view_validity setFrame:CGRectMake(_view_validity.frame.origin.x, _lbl_offdetail.frame.origin.y+f1+20, _view_validity.frame.size.width, _view_validity.frame.size.height)];
                    
                    [_view_termsCond setFrame:CGRectMake(_view_termsCond.frame.origin.x, _view_validity.frame.origin.y+_view_validity.frame.size.height+10, _view_termsCond.frame.size.width, f2+20)];
                    
                    [_lbl_terms setFrame:CGRectMake(_lbl_terms.frame.origin.x, 35, _lbl_terms.frame.size.width, f2)];
                    
                    [_btn_website setFrame:CGRectMake(_btn_website.frame.origin.x, _view_termsCond.frame.origin.y+_view_termsCond.frame.size.height+20, _btn_website.frame.size.width, _btn_website.frame.size.height)];
                    
                    _scr_predicdet.contentSize = CGSizeMake(0, _btn_website.frame.origin.y+60) ;
                }
                else{
                    [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
                }
            }
        }];
    }
}
-(IBAction)actionweb:(id)sender{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:_strurl]];
    
}
- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
