//
//  PrivilageViewController.h
//  Uber Reward
//
//  Created by Arvind Seth on 22/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UtillClass.h"
#import "SlidingViewController.h"
@interface PrivilageViewController : UIViewController<slideViewControllerDelegate>

{   NSMutableArray *arrcardimage;
    NSMutableArray*arrdiscimg;
    
    NSMutableArray *arrheaderbold;
    NSMutableArray *arrPreevID;
    NSMutableArray *arrprofname;
    NSMutableArray*arrprofimg;
    NSMutableArray *arrPartner;

    
    NSMutableArray *arrofferheading;
    NSMutableArray *arrofferid;
    
    SlidingViewController *slideViewMenu;
}
@property (strong, nonatomic) IBOutlet UILabel *lbl_priornew;

@property (strong, nonatomic) IBOutlet UIScrollView *scr_pre;
@property (strong, nonatomic) IBOutlet UICollectionView *col_prev_offers;
@property (strong, nonatomic) IBOutlet UIImageView *img_prev;
@property (strong, nonatomic) IBOutlet UICollectionView *col_cards;

@end
