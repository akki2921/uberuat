//
//  PrivilageViewController.m
//  Uber Reward
//
//  Created by Arvind Seth on 22/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import "PrivilageViewController.h"
#import "PreCollectionViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "PreCardCollectionViewCell.h"
#import "PrevDiscDetailViewController.h"
#import "CardDetailViewController.h"
@interface PrivilageViewController ()

@end

@implementation PrivilageViewController
-(void)viewDidAppear:(BOOL)animated {
    [[UtillClass sharedInstance]HideLoader];
    [self HideSlideMenu];
    [[UtillClass sharedInstance] showbottom_items];
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"ios  Privilages"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    [self GetPrevi];
}
- (IBAction)select_sidemenu:(id)sender {
    
    if (slideViewMenu.view) {
        [self HideSlideMenu];
    } else {
        slideViewMenu = [[UtillClass sharedInstance] CallSlideMenuView:self yVal:0.0f];
    }
}
- (IBAction)select_back:(id)sender{
    [[UtillClass sharedInstance] hidebottom_items];
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
   
                UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
                [self presentViewController:vc animated:false completion:nil];
    
    
}
#pragma mark - slideView Delegate

-(void) slideViewControllerDone:(SlidingViewController *)sender
{
    if (sender) {
        [self HideSlideMenu];
    }
}
-(void) HideSlideMenu
{
    [UIView animateWithDuration:0.50 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        //code with animation
        // slideViewMenu.view.frame = CGRectMake(-self.view.frame.size.width, 0.0f, self.view.frame.size.width, self.view.frame.size.height - 0.0f);
        [slideViewMenu.view removeFromSuperview];
    } completion:^(BOOL finished) {
        //code for completion
        [slideViewMenu.view removeFromSuperview];
        slideViewMenu = nil;
        
    }];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _scr_pre.showsVerticalScrollIndicator=NO;
    arrcardimage=[[NSMutableArray alloc]init];
    arrdiscimg=[[NSMutableArray alloc]init];
    
    arrheaderbold=[[NSMutableArray alloc]init];
    arrprofname=[[NSMutableArray alloc]init];
    arrprofimg=[[NSMutableArray alloc]init];
    arrPartner=[[NSMutableArray alloc]init];
    arrPreevID =[[NSMutableArray alloc]init];
    
    arrofferheading=[[NSMutableArray alloc]init];
    arrofferid=[[NSMutableArray alloc]init];
    
    _col_cards.scrollEnabled=NO;
//    if([UIScreen mainScreen].bounds.size.height==812 ){
//
//        [_img_prev setImage:[UIImage imageNamed:@"prev_x.png"]];
//
//    }
    [_scr_pre setContentSize:CGSizeMake(0,_col_prev_offers.frame.size.height+_col_prev_offers.frame.origin.y+138)];
    
    if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
        
    }
    else{
        // [_scr_pre setTransform:CGAffineTransformMakeScale(-1, 1)];
        [_col_prev_offers setTransform:CGAffineTransformMakeScale(-1, 1)];
       
     
        
        
    }
    
    // Do any additional setup after loading the view.
}

-(void)GetPrevi{
    [self.view endEditing:YES];
    if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        };
        
    }
    else{
        [[UtillClass sharedInstance] ShowLoaderOn:self.parentViewController withtext:@"Please wait"];
        
        [[UtillClass sharedInstance] getresponsefromservice:[NSString stringWithFormat:@"Account/PrivilegesandOffers?CultureId=%@&CustomerId=%@&altId=0",[[UtillClass sharedInstance] getcultureID],[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"Login Details"]objectForKey:@"CustomerId"]] withbearer:[[UtillClass sharedInstance] getapp_sessiontoken] response:^(NSDictionary * dict_response) {
            [[UtillClass sharedInstance] HideLoader];
            if (dict_response ==nil) {
                [[UtillClass sharedInstance] showerrortoastmsg:@"Server error, Please try after sometime."];
            
            }
            else{
                if ([[dict_response objectForKey:@"Status"] integerValue] == 1) {
                    [arrprofimg addObject:[dict_response objectForKey:@"UserImage"]];
                    [arrPartner addObject:[dict_response objectForKey:@"Tier"]];
                    [arrprofname addObject:[NSString stringWithFormat:@"%@ %@",[dict_response objectForKey:@"FirstName"],[dict_response objectForKey:@"LastName"]]];
                    NSArray *arr= [dict_response objectForKey:@"Privileges"];
                    for (int i =0; i<[arr count]; i++) {
                        [arrheaderbold addObject:[[arr objectAtIndex:i]objectForKey:@"Heading"]];
                        [arrPreevID addObject:[[arr objectAtIndex:i]objectForKey:@"PrivilegeID"]];
                    }
                    
                    NSArray *arr2= [dict_response objectForKey:@"Offers"];
                    for (int i =0; i<[arr2 count]; i++) {
                        // [arrheaderbold addObject:[[arr objectAtIndex:i]objectForKey:@"Heading"]];
                        [arrdiscimg addObject:[[arr2 objectAtIndex:i]objectForKey:@"OfferImage"]];
                        [arrofferid addObject:[[arr2 objectAtIndex:i]objectForKey:@"OfferID"]];
                        [arrofferheading addObject:[[arr2 objectAtIndex:i]objectForKey:@"OfferHeading"]];
                    }
                    [_col_cards reloadData
                     ];
                    [_col_prev_offers reloadData
                     ];
                    
                }
                else{
                    [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
                }
            }
        }];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView.tag==1) {
        
    
    if ([UIScreen mainScreen].bounds.size.height==568) {
        return CGSizeMake(266, 120);
    }
    else if([UIScreen mainScreen].bounds.size.height==667){
        return CGSizeMake(312, 135);
    }
    else if([UIScreen mainScreen].bounds.size.height==736){
        return CGSizeMake(346, 140);
    }
    else if([UIScreen mainScreen].bounds.size.height==812){
        return CGSizeMake(312, 140);
    }
    else{

        return CGSizeMake(266, 93);

    }
    }else{
        
        if ([UIScreen mainScreen].bounds.size.height==568) {
            return CGSizeMake(115, 118);
        }
        else if(([UIScreen mainScreen].bounds.size.height==667) ||([UIScreen mainScreen].bounds.size.height==812) ){
            return CGSizeMake(136, 136);
        }
        else if([UIScreen mainScreen].bounds.size.height==736){
            return CGSizeMake(142, 142);
        }
        else{
            
            return CGSizeMake(136, 136);
            
        }
    }

}


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    UIEdgeInsets insets=UIEdgeInsetsMake(0, 10, 0, 10);
    return insets;
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
    return 1;
    
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView.tag==1) {
        return [arrPreevID count];
    }
    return [arrdiscimg count];
   
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionVW cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (collectionVW.tag==1) {
        
       PreCardCollectionViewCell*cell = [collectionVW dequeueReusableCellWithReuseIdentifier:@"PreCardCollectionViewCell" forIndexPath:indexPath];
        
        cell.lbl_headerbold.text=[arrheaderbold objectAtIndex:indexPath.item];
        cell.lbl_profname.text=[arrprofname objectAtIndex:0];
        
        if ([[arrprofimg objectAtIndex:0] isKindOfClass:[NSNull class]] || [[arrprofimg objectAtIndex:0] isEqualToString:@""] ) {
                       [cell.img_prof sd_setImageWithURL:[NSURL URLWithString:@""]
                                        placeholderImage:[UIImage imageNamed:@"Pro.png"]];
            
                   }else{
            
                       [cell.img_prof sd_setImageWithURL:[NSURL URLWithString:[arrprofimg objectAtIndex:0]]
                                        placeholderImage:[UIImage imageNamed:@"Pro.png"]];
            }
        
        if ([[arrPartner objectAtIndex:0] integerValue] ==1) {
            [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"partnertype"];
            cell.img_badge.image = [UIImage imageNamed:@"gold.png"];
            if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                cell.lbl_Partner.text=@"Gold Partner";
            }
            else{
               cell.lbl_Partner.text=@"شريك الفئة الذهبية";
            }
            
        }
        else if ([[arrPartner objectAtIndex:0] integerValue] ==2) {
            [[NSUserDefaults standardUserDefaults] setValue:@"2" forKey:@"partnertype"];
            cell.img_badge.image = [UIImage imageNamed:@"silver.png"];
            if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                cell.lbl_Partner.text=@"Silver Partner";
            }
            else{
                cell.lbl_Partner.text=@"شريك الفئة الفضية";
            }
        }
        else if ([[arrPartner objectAtIndex:0]  integerValue] ==3) {
            [[NSUserDefaults standardUserDefaults] setValue:@"3" forKey:@"partnertype"];
            cell.img_badge.image = [UIImage imageNamed:@"bronze.png"];
            if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                cell.lbl_Partner.text=@"Bronze Partner";
            }
            else{
                cell.lbl_Partner.text=@"شريك الفئة البرونزية";
            }
        }
        else {
            [[NSUserDefaults standardUserDefaults] setValue:@"4" forKey:@"partnertype"];
            cell.img_badge.image = [UIImage imageNamed:@"platinum.png"];
            if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                cell.lbl_Partner.text=@"Platinum Partner";
            }
            else{
                cell.lbl_Partner.text=@"شريك فئة البلاتينيوم";
            }
        }
        
        if ([[arrPartner objectAtIndex:0]  integerValue] ==1 || [[arrPartner objectAtIndex:0]  integerValue]==4) {
            cell.img_hidelay.hidden=YES;
            if ([[arrPartner objectAtIndex:0]  integerValue] ==1) {
               cell.img_bg.image=[UIImage imageNamed:@"p2.jpg"];
            }
            else if ([[arrPartner objectAtIndex:0]  integerValue] ==4) {
                cell.img_bg.image=[UIImage imageNamed:@"p3.jpg"];
            }
            
        }
        else{
            cell.img_hidelay.hidden=YES;
            cell.lbl_profname.text=@"";
            cell.lbl_Partner.text=@"";
            cell.img_badge.image=[UIImage imageNamed:@""];
            cell.img_prof.image=[UIImage imageNamed:@""];
            cell.img_bg.image=[UIImage imageNamed:@"p1g.png"];
        }
       // _lbl_priornew.text =[arrheaderbold objectAtIndex:indexPath.item];
        
        cell.img_prof.clipsToBounds=YES;
        cell.img_prof.layer.cornerRadius=cell.img_prof.frame.size.width/2;
        
        cell.img_hidelay.clipsToBounds=YES;
        cell.img_hidelay.layer.cornerRadius= 8;
        
        return cell;
        
    }else{
        PreCollectionViewCell*cell = [collectionVW dequeueReusableCellWithReuseIdentifier:@"PreCollectionViewCell" forIndexPath:indexPath  ];
        cell.layer.borderWidth= 0.7;
        cell.layer.borderColor=[UIColor colorWithRed:192/255.0f green:192/255.0f blue:192/255.0f alpha:1].CGColor;
        cell.layer.cornerRadius=0;
       if ([[arrdiscimg objectAtIndex:indexPath.row] isKindOfClass:[NSNull class]] || [[arrdiscimg objectAtIndex:indexPath.item] isEqualToString:@""] ) {
           [cell.img_partDisc sd_setImageWithURL:[NSURL URLWithString:@""]
                            placeholderImage:[UIImage imageNamed:@""]];
           
       }else{
           
           [cell.img_partDisc sd_setImageWithURL:[NSURL URLWithString:[arrdiscimg objectAtIndex:indexPath.item]]
                            placeholderImage:[UIImage imageNamed:@""]];
           
       }
        cell.lbl_offerheading.text = [arrofferheading objectAtIndex:indexPath.item];
       
       
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            
        }
        else{
            
            [cell.contentView setTransform:CGAffineTransformMakeScale(-1, 1)];
        }
        
        return  cell;
    }
   
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView.tag==1) {
    CardDetailViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"CardDetailViewController"];
        
        
//        @property (strong, nonatomic) NSString *strPrevID;
//        @property (strong, nonatomic) NSString *strimgheader;
//        @property (strong, nonatomic) NSString *strimgprofname;
//        @property (strong, nonatomic) NSString *strpartnertype;
//        @property (strong, nonatomic) NSString *strbadgeimagename;
//        @property (strong, nonatomic) NSString *strprofimageurl;
        
        vc.strPrevID= [arrPreevID objectAtIndex:indexPath.item];
        vc.strimgheader= [arrheaderbold objectAtIndex:indexPath.item];
        vc.strimgprofname= [arrprofname objectAtIndex:0];
        vc.strpartnertype= [arrPartner objectAtIndex:0];
        vc.strprofimageurl= [arrprofimg objectAtIndex:0];
        
        id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
        [tracker set:kGAIScreenName value:@"ios  Privilages"];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Privilages"
                                                              action:@"Card Press"
                                                               label:vc.strPrevID
                                                               value:nil] build]];
        if ([vc.strpartnertype isEqualToString:@"1"] || [vc.strpartnertype isEqualToString:@"4"]) {
             [self presentViewController:vc animated:false completion:nil];
        }
        
   
    }
    else{
        
     PrevDiscDetailViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"PrevDiscDetailViewController"];
        
        vc.strOfferid= [arrofferid objectAtIndex:indexPath.item];
        vc.strimageurl= [arrdiscimg objectAtIndex:indexPath.item];
        id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
        [tracker set:kGAIScreenName value:@"ios  Privilages"];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Offers"
                                                              action:@"Offer Press"
                                                               label:vc.strOfferid
                                                               value:nil] build]];
        [self presentViewController:vc animated:false completion:nil];
        
    }
    
   
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
