//
//  SpedPontViewController.h
//  Uber Reward
//
//  Created by Arvind Seth on 22/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UtillClass.h"
#import "SlidingViewController.h"
#import <Google/Analytics.h>
@interface SpedPontViewController : UIViewController<slideViewControllerDelegate>
{
    id<GAITracker> tracker;
    NSMutableArray *arr_img_merchants;
    NSMutableArray *arr_voucherid;
    NSMutableArray *arr_voucherdesc;
    NSMutableArray *arr_bcode;
    SlidingViewController *slideViewMenu;
    NSMutableArray*arrenable;
    NSString*TotalPoints;
}

@property (strong, nonatomic) IBOutlet UICollectionView *col_spend;

@end
