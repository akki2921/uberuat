//
//  SpedPontViewController.m
//  Uber Reward
//
//  Created by Arvind Seth on 22/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import "SpedPontViewController.h"
#import "SpendCollectionViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "SpendmerchantViewController.h"
@interface SpedPontViewController ()

@end

@implementation SpedPontViewController
-(void)viewDidAppear:(BOOL)animated {
    [[UtillClass sharedInstance]HideLoader];
    [self HideSlideMenu];
    [[UtillClass sharedInstance] showbottom_items];
    

    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"ios  Spend Point 1"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    [self Getspenbrands];

}
-(void)GetHOmesummurySpend{
    [self.view endEditing:YES];
    if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        };
        
    }
    else{
        [[UtillClass sharedInstance] ShowLoaderOn:self.parentViewController withtext:@"Please wait"];
        
        [[UtillClass sharedInstance] getresponsefromservice:[NSString stringWithFormat:@"Statement/getHomeSummary?CultureId=%@&CustomerId=%@&altId=0",[[UtillClass sharedInstance] getcultureID],[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"Login Details"]objectForKey:@"CustomerId"]] withbearer:[[UtillClass sharedInstance] getapp_sessiontoken] response:^(NSDictionary * dict_response) {
            [[UtillClass sharedInstance] HideLoader];
            if (dict_response ==nil) {
                [[UtillClass sharedInstance] showerrortoastmsg:@"Server error, Please try after sometime."];
                
            }
            else{
                if ([[dict_response objectForKey:@"Status"] integerValue] == 1) {
                    
                 [[NSUserDefaults standardUserDefaults] setValue:[[UtillClass sharedInstance] getpointformatofint:[[dict_response objectForKey:@"BalancePoint"] intValue]] forKey:@"BalancePoints"];
                    
                    
                    
                }
                else{
                    [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
                }
            }
        }];
    }
    
    
    /*
     UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"RegEmailViewController"];
     [self presentViewController:vc animated:false completion:nil];
     */
}
-(void)Getspenbrands{
    [self.view endEditing:YES];
    if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        };
        
    }
    else{
        [[UtillClass sharedInstance] ShowLoaderOn:self.parentViewController withtext:@"Please wait"];
        
        [[UtillClass sharedInstance] getresponsefromservice:[NSString stringWithFormat:@"Redemption/getAvailableRedemtionsMobile?CultureId=%@&CustomerId=%@",[[UtillClass sharedInstance] getcultureID],[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"Login Details"]objectForKey:@"CustomerId"]] withbearer:[[UtillClass sharedInstance] getapp_sessiontoken] response:^(NSDictionary * dict_response) {
            [[UtillClass sharedInstance] HideLoader];
            if (dict_response ==nil) {
                [[UtillClass sharedInstance] showerrortoastmsg:@"Server error, Please try after sometime."];
            }
            else{
                if ([[dict_response objectForKey:@"Status"] integerValue] == 1) {
                    [[UtillClass sharedInstance]  setUserdefaultofDict:dict_response withKayname:@"getAvailableRedemtionsMobile"];
                    TotalPoints = [dict_response objectForKey:@"TotalPoints"];
                    NSArray *Arr = [dict_response objectForKey:@"redemptionModel"];
                    for (int i=0; i<[Arr count]; i++) {
                        [arr_img_merchants addObject:[[Arr objectAtIndex:i]objectForKey:@"VoucherURL"]];
                         [arr_voucherid addObject:[[Arr objectAtIndex:i]objectForKey:@"VoucherID"]];
                          [arr_bcode addObject:[[Arr objectAtIndex:i]objectForKey:@"BrandCode"]];
                        [arr_voucherdesc addObject:[[Arr objectAtIndex:i]objectForKey:@"VoucherDesc"]];
                         [arrenable addObject:[[Arr objectAtIndex:i]objectForKey:@"points"]];
                    }
                    [self GetHOmesummurySpend];
                    [_col_spend reloadData];
                
                    
                }
                else{
                    [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
                }
            }
        }];
    }
    
    
    /*
     UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"RegEmailViewController"];
     [self presentViewController:vc animated:false completion:nil];
     */
}
-(IBAction)backregtowel:(id)sender{
    
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
    [self presentViewController:vc animated:false completion:nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    arr_img_merchants =[[NSMutableArray alloc]init];
    arr_voucherid = [[NSMutableArray alloc]init];
    arr_bcode = [[NSMutableArray alloc]init];
    arr_voucherdesc = [[NSMutableArray alloc]init];
   arrenable = [[NSMutableArray alloc]init];
    if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
        
    }
    else{
        
        [_col_spend setTransform:CGAffineTransformMakeScale(-1, 1)];
    }
    // Do any additional setup after loading the view.
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([UIScreen mainScreen].bounds.size.height==568) {
        return CGSizeMake(115, 115);
    }
    else if(([UIScreen mainScreen].bounds.size.height==667) ||([UIScreen mainScreen].bounds.size.height==812) ){
        return CGSizeMake(136, 136);
    }
    else if([UIScreen mainScreen].bounds.size.height==736){
        return CGSizeMake(142, 142);
    }
    else{
        
        return CGSizeMake(136, 136);
        
    }
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    UIEdgeInsets insets=UIEdgeInsetsMake(0, 10, 0, 10);
    return insets;
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
    return 1;
    
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return [arr_img_merchants count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionVW cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    SpendCollectionViewCell*cell = [collectionVW dequeueReusableCellWithReuseIdentifier:@"SpendCollectionViewCell" forIndexPath:indexPath  ];
    [cell.img_mer sd_setImageWithURL:[NSURL URLWithString:[arr_img_merchants objectAtIndex:indexPath.item]]
                placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
   cell.layer.borderWidth= 0.7;
   cell.layer.borderColor=[UIColor colorWithRed:192/255.0f green:192/255.0f blue:192/255.0f alpha:1].CGColor;
   cell.layer.cornerRadius=0;
    if ([[arrenable objectAtIndex:indexPath.item] intValue] <= [TotalPoints intValue]) {
        cell.userInteractionEnabled =YES;
        cell.img_disable.hidden=YES;
    }
    else{
        cell.userInteractionEnabled=NO;
        cell.img_disable.hidden=NO;
    }
    if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
        
    }
    else{
        
        [cell.contentView setTransform:CGAffineTransformMakeScale(-1, 1)];
    }
   
    return  cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    SpendmerchantViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"SpendmerchantViewController"];
    vc.strVoucherId= [arr_voucherid objectAtIndex:indexPath.item];
    vc.strBrandCode= [arr_bcode objectAtIndex:indexPath.item];
    vc.straltId=@"0";
    vc.strvdesc = [arr_voucherdesc objectAtIndex:indexPath.item];
    
     [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@,%@,%@",vc.straltId,vc.strBrandCode,vc.strVoucherId] forKey:@"brandseldata"];
    
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"ios  Spend Point 1"];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Brand click"
                                                          action:@"button_press"
                                                           label:vc.strBrandCode
                                                           value:nil] build]];
    
    [self presentViewController:vc animated:false completion:nil];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)select_sidemenu:(id)sender {
    
    if (slideViewMenu.view) {
        [self HideSlideMenu];
    } else {
        slideViewMenu = [[UtillClass sharedInstance] CallSlideMenuView:self yVal:0.0f];
    }
}

#pragma mark - slideView Delegate

-(void) slideViewControllerDone:(SlidingViewController *)sender
{
    if (sender) {
        [self HideSlideMenu];
    }
}
-(void) HideSlideMenu
{
    [UIView animateWithDuration:0.50 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        //code with animation
        // slideViewMenu.view.frame = CGRectMake(-self.view.frame.size.width, 0.0f, self.view.frame.size.width, self.view.frame.size.height - 0.0f);
        [slideViewMenu.view removeFromSuperview];
    } completion:^(BOOL finished) {
        //code for completion
        [slideViewMenu.view removeFromSuperview];
        slideViewMenu = nil;
        
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
