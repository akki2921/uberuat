//
//  SpendCollectionViewCell.h
//  Uber Reward
//
//  Created by Arvind Seth on 24/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpendCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img_mer;

@property (strong, nonatomic) IBOutlet UIImageView *img_disable;

@end
