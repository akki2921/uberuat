//
//  SpendVoucherViewController.m
//  Uber Reward
//
//  Created by Arvind Seth on 24/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import "SpendVoucherViewController.h"
#import "SpendVoucherViewController.h"
@interface SpendVoucherViewController ()

@end

@implementation SpendVoucherViewController
@synthesize strurl;
-(void)viewDidAppear:(BOOL)animated {
    [[UtillClass sharedInstance]HideLoader];
    [self HideSlideMenu];
    [[UtillClass sharedInstance] showbottom_items];
    
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"ios  Spend Voucher"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
}
-(IBAction)backregtowel:(id)sender{
    
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
    [self presentViewController:vc animated:false completion:nil];
}
-(IBAction)emailvoucher:(id)sender{
    [self.view endEditing:YES];
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"ios  Spend Voucher"];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Email voucher"
                                                          action:@"button_press"
                                                           label:[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"Login Details"]objectForKey:@"CustomerId"]
                                                           value:nil] build]];
    
    if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        };
        
    }
    else{
        [[UtillClass sharedInstance] ShowLoaderOn:self.parentViewController withtext:@"Please wait"];
        
        [[UtillClass sharedInstance] getresponsefromservice:[NSString stringWithFormat:@"YouGotaGiftAPI/Get_Email_Voucher?CultureId=%@&CustomerId=%@&altId=0&PdfUrl=%@",[[UtillClass sharedInstance] getcultureID],[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"Login Details"]objectForKey:@"CustomerId"],strurl] withbearer:[[UtillClass sharedInstance] getapp_sessiontoken] response:^(NSDictionary * dict_response) {
            [[UtillClass sharedInstance] HideLoader];
            if (dict_response ==nil) {
                [[UtillClass sharedInstance] showerrortoastmsg:@"Server error, Please try after sometime."];
            }
            else{
                if ([[dict_response objectForKey:@"Status"] integerValue] == 1) {
                    
                [[UtillClass sharedInstance] showtoastmsg:[dict_response objectForKey:@"msg"]];
                    
                }
                else{
                    [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
                }
            }
        }];
    }
    
    
    /*
     UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"RegEmailViewController"];
     [self presentViewController:vc animated:false completion:nil];
     */
}

-(IBAction)smsvoucher:(id)sender{
    [self.view endEditing:YES];
    if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        };
        
    }
    else{
        [[UtillClass sharedInstance] ShowLoaderOn:self.parentViewController withtext:@"Please wait"];
        
        [[UtillClass sharedInstance] getresponsefromservice:[NSString stringWithFormat:@"YouGotaGiftAPI/Get_SMS_Voucher?CultureId=%@&CustomerId=%@&altId=0&PdfUrl=%@",[[UtillClass sharedInstance] getcultureID],[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"Login Details"]objectForKey:@"CustomerId"],strurl] withbearer:[[UtillClass sharedInstance] getapp_sessiontoken] response:^(NSDictionary * dict_response) {
            [[UtillClass sharedInstance] HideLoader];
            if (dict_response ==nil) {
                [[UtillClass sharedInstance] showerrortoastmsg:@"Server error, Please try after sometime."];
            }
            else{
                if ([[dict_response objectForKey:@"Status"] integerValue] == 1) {
                   
                    [[UtillClass sharedInstance] showtoastmsg:[dict_response objectForKey:@"msg"]];
                    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
                    [tracker set:kGAIScreenName value:@"ios  Spend Voucher"];
                    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"SMS Voucher"
                                                                          action:@"button_press"
                                                                           label:[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"Login Details"]objectForKey:@"CustomerId"]
                                                                           value:nil] build]];
                }
                else{
                    [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
                }
            }
        }];
    }
    
    
    /*
    
     */
}
- (IBAction)select_sidemenu:(id)sender {
    
    if (slideViewMenu.view) {
        [self HideSlideMenu];
    } else {
        slideViewMenu = [[UtillClass sharedInstance] CallSlideMenuView:self yVal:0.0f];
    }
}

#pragma mark - slideView Delegate

-(void) slideViewControllerDone:(SlidingViewController *)sender
{
    if (sender) {
        [self HideSlideMenu];
    }
}
-(void) HideSlideMenu
{
    [UIView animateWithDuration:0.50 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        //code with animation
        // slideViewMenu.view.frame = CGRectMake(-self.view.frame.size.width, 0.0f, self.view.frame.size.width, self.view.frame.size.height - 0.0f);
        [slideViewMenu.view removeFromSuperview];
    } completion:^(BOOL finished) {
        //code for completion
        [slideViewMenu.view removeFromSuperview];
        slideViewMenu = nil;
        
    }];
}
- (UIViewController*)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller
{
    return self;
}
-(IBAction)viewVoucher:(id)sender{
 // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strurl]];
    UIButton *btn = (UIButton *) sender;
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"ios  Spend Voucher"];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"View Voucher"
                                                          action:@"button_press"
                                                           label:[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"Login Details"]objectForKey:@"CustomerId"]
                                                           value:nil] build]];
    //NSString *urlStr = [NSString stringWithFormat:@"%@",[[Array_TableData objectAtIndex:btn.tag] valueForKey:@"VoucherURL"]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strurl]];
    
    /*
    NSArray *arr=[strurl componentsSeparatedByString:@"/"];
    
    NSURL* url = [[NSURL alloc] initWithString:strurl];
    
    NSURL* documentsUrl = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask].firstObject;
    NSURL* destinationUrl = [documentsUrl URLByAppendingPathComponent:[arr objectAtIndex:[arr count]-1]];
    
    // Actually download
    NSError* err = nil;
    NSData* fileData = [[NSData alloc] initWithContentsOfURL:url options:NSDataReadingUncached error:&err];
    if (!err && fileData && fileData.length && [fileData writeToURL:destinationUrl atomically:true])
    {
        // Downloaded and saved, now present the document controller (store variable, or risk garbage collection!)
        UIDocumentInteractionController* document = [UIDocumentInteractionController interactionControllerWithURL:destinationUrl];
        document.delegate = self;
        [document presentPreviewAnimated:YES];
    }
    */
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
