//
//  SpendmerchantViewController.h
//  Uber Reward
//
//  Created by Arvind Seth on 24/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UtillClass.h"
#import "SlidingViewController.h"
#import "NIDropDown.h"
@interface SpendmerchantViewController : UIViewController<slideViewControllerDelegate,NIDropDownDelegate>{
    
    SlidingViewController*slideViewMenu;
    NSMutableArray *arrdropdown;
    NIDropDown *dropDown;
    NSString*stringcontry;
    NSDictionary *Dictmxmin;
}
@property (strong, nonatomic) IBOutlet UIButton *btn_confsupport;
@property (strong, nonatomic) IBOutlet UIButton *btn_confirm;
@property (strong, nonatomic) IBOutlet UIView *view_permiss;
@property (strong, nonatomic) IBOutlet UIButton *btn_selamo;
@property (strong, nonatomic) IBOutlet UIImageView *img_drop;
@property (strong, nonatomic) IBOutlet UILabel *lbl_pointbalance;
@property (strong, nonatomic) IBOutlet UITextField *tf_merchantname;
@property (strong, nonatomic) IBOutlet UITextField *tf_val;
@property (strong, nonatomic) IBOutlet UIImageView *img_selection;

@property (strong, nonatomic) IBOutlet UITextField *tf_selectamount;
@property (strong, nonatomic) IBOutlet UITextField *lbl_amountmain;
@property(strong, nonatomic) NSString *strVoucherId;
@property(strong, nonatomic) NSString *strBrandCode;
@property(strong, nonatomic) NSString *strvdesc;
@property(strong, nonatomic) NSString *straltId;
@property (strong, nonatomic) IBOutlet UIButton *btn_check;
@property (strong, nonatomic) IBOutlet UIButton *btn_denot;
@property (strong, nonatomic) IBOutlet UIView *view_terms;
@property (strong, nonatomic) IBOutlet UITextView *lbl_viewterms;
@property (strong, nonatomic) IBOutlet UITextView *lbl_viewvoucher;

@property (strong, nonatomic) IBOutlet UIButton *btn_voucher;
@property (strong, nonatomic) IBOutlet UIButton *btn_terms;
@property (strong, nonatomic) IBOutlet UIButton *btn_location;
@property (strong, nonatomic) IBOutlet UIView *view_voucher;

@end
