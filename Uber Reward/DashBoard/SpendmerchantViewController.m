//
//  SpendmerchantViewController.m
//  Uber Reward
//
//  Created by Arvind Seth on 24/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import "SpendmerchantViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "noLOginWebViewViewController.h"
#import "SpendVoucherViewController.h"
@interface SpendmerchantViewController ()



@end

@implementation SpendmerchantViewController
@synthesize straltId,strBrandCode,strVoucherId,strvdesc;

-(void)viewDidAppear:(BOOL)animated {
    [[UtillClass sharedInstance]HideLoader];
    [self HideSlideMenu];
    [[UtillClass sharedInstance] showbottom_items];
    
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"ios  Spend Merchant"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"brandseldata"];
    
    if (!strVoucherId) {
        strVoucherId= [[str componentsSeparatedByString:@","] objectAtIndex:2];
        strBrandCode= [[str componentsSeparatedByString:@","] objectAtIndex:1];
        straltId= [[str componentsSeparatedByString:@","] objectAtIndex:0];
    }
    
    [self PostYougotaGiftVoucher];
    
}

- (IBAction)okvoucher:(id)sender {
    _view_terms.hidden=YES;
    _view_voucher.hidden=YES;
}

- (IBAction)okterms:(id)sender {
    _view_terms.hidden=YES;
    _view_voucher.hidden=YES;
}

- (IBAction)locatons:(id)sender {
}

- (IBAction)terms:(id)sender {
    _view_terms.hidden=NO;
    _view_voucher.hidden=YES;
    //_lbl_viewterms.text= [NSString stringWithFormat:@"%@ %@"]
    
}

- (IBAction)aboutvoucher:(id)sender {
    _view_terms.hidden=YES;
    _view_voucher.hidden=NO;
    
}
-(IBAction)backregtowel:(id)sender{
    
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"SpedPontViewController"];
    [self presentViewController:vc animated:false completion:nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    arrdropdown = [[NSMutableArray alloc]init];
    
    
    if([UIScreen mainScreen].bounds.size.height==812 ){
        [_img_selection setFrame:CGRectMake(_img_selection.frame.origin.x, _img_selection.frame.origin.y, 130, 127)];
       [_btn_confirm setFrame:CGRectMake(_btn_confirm.frame.origin.x, _btn_confirm.frame.origin.y, _btn_confirm.frame.size.width, 38)];
         [_btn_confsupport setFrame:CGRectMake(_btn_confsupport.frame.origin.x, _btn_confirm.frame.origin.y, _btn_confsupport.frame.size.width, 38)];
        [_img_drop setFrame:CGRectMake(_img_drop.frame.origin.x, _img_drop.frame.origin.y, _img_drop.frame.size.width, 20)];
    }
    _tf_merchantname.layer.borderColor=[UIColor colorWithRed:192/255.0f green:192/255.0f blue:192/255.0f alpha:1].CGColor;
    _tf_val.layer.borderWidth= 1;
    _tf_val.layer.borderColor=[UIColor colorWithRed:192/255.0f green:192/255.0f blue:192/255.0f alpha:1].CGColor;
    _tf_merchantname.layer.borderWidth= 1;
    
    _tf_val.layer.cornerRadius=1;
    _tf_merchantname.layer.cornerRadius=1;
    
    _img_selection.layer.borderWidth= 0;
    _img_selection.layer.borderColor=[UIColor colorWithRed:192/255.0f green:192/255.0f blue:192/255.0f alpha:1].CGColor;
  _img_selection.layer.cornerRadius=1;
    
    _lbl_amountmain.layer.borderWidth= 1;
    _lbl_amountmain.layer.borderColor=[UIColor colorWithRed:192/255.0f green:192/255.0f blue:192/255.0f alpha:1].CGColor;
    _lbl_amountmain.layer.cornerRadius=1;
    
    _btn_terms.layer.borderWidth= 0.7;
    _btn_terms.layer.borderColor=[UIColor colorWithRed:198/255.0f green:54/255.0f blue:74/255.0f alpha:1].CGColor;
    _btn_terms.layer.cornerRadius=1;
    
    _btn_voucher.layer.borderWidth= 0.7;
    _btn_voucher.layer.borderColor=[UIColor colorWithRed:198/255.0f green:54/255.0f blue:74/255.0f alpha:1].CGColor;
    _btn_voucher.layer.cornerRadius=1;
    
    _btn_location.layer.borderWidth= 0.0;
    _btn_location.layer.borderColor=[UIColor colorWithRed:198/255.0f green:54/255.0f blue:74/255.0f alpha:1].CGColor;
    _btn_location.layer.cornerRadius=1;
    
    if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
        _lbl_pointbalance.text=[NSString stringWithFormat:@"Your Points balance is %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"BalancePoints"]];
    }
    else{
       _lbl_pointbalance.text=[NSString stringWithFormat:@" رصيد نقاطك هو%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"BalancePoints"]];
        _tf_selectamount.placeholder=@"يرجى اختيار القيمة";
        _lbl_amountmain.placeholder=@"النقاط";
        
    }
   
    
    // Do any additional setup after loading the view.
}

-(void)PostYougotaGiftVoucher{
    [self.view endEditing:YES];
    if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        };
        
    }
    else{
        [[UtillClass sharedInstance] ShowLoaderOn:self.parentViewController withtext:@"Please wait"];
        NSMutableDictionary * dict = [[NSMutableDictionary alloc]init];
        [dict setValue:@"1" forKey:@"quantity"];
        
        [[UtillClass sharedInstance] Postresponsetoservice:dict withBearer:[[UtillClass sharedInstance] getapp_sessiontoken] withurl:[NSString stringWithFormat:@"YouGotaGiftAPI/YougotaGiftVoucher?CustomerId=%@&CultureId=%@&VoucherId=%@&BrandCode=%@&altId=0",[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"Login Details"]objectForKey:@"CustomerId"],[[UtillClass sharedInstance]getcultureID],strVoucherId,strBrandCode] response:^(NSDictionary * dict_response) {
            [[UtillClass sharedInstance] HideLoader];
            if (dict_response ==nil) {
                [[UtillClass sharedInstance] showerrortoastmsg:@"Server error, Please try after sometime."];
            }
            else{
                if ([[dict_response objectForKey:@"Status"] integerValue] == 1) {
                    [[UtillClass sharedInstance]  setUserdefaultofDict:dict_response withKayname:@"getAvailableRedemtionsMobile"];
                    NSArray *Arr = [dict_response objectForKey:@"brands"];
                    if ([[[[Arr objectAtIndex:0] objectForKey:@"denominations"] objectForKey:[[Arr objectAtIndex:0] objectForKey:@"brand_accepted_currency"]] isKindOfClass:[NSDictionary class]]) {
                        
                        Dictmxmin = [[[Arr objectAtIndex:0] objectForKey:@"denominations"] objectForKey:[[Arr objectAtIndex:0] objectForKey:@"brand_accepted_currency"]];
                        if ([Dictmxmin objectForKey:@"max"]) {
                            _tf_selectamount.userInteractionEnabled=YES;
                            if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                                _tf_selectamount.placeholder =@"Enter Amount";
                                _btn_selamo.hidden=YES;
                                _img_drop.hidden=YES;
                            }
                            else{
                                _tf_selectamount.placeholder =@"يرجى ادخال القيمة";
                                _btn_selamo.hidden=YES;
                                _img_drop.hidden=YES;
                            }
                            
                        }
                    }
                    else{
                        _tf_selectamount.userInteractionEnabled=NO;
                        _btn_selamo.hidden=NO;
                        _img_drop.hidden=NO;
                        
                    NSArray *arrs= [[[Arr objectAtIndex:0] objectForKey:@"denominations"] objectForKey:[[Arr objectAtIndex:0] objectForKey:@"brand_accepted_currency"]];
                    for (int i =0 ; i<[arrs count]; i++) {
                        NSString *strbalp= [[[NSUserDefaults standardUserDefaults] objectForKey:@"BalancePoints"] stringByReplacingOccurrencesOfString:@"," withString:@""];
                        if (![[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                        
                            NSNumberFormatter *formatter = [NSNumberFormatter new];
                            formatter.locale = [NSLocale localeWithLocaleIdentifier:@"ar"];
                            
                            for (NSInteger i = 0; i < 10; i++) {
                                NSNumber *num = @(i);
                                strbalp = [strbalp stringByReplacingOccurrencesOfString:[formatter stringFromNumber:num] withString:num.stringValue];
                            }
                            
                           
                            NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"٬"];
                            strbalp = [[strbalp componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString: @""];
                         
                        }
                        
                        NSString *strh =[arrs objectAtIndex:i];
                        
                        if ([strh integerValue]*80<=[strbalp integerValue] ) {
                           [arrdropdown addObject:[NSString stringWithFormat:@"%@.00",[arrs objectAtIndex:i]]];
                        }
                        
                    }
                    }
                    _tf_merchantname.text = [[Arr objectAtIndex:0] objectForKey:@"name"];
                    
                    [_img_selection sd_setImageWithURL:[NSURL URLWithString:[[Arr objectAtIndex:0] objectForKey:@"logo"]]
                                    placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
                    [_btn_denot setTitle:[NSString stringWithFormat:@"%@",[[Arr objectAtIndex:0] objectForKey:@"brand_accepted_currency"]] forState:UIControlStateNormal] ;
                    _lbl_viewterms.text=[NSString stringWithFormat:@"%@",[[Arr objectAtIndex:0] objectForKey:@"redemption_instructions"]];
                    _lbl_viewvoucher.text=[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",[[Arr objectAtIndex:0] objectForKey:@"description"]]];
                    stringcontry=[[[Arr objectAtIndex:0] objectForKey:@"country"] objectForKey:@"code"];
                }
                else{
                    [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
                }
            }
        }];
    }
    
    
    /*
     UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"RegEmailViewController"];
     [self presentViewController:vc animated:false completion:nil];
     */
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
   return  [textField resignFirstResponder];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (range.length > 0)
    {// We're deleting
//         _lbl_amountmain.text = [NSString stringWithFormat:@"%ld",[[textField.text substringFromIndex:(textField.text.length -1)] integerValue]*80];
 
        NSString *tmpstr= _tf_selectamount.text;
        if (range.location==0) {

              _lbl_amountmain.text=@"";
            
        }
        else{
           _lbl_amountmain.text = [NSString stringWithFormat:@"%ld",[[tmpstr substringToIndex:(tmpstr.length -1)] integerValue]*80];
        }
        return YES;
    }
   
    
    

    NSString *numberRegEx = @"[0-9]";
    NSPredicate *testRegex = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegEx];
    
    if(![testRegex evaluateWithObject:string]){
        return NO;
    }
    if (textField.text.length >= 7) {
        return NO;
    }
    NSString *Strtfval=string;
    NSString *strmax =[Dictmxmin objectForKey:@"max"];
    NSString *strmin =[Dictmxmin objectForKey:@"min"];
 
        if ([Strtfval integerValue] > [strmax integerValue]) {
          //  return NO;
        }
       if ([Strtfval integerValue] < [strmin integerValue]) {
       // return NO;
    }
    
    _lbl_amountmain.text = [NSString stringWithFormat:@"%ld",[[textField.text stringByAppendingString:string] integerValue]*80];
    
    
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    textField.placeholder = @"";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)select_sidemenu:(id)sender {
    
    if (slideViewMenu.view) {
        [self HideSlideMenu];
    } else {
        slideViewMenu = [[UtillClass sharedInstance] CallSlideMenuView:self yVal:0.0f];
    }
}

#pragma mark - slideView Delegate

-(void) slideViewControllerDone:(SlidingViewController *)sender
{
    if (sender) {
        [self HideSlideMenu];
    }
}
-(void) HideSlideMenu
{
    [UIView animateWithDuration:0.50 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        //code with animation
        // slideViewMenu.view.frame = CGRectMake(-self.view.frame.size.width, 0.0f, self.view.frame.size.width, self.view.frame.size.height - 0.0f);
        [slideViewMenu.view removeFromSuperview];
    } completion:^(BOOL finished) {
        //code for completion
        [slideViewMenu.view removeFromSuperview];
        slideViewMenu = nil;
        
        
    }];
}
- (IBAction)action_select_ccode:(id)sender {
    [self OpenDropDown:sender Array:[arrdropdown copy]];
    
}

- (void) niDropDownDelegateMethod: (NSString *)btntext withbtntag:(NSString *)btntag dropdown: (NIDropDown *) sender {
    dropDown = nil;
    
    if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
        
    
    _tf_selectamount.text =[[NSString stringWithFormat:@"%@", [[btntext componentsSeparatedByString:@","]objectAtIndex:0]] substringFromIndex:1] ;
    }else{
        
     
        
        _tf_selectamount.text =[[NSString stringWithFormat:@"%@", [[btntext componentsSeparatedByString:@","]objectAtIndex:0]] substringToIndex:[[btntext componentsSeparatedByString:@","]objectAtIndex:0].length-1] ;
    }
    
    
    
    _lbl_amountmain.text = [NSString stringWithFormat:@"%ld",[_tf_selectamount.text integerValue]*80];

    
}
-(IBAction)select_submit:(id)sender{
    
    [self.view endEditing:YES];
    NSString *strmax =[Dictmxmin objectForKey:@"max"];
    NSString *strmin =[Dictmxmin objectForKey:@"min"];
    
  NSString *balpoint=  [[NSUserDefaults standardUserDefaults] objectForKey:@"BalancePoints"];
    if (![[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
        
        NSNumberFormatter *formatter = [NSNumberFormatter new];
        formatter.locale = [NSLocale localeWithLocaleIdentifier:@"ar"];
        
        for (NSInteger i = 0; i < 10; i++) {
            NSNumber *num = @(i);
            balpoint = [balpoint stringByReplacingOccurrencesOfString:[formatter stringFromNumber:num] withString:num.stringValue];
        }
        
        
        NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"٬"];
        balpoint = [[balpoint componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString: @""];
        
    }
    if (!_lbl_amountmain.text.length) {
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
           [[UtillClass sharedInstance] showerrortoastmsg:@"Select Amount"];
        }
        else{
           [[UtillClass sharedInstance] showerrortoastmsg:@"يرجى اختيار القيمة"];
        }
       
    }
   
    else if ([_lbl_amountmain.text integerValue] > [[balpoint stringByReplacingOccurrencesOfString:@"," withString:@""] integerValue]) {
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
          [[UtillClass sharedInstance] showerrortoastmsg:[NSString stringWithFormat:@"You do not have sufficient Points"]];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"ليس لديك نقاط كافية"];
        }
        
        
    }
    else if (strmax!=nil) {
        
        if ([_tf_selectamount.text integerValue]%10!=0) {
            
       
            if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
             [[UtillClass sharedInstance] showerrortoastmsg:[NSString stringWithFormat:@"Enter amount in multiples of 10"]];
            }
            else{
                [[UtillClass sharedInstance] showerrortoastmsg:[NSString stringWithFormat:@"أدخل القيمة من مضاعفات ال 10"]];
            }
            
        }
   else if ([_lbl_amountmain.text integerValue] > [strmax integerValue]*80) {
       
      if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
           [[UtillClass sharedInstance] showerrortoastmsg:[NSString stringWithFormat:@"You have entered an invalid amount. Redeem for %ld %@ and below",(long)[strmax integerValue],_btn_denot.titleLabel.text]];
       }
       else{
           [[UtillClass sharedInstance] showerrortoastmsg:[NSString stringWithFormat:@"لقد قمت بإدخال مبلغ غير صحيح. الاستبدال لـ . النقاط فما فوق %ld  %@ وما تحت",(long)[strmax integerValue],_btn_denot.titleLabel.text]];
       }
       
       
    }
 else if ([_lbl_amountmain.text integerValue] < [strmin integerValue]*80) {
     
     
     if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
         [[UtillClass sharedInstance] showerrortoastmsg:[NSString stringWithFormat:@"Please enter minimum %ld %@ to redeem",(long)[strmin integerValue],_btn_denot.titleLabel.text]];
     }
     else{
        [[UtillClass sharedInstance] showerrortoastmsg:[NSString stringWithFormat:@"يرجى ادخال %ld %@ سعودي على الأقل للاستبدال",(long)[strmin integerValue],_btn_denot.titleLabel.text]];
     }
     
   
     
    }
 else if (_btn_check.tag==1) {
     
     if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
        [[UtillClass sharedInstance] showerrortoastmsg:@"Please accept the Uber PLATINUM Terms & Conditions"];
     }
     else{
          [[UtillClass sharedInstance] showerrortoastmsg:@"أقبل شروط وأحكام برنامج أوبرللمكافآت"];
     }
     
     
    
 }
        else{
            _view_permiss.hidden=NO;
        }
        
    }
    
  else if (_btn_check.tag==1) {
      if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
          [[UtillClass sharedInstance] showerrortoastmsg:@"Please accept the Uber PLATINUM Terms & Conditions"];
      }
      else{
          [[UtillClass sharedInstance] showerrortoastmsg:@"أقبل شروط وأحكام برنامج أوبرللمكافآت"];
      }
    }
  else{
    if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        }
       
        
    }
    
      else{
          _view_permiss.hidden=NO;
      }}
}
#pragma mark - DropDown Delegate
- (void)OpenDropDown:(UIButton *)sender Array:(NSArray *) arr
{
    [self.view endEditing:YES];
    //    arr = [[NSArray alloc] init];
    
    NSArray * arrImage = [[NSArray alloc] init];
    
   
    
    
    if(dropDown == nil) {
        CGFloat f = 200;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :arrImage :@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        dropDown = nil;
        
    }
}
- (IBAction)select_check:(id)sender {
    if (_btn_check.tag==1) {
        [_btn_check setBackgroundImage:[UIImage imageNamed:@"checkbox-tick.png"] forState:UIControlStateNormal];
        _btn_check.tag=2;
    }
    else{
        _btn_check.tag=1;
       [_btn_check setBackgroundImage:[UIImage imageNamed:@"checkbox.png"] forState:UIControlStateNormal];
    }
}
- (IBAction)TandC:(id)sender {
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    app.ViewWebTypeStr = @"T&C";
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"ios  Spend Merchant"];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Spend Merchant"
                                                          action:@"T & C action"
                                                           label:[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"Login Details"]objectForKey:@"CustomerId"]
                                                           value:nil] build]];
    
    noLOginWebViewViewController *controller = [[noLOginWebViewViewController alloc] initWithNibName:[[UtillClass sharedInstance] getnologinxib] bundle:nil];
    [self presentViewController:controller animated:false completion:nil];
}
- (IBAction)clickyes:(id)sender {
    [[UtillClass sharedInstance] ShowLoaderOn:self.parentViewController withtext:@"Please wait"];
    
    NSDictionary *dicttemp = [[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"getAvailableRedemtionsMobile"];
    
    NSMutableDictionary * dict = [[NSMutableDictionary alloc]init];
    [dict setValue:@"" forKey:@"reference_id"];
    [dict setValue:[NSNumber numberWithInt:1] forKey:@"delivery_type"];
    [dict setValue:strBrandCode forKey:@"brand_code"];
    [dict setValue:_btn_denot.titleLabel.text forKey:@"currency"];
    [dict setValue:[NSNumber numberWithDouble:[_tf_selectamount.text doubleValue]] forKey:@"amount"];
    [dict setValue:stringcontry forKey:@"country"];
    [dict setValue:@"" forKey:@"receiver_name"];
    [dict setValue:@"" forKey:@"receiver_email"];
    [dict setValue:@"" forKey:@"receiver_phone"];
    
    
    [[UtillClass sharedInstance] Postresponsetoservice:dict withBearer:[[UtillClass sharedInstance] getapp_sessiontoken] withurl:[NSString stringWithFormat:@"YouGotaGiftAPI/YougotaGiftRedeemVoucher?CustomerId=%@&CultureId=%@&VoucherId=%@&BrandCode=%@&altId=0",[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"Login Details"]objectForKey:@"CustomerId"],[[UtillClass sharedInstance]getcultureID],strVoucherId,strBrandCode] response:^(NSDictionary * dict_response) {
        [[UtillClass sharedInstance] HideLoader];
        if (dict_response ==nil) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Server error, Please try after sometime."];
        }
        else{
            if ([[dict_response objectForKey:@"Status"] integerValue] == 1) {
                
                SpendVoucherViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"SpendVoucherViewController"];
                vc.strurl=[dict_response objectForKey:@"pdf_link"];
                [self presentViewController:vc animated:false completion:nil];
                
                int n = [_tf_selectamount.text doubleValue];
                int y = [[[NSUserDefaults standardUserDefaults] valueForKey:@"BalancePoints"] intValue];
                NSString *stry= [NSString stringWithFormat:@"%d",y-n];
                
                [[NSUserDefaults standardUserDefaults] setValue:[[UtillClass sharedInstance] getpointformatofint:[stry intValue]] forKey:@"BalancePoints"];
                
                NSError * err;
                NSData * jsonDataAnly = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
                NSString * myStringAnly = [[NSString alloc] initWithData:jsonDataAnly   encoding:NSUTF8StringEncoding];
                
                id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
                [tracker set:kGAIScreenName value:@"ios  Spend Merchant"];
                [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Spend Merchant"
                                                                      action:@"redeem Points action"
                                                                       label:myStringAnly
                                                                       value:nil] build]];
                
                 [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
            }
            else{
                [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
            }
        }
    }];
}
- (IBAction)clickNo:(id)sender {
     _view_permiss.hidden=YES;
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"ios  Spend Merchant"];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Spend Merchant"
                                                          action:@"Return without Redeem points"
                                                           label:[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"Login Details"]objectForKey:@"CustomerId"]
                                                           value:nil] build]];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
