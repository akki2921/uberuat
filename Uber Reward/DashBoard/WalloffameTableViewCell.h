//
//  WalloffameTableViewCell.h
//  Uber Reward
//
//  Created by Arvind Seth on 22/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WalloffameTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lbl_header;
@property (strong, nonatomic) IBOutlet UILabel *lbl_subheader;
@property (strong, nonatomic) IBOutlet UIImageView *img_prof;
@property (strong, nonatomic) IBOutlet UIImageView *img_badge;
@property (strong, nonatomic) IBOutlet UILabel *lbl_bottom;
@property (strong, nonatomic) IBOutlet UILabel *lbl_line;
@end
