//
//  WalloffameViewController.h
//  Uber Reward
//
//  Created by Arvind Seth on 22/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UtillClass.h"
#import "SlidingViewController.h"
#import "DatePickerViewController.h"
@interface WalloffameViewController : UIViewController<slideViewControllerDelegate,myDatePickerViewControllerDelegate>

{
     DatePickerViewController *datePickerView;
    SlidingViewController *slideViewMenu;
    NSMutableArray *arrheader;
    NSMutableArray *arrsubheader;
    NSMutableArray *arrimgprof;
    NSMutableArray*arrimgbadge;
    NSMutableArray *arrbottom;
    float height;
    NSMutableArray *arr_month;
    NSString* m ;
    IBOutlet UIButton* btn_filter;
    NSMutableArray*arr_wecel;
    
}
@property (strong, nonatomic) IBOutlet UILabel *lbl_headingwof;


@property (strong, nonatomic) IBOutlet UITableView *tbl_walloffame;
@property (strong, nonatomic) IBOutlet UIImageView *img_wof;
@property (strong, nonatomic) IBOutlet UIScrollView *scr_wof;

@end
