//
//  WalloffameViewController.m
//  Uber Reward
//
//  Created by Arvind Seth on 22/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import "WalloffameViewController.h"
#import "WalloffameTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface WalloffameViewController ()

@end

@implementation WalloffameViewController
-(void)viewDidAppear:(BOOL)animated {
    [[UtillClass sharedInstance]HideLoader];
    [self HideSlideMenu];
    [[UtillClass sharedInstance] showbottom_items];
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"ios  Wall of Fame"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
   [self GetWalloffame];
}
- (IBAction)select_sidemenu:(id)sender {
    
    if (slideViewMenu.view) {
        [self HideSlideMenu];
    } else {
        slideViewMenu = [[UtillClass sharedInstance] CallSlideMenuView:self yVal:0.0f];
    }
}
- (IBAction)select_back:(id)sender{
  [[UtillClass sharedInstance] hidebottom_items];
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
    [self presentViewController:vc animated:false completion:nil];
    
    
}
-(void)GetWalloffame{
    [self.view endEditing:YES];
    if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        };
        
    }
    else{
        [[UtillClass sharedInstance] ShowLoaderOn:self.parentViewController withtext:@"Please wait"];
        
        [[UtillClass sharedInstance] getresponsefromservice:[NSString stringWithFormat:@"Account/GetWallofFame?CultureId=%@&CustomerId=%@&altId=0&month=-1&year=-1",[[UtillClass sharedInstance] getcultureID],[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"Login Details"]objectForKey:@"CustomerId"]] withbearer:[[UtillClass sharedInstance] getapp_sessiontoken] response:^(NSDictionary * dict_response) {
            [[UtillClass sharedInstance] HideLoader];
            if (dict_response ==nil) {
                [[UtillClass sharedInstance] showerrortoastmsg:@"Server error, Please try after sometime."];
                
            }
            else{
                if ([[dict_response objectForKey:@"Status"] integerValue] == 1) {
                    
                    NSArray *Arr=[dict_response objectForKey:@"WallofFame"];
                    NSString *strmt=[dict_response objectForKey:@"MonthTitle"];
                    if (Arr.count==0) {
                        _lbl_headingwof.text = [dict_response objectForKey:@"TitleWithoutData"];
                        
                    }
                    else{
                        _lbl_headingwof.text = [dict_response objectForKey:@"TitlehavingData"];
                        float f1 =[self getLabelHeight:_lbl_headingwof];
                        [_lbl_headingwof setFrame:CGRectMake(_lbl_headingwof.frame.origin.x, _lbl_headingwof.frame.origin.y+5, _lbl_headingwof.frame.size.width, f1)];
                        [_tbl_walloffame setFrame:CGRectMake(_tbl_walloffame.frame.origin.x, _lbl_headingwof.frame.origin.y+f1+20, _tbl_walloffame.frame.size.width, _tbl_walloffame.frame.size.height+42)];
                        
                        
                        for (int i=0; i<[Arr count]; i++) {
                            
                            NSString * strhe =[NSString stringWithFormat:@"%@",strmt];
                            strhe =[strhe stringByReplacingOccurrencesOfString:@"[Year] [Month]" withString:[NSString stringWithFormat:@"%@ %@",[[Arr objectAtIndex:i] objectForKey:@"Year"],[arr_month objectAtIndex:[[[Arr objectAtIndex:i] objectForKey:@"Month"] integerValue]-1]]];
                            
                            strhe =[strhe stringByReplacingOccurrencesOfString:@"[Month] [Year]" withString:[NSString stringWithFormat:@"%@ %@",[arr_month objectAtIndex:[[[Arr objectAtIndex:i] objectForKey:@"Month"] integerValue]-1],[[Arr objectAtIndex:i] objectForKey:@"Year"]]];
                            
                            
                            [arrheader addObject:strhe];
                            
                            [arrsubheader addObject:[NSString stringWithFormat:@"%@ %@",[[Arr objectAtIndex:i] objectForKey:@"FirstName"],[[Arr objectAtIndex:i] objectForKey:@"LastName"]]];
                            [arrimgbadge addObject:[NSString stringWithFormat:@"%@",[[Arr objectAtIndex:i] objectForKey:@"Tier"]]];
                            
                            [arrimgprof addObject:[NSString stringWithFormat:@"%@",[[Arr objectAtIndex:i] objectForKey:@"UserImage"]]];
                            
                            NSString*textwecel= [NSString stringWithFormat:@"%@",[[Arr objectAtIndex:i] objectForKey:@"AboveImageTemplate"]];
                            textwecel= [textwecel stringByReplacingOccurrencesOfString:@"[FirstName] [LastName]" withString:[arrsubheader objectAtIndex:i]];
                            textwecel= [textwecel stringByReplacingOccurrencesOfString:@"[LastName] [FirstName]" withString:[NSString stringWithFormat:@"%@ %@",[[[arrsubheader objectAtIndex:i] componentsSeparatedByString:@" "]objectAtIndex:1],[[[arrsubheader objectAtIndex:i] componentsSeparatedByString:@" "]objectAtIndex:0]]];
                            
                            textwecel= [textwecel stringByReplacingOccurrencesOfString:@"[Text1]" withString:[[Arr objectAtIndex:i] objectForKey:@"Text1"]];
                            
                            textwecel= [textwecel stringByReplacingOccurrencesOfString:@"[Text2]" withString:[[Arr objectAtIndex:i] objectForKey:@"Text2"]];
                            
                            NSString *text = [NSString stringWithFormat:@"%@",[[Arr objectAtIndex:i] objectForKey:@"Template"]];
                            text= [text stringByReplacingOccurrencesOfString:@"[FirstName] [LastName]" withString:[arrsubheader objectAtIndex:i]];
                            text= [text stringByReplacingOccurrencesOfString:@"[LastName] [FirstName]" withString:[NSString stringWithFormat:@"%@ %@",[[[arrsubheader objectAtIndex:i] componentsSeparatedByString:@" "]objectAtIndex:1],[[[arrsubheader objectAtIndex:i] componentsSeparatedByString:@" "]objectAtIndex:0]]];
                            text= [text stringByReplacingOccurrencesOfString:@"[Trip1]" withString:[[Arr objectAtIndex:i] objectForKey:@"Trip1"]];
                            text= [text stringByReplacingOccurrencesOfString:@"[Trip2]" withString:[[Arr objectAtIndex:i] objectForKey:@"Trip2"]];
                            text= [text stringByReplacingOccurrencesOfString:@"[Trip3]" withString:[[Arr objectAtIndex:i] objectForKey:@"Trip3"]];
                            text= [text stringByReplacingOccurrencesOfString:@"[Trip4]" withString:[[Arr objectAtIndex:i] objectForKey:@"BoldText"]];
                            text= [text stringByReplacingOccurrencesOfString:@"[Month]" withString:[arr_month objectAtIndex:[[[Arr objectAtIndex:i] objectForKey:@"Month"] integerValue]-1]];
                            text= [text stringByReplacingOccurrencesOfString:@"[Year]" withString:[NSString stringWithFormat:@"%@",[[Arr objectAtIndex:i] objectForKey:@"Year"]]];
                            NSDictionary *attribs = @{
                                                      NSForegroundColorAttributeName:[UIColor blackColor],
                                                      NSFontAttributeName: [UIFont fontWithName:@"ClanPro-Book" size:10.6]
                                                      };
                            NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text attributes:attribs];
                            NSRange rangewel = [textwecel rangeOfString:[arrsubheader objectAtIndex:i]];
                            NSRange rangewel2 = [textwecel rangeOfString:[[Arr objectAtIndex:i] objectForKey:@"Text1"]];
                            NSRange rangewel3 = [textwecel rangeOfString:[[Arr objectAtIndex:i] objectForKey:@"Text2"]];
                            UIFont *boldFont = [UIFont fontWithName:@"ClanPro-Medium" size:10.6];
                            NSRange range = [text rangeOfString:[arrsubheader objectAtIndex:i]];
                            NSRange range2  = [text rangeOfString:[[Arr objectAtIndex:i] objectForKey:@"Trip1"]];
                            NSRange range3 = [text rangeOfString:[[Arr objectAtIndex:i] objectForKey:@"Trip2"]];
                            NSRange range4  = [text rangeOfString:[[Arr objectAtIndex:i] objectForKey:@"Trip3"]];
                            NSRange range6 = [text rangeOfString:[[Arr objectAtIndex:i] objectForKey:@"BoldText"]];
                            NSRange range5  = [text rangeOfString:[arr_month objectAtIndex:[[[Arr objectAtIndex:i] objectForKey:@"Month"] integerValue]-1]];
                            
                            NSRange range7  = [text rangeOfString:[NSString stringWithFormat:@"%@",[[Arr objectAtIndex:i] objectForKey:@"Year"]]];
                            
                            [attributedText setAttributes:@{NSForegroundColorAttributeName: [UIColor blackColor],
                                                            NSFontAttributeName:boldFont} range:range];
                            [attributedText setAttributes:@{NSForegroundColorAttributeName: [UIColor blackColor],
                                                            NSFontAttributeName:boldFont} range:range2];
                            [attributedText setAttributes:@{NSForegroundColorAttributeName: [UIColor blackColor],
                                                            NSFontAttributeName:boldFont} range:range3];
                            [attributedText setAttributes:@{NSForegroundColorAttributeName: [UIColor blackColor],
                                                            NSFontAttributeName:boldFont} range:range4];
                            [attributedText setAttributes:@{NSForegroundColorAttributeName: [UIColor blackColor],
                                                            NSFontAttributeName:boldFont} range:range5];
                            [attributedText setAttributes:@{NSForegroundColorAttributeName: [UIColor blackColor],
                                                            NSFontAttributeName:boldFont} range:range6];
                            [attributedText setAttributes:@{NSForegroundColorAttributeName: [UIColor blackColor],
                                                            NSFontAttributeName:boldFont} range:range7];
                            [arrbottom addObject:attributedText];
                            
                            
                            NSMutableAttributedString *attributedTextwecel = [[NSMutableAttributedString alloc] initWithString:textwecel attributes:attribs];
                            [attributedTextwecel setAttributes:@{NSForegroundColorAttributeName: [UIColor blackColor],
                                                                 NSFontAttributeName:boldFont} range:rangewel];
                            [attributedTextwecel setAttributes:@{NSForegroundColorAttributeName: [UIColor blackColor],
                                                                 NSFontAttributeName:boldFont} range:rangewel2];
                            [attributedTextwecel setAttributes:@{NSForegroundColorAttributeName: [UIColor blackColor],
                                                                 NSFontAttributeName:boldFont} range:rangewel3];
                            
                            [arr_wecel addObject:attributedTextwecel];
                        }
                        m=  [[[arrheader objectAtIndex:0] componentsSeparatedByString:@" "] objectAtIndex:0];
                    }
                    [_tbl_walloffame reloadData];
                    
                    
                }
                else{
                    [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
                }
            }
        }];
    }
    
}
#pragma mark - slideView Delegate

-(void) slideViewControllerDone:(SlidingViewController *)sender
{
    if (sender) {
        [self HideSlideMenu];
    }
}
-(void) HideSlideMenu
{
    [UIView animateWithDuration:0.50 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        //code with animation
        // slideViewMenu.view.frame = CGRectMake(-self.view.frame.size.width, 0.0f, self.view.frame.size.width, self.view.frame.size.height - 0.0f);
        [slideViewMenu.view removeFromSuperview];
    } completion:^(BOOL finished) {
        //code for completion
        [slideViewMenu.view removeFromSuperview];
        slideViewMenu = nil;
        
    }];
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)viewDidLayoutSubviews
{
    
    
    if ([_tbl_walloffame respondsToSelector:@selector(setSeparatorInset:)]) {
        [_tbl_walloffame setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([_tbl_walloffame respondsToSelector:@selector(setLayoutMargins:)]) {
        [_tbl_walloffame setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    height= 270;
    
    if ([UIScreen mainScreen].bounds.size.height==568) {
         height= 230;
    }
        else if ([UIScreen mainScreen].bounds.size.height==667) {
        height= 250;
            
        }
   else if([UIScreen mainScreen].bounds.size.height==812 ){
         height= 270;
        }
 else if ([UIScreen mainScreen].bounds.size.height==736){
      height= 285;
 }
     
     
    arrheader =[[NSMutableArray alloc]init];
    arrsubheader=[[NSMutableArray alloc]init];
    arrimgprof=[[NSMutableArray alloc]init];
    arrimgbadge=[[NSMutableArray alloc]init];
    arrbottom=[[NSMutableArray alloc]init];
    arr_month =[[NSMutableArray alloc]init];
    arr_wecel =[[NSMutableArray alloc]init];
//    [arrheader addObject:@"December 2017 driver of the month"];
//    [arrheader addObject:@"October 2017 driver of the month"];
//    [arrheader addObject:@"June 2017 driver of the month"];
//    [arrheader addObject:@"August 2017 driver of the month"];
//
//    [arrsubheader addObject:@"Decembh wfkh e erwerer  r r erger ergergonth"];
//    [arrsubheader addObject:@"Octo erger er ge er er dvbdfsheg ertgergegefgvg er g erger erge rger erg erger erg ergers gr of the month"];
//    [arrsubheader addObject:@"June 201er gergerg ersge month"];
//    [arrsubheader addObject:@"Aer gerg erg ergergerg f the month"];
//
//
//    [arrimgprof addObject:@""];
//    [arrimgprof addObject:@""];
//    [arrimgprof addObject:@""];
//    [arrimgprof addObject:@""];
//
//    [arrimgbadge addObject:@"1"];
//    [arrimgbadge addObject:@"2"];
//    [arrimgbadge addObject:@"3"];
//    [arrimgbadge addObject:@"4"];
//
//
//    [arrbottom addObject:@"December 201r gerge er ert sse erserrrrteryeryetye ert ertrt erter7 driver of the month"];
//    [arrbottom addObject:@"Octo sds dberfdssdf d s sf dfgdfg dfg fdg dfgdfg dfgd gf  dfgsfgfg dfghhfgh ghfghh gfhrty etaegaw gese tre htrh rt hrt sd fdsg dfhthtuuy y hsfss  dsfds 2017 driver of the month"];
//    [arrbottom addObject:@"June 2017 driver of the month"];
//    [arrbottom addObject:@"August g gdsfs s sdfdsf dsf dsf f s sfs sd ds sdfdsfdsfds fds dsfdfsdfdsfds g sfds fgdfg sdfgof the month"];

    if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
        [arr_month addObject:@"January"];
        [arr_month addObject:@"February"];
        [arr_month addObject:@"March"];
        [arr_month addObject:@"April"];
        [arr_month addObject:@"May"];
        [arr_month addObject:@"June"];
        [arr_month addObject:@"July"];
        [arr_month addObject:@"August"];
        [arr_month addObject:@"September"];
        [arr_month addObject:@"October"];
        [arr_month addObject:@"November"];
        [arr_month addObject:@"December"];
    }
    else{
        [arr_month addObject:@"يناير"];
        [arr_month addObject:@"فبراير"];
        [arr_month addObject:@"مارس"];
        [arr_month addObject:@"أبريل"];
        [arr_month addObject:@"مايو"];
        [arr_month addObject:@"يونيو"];
        [arr_month addObject:@"يوليو"];
        [arr_month addObject:@"أغسطس"];
        [arr_month addObject:@"سبتمبر"];
        [arr_month addObject:@"أكتوبر"];
        [arr_month addObject:@"نوفمبر"];
        [arr_month addObject:@"ديسمبر"];
    }
  
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  [arrheader count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"WalloffameTableViewCell";
    
    WalloffameTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier forIndexPath: indexPath];
   // cell.backgroundColor = [UIColor redColor];
  
    cell.lbl_header.text = [arrheader objectAtIndex:indexPath.row];

    NSString *str1=@""; NSString *strboldtext=@""; NSString*str2=@"";
    
//    if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
//
//        str1 = @"We celebrate";
//        strboldtext = [NSString stringWithFormat:@" %@",[arrsubheader objectAtIndex:indexPath.row]];
//       str2= @" who has...";
//
//    }else{
//        str1 = @"We celebrate";
//        strboldtext = [NSString stringWithFormat:@" %@",[arrsubheader objectAtIndex:indexPath.row]];
//        str2= @" who has...";
//    }
//        NSString *text = [NSString stringWithFormat:@"%@ %@ %@",str1,strboldtext,str2];
//
//        NSDictionary *attribs = @{
//                                  NSForegroundColorAttributeName:[UIColor blackColor],
//                                  NSFontAttributeName: [UIFont fontWithName:@"ClanPro-Book" size:10.6]
//                                  };
//        NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text attributes:attribs];
//        UIFont *boldFont = [UIFont fontWithName:@"ClanPro-Medium" size:10.6];
//        NSRange range = [text rangeOfString:strboldtext];
//        [attributedText setAttributes:@{NSForegroundColorAttributeName: [UIColor blackColor],
//                                        NSFontAttributeName:boldFont} range:range];

        cell.lbl_subheader.attributedText=[arr_wecel objectAtIndex:indexPath.row];

       cell.lbl_bottom.attributedText=[arrbottom objectAtIndex:indexPath.row];



    if ([[arrimgprof objectAtIndex:indexPath.row] isKindOfClass:[NSNull class]] || [[arrimgprof objectAtIndex:indexPath.row] isEqualToString:@""] ) {
        [cell.img_prof sd_setImageWithURL:[NSURL URLWithString:@""]
                        placeholderImage:[UIImage imageNamed:@"Pro.png"]];
    }else{

        [cell.img_prof sd_setImageWithURL:[NSURL URLWithString:@""]
                        placeholderImage:[UIImage imageNamed:@"Pro.png"]];

    }


    if ([[arrimgbadge objectAtIndex:indexPath.row] integerValue] ==1) {
        [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"partnertype"];
        cell.img_badge.image = [UIImage imageNamed:@"gold.png"];
    }
    else if ([[arrimgbadge objectAtIndex:indexPath.row] integerValue] ==2) {
        [[NSUserDefaults standardUserDefaults] setValue:@"2" forKey:@"partnertype"];
        cell.img_badge.image = [UIImage imageNamed:@"silver.png"];
    }
    else if ([[arrimgbadge objectAtIndex:indexPath.row]  integerValue] ==3) {
        [[NSUserDefaults standardUserDefaults] setValue:@"3" forKey:@"partnertype"];
        cell.img_badge.image = [UIImage imageNamed:@"bronze.png"];
    }
    else {
        [[NSUserDefaults standardUserDefaults] setValue:@"4" forKey:@"partnertype"];
        cell.img_badge.image = [UIImage imageNamed:@"platinum.png"];
    }
    // [_tbl_walloffame beginUpdates];
     cell.lbl_bottom.numberOfLines= 20;
     cell.lbl_header.numberOfLines= 20;
     cell.lbl_subheader.numberOfLines= 20;
    
     float f1 =[self getLabelHeight:cell.lbl_header];
     float f2 =[self getLabelHeight:cell.lbl_subheader];
    
     float f4 =[self getLabelHeight:cell.lbl_bottom];
    
// [cell.lbl_header setFrame:CGRectMake(cell.lbl_header.frame.origin.x, cell.lbl_header.frame.origin.y, cell.lbl_header.frame.size.width, cell.lbl_header.frame.size.height)];

//
//    [cell.lbl_subheader setFrame:CGRectMake(cell.lbl_subheader.frame.origin.x, cell.lbl_subheader.frame.origin.y, cell.lbl_subheader.frame.size.width, f2)];
//
////
//    [cell.img_prof setFrame:CGRectMake(cell.img_prof.frame.origin.x, cell.lbl_subheader.frame.origin.y+f2 +20, cell.img_prof.frame.size.width, cell.img_prof.frame.size.height)];
//
//   [cell.lbl_bottom setFrame:CGRectMake(cell.lbl_bottom.frame.origin.x, cell.img_prof.frame.origin.y+cell.img_prof.frame.size.height +20, cell.lbl_bottom.frame.size.width, f4)];
//
//    height= cell.lbl_bottom.frame.origin.y+cell.lbl_bottom.frame.size.height+20;
//
//
//    [_tbl_walloffame endUpdates];
//
    

    if ([m isEqualToString:[[[arrheader objectAtIndex:indexPath.row] componentsSeparatedByString:@" "] objectAtIndex:0]]) {
        if (indexPath.row==0) {
          cell.lbl_header.hidden=NO;
        cell.lbl_line.hidden=YES ;
        }
        else{
     cell.lbl_header.hidden=YES;
     cell.lbl_line.hidden=NO ;
       
        }
    }
    else{
    cell.lbl_header.hidden=NO;
     cell.lbl_line.hidden=YES ;
    }
    @try{
    if (indexPath.row+1 < [arrheader count]) {

  m = [[[arrheader objectAtIndex:indexPath.row+1] componentsSeparatedByString:@" "] objectAtIndex:0];
    }
   }
    @catch(NSException *e){
        
    }
    @finally{
        
    }
    
    cell.selectionStyle= UITableViewCellSelectionStyleNone;
    return cell;
    
}
- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}

- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section
{
    //Set the background color of the View
    view.tintColor = [UIColor clearColor];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

        return height;
  
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
}

-(IBAction)filter:(id)sender{
    
    
    NSDate *Seldate = nil;
   
        NSDateFormatter *formate = [[NSDateFormatter alloc] init];
        [formate setDateFormat:@"MM/yyyy"];
        
    // Seldate = [formate dateFromString:@"03/2018"];
    
   
    [self ShowDatePickerMin:nil max:[NSDate date] sel:Seldate tag:1];
}
-(void) ShowDatePickerMin:(NSDate *) minDate max:(NSDate *) maxDate sel:(NSDate *) cDate tag:(NSInteger) tagVal
{
    
    [self.view endEditing:YES];
    if (datePickerView.view) {
        [datePickerView.view removeFromSuperview];
        datePickerView = nil;
    }
    
    datePickerView = [[DatePickerViewController alloc] init];
    [self addChildViewController:datePickerView];
    datePickerView.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    datePickerView.view.tag = tagVal;
    
    datePickerView.datePicker_MyDate.datePickerMode = UIDatePickerModeDate;
   
    datePickerView.dateType=@"WOF";
    datePickerView.datePicker_MyDate.backgroundColor = [UIColor whiteColor];
    if (minDate) {
        datePickerView.datePicker_MyDate.minimumDate = minDate;
    }
    
    if (maxDate) {
        datePickerView.datePicker_MyDate.maximumDate = maxDate;
    }
    
    if (cDate) {
        datePickerView.datePicker_MyDate.date = cDate;
    }
    
    [self.view addSubview:datePickerView.view];
    [datePickerView didMoveToParentViewController:self];
    
    datePickerView.delegate = self;
}

-(void) DatePickerViewControllerDone:(DatePickerViewController *)sender
{
   
      [btn_filter setTitle:[NSString stringWithFormat:@"%@",sender.SelectedDateStr] forState:UIControlStateNormal];

    if (datePickerView.view) {
        [datePickerView.view removeFromSuperview];
        datePickerView = nil;
    }
}
-(void) DatePickerViewControllerCancel:(DatePickerViewController *)sender
{
    if (datePickerView.view) {
        [datePickerView.view removeFromSuperview];
        datePickerView = nil;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
