//
//  DatePickerViewController.h
//  SPSS
//
//  Created by 9Dim on 18/05/17.
//  Copyright © 2017 TechRadiation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DatePickerViewController;
@protocol myDatePickerViewControllerDelegate
- (void) DatePickerViewControllerDone: (DatePickerViewController *) sender;
- (void) DatePickerViewControllerCancel: (DatePickerViewController *) sender;
//- (void) PickerViewDelegateMethodRowSelect: (DatePickerViewController *) sender;
@end

@interface DatePickerViewController : UIViewController

@property (nonatomic, retain) id <myDatePickerViewControllerDelegate> delegate;

@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker_MyDate;
@property (nonatomic, strong) NSString *SelectedDateStr;
@property (nonatomic, strong) NSString *dateType;
- (IBAction)Action_Cancel:(id)sender;
- (IBAction)Action_Done:(id)sender;


@end
