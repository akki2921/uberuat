//
//  DatePickerViewController.m
//  SPSS
//
//  Created by 9Dim on 18/05/17.
//  Copyright © 2017 TechRadiation. All rights reserved.
//

#import "DatePickerViewController.h"
#import "UtillClass.h"

@interface DatePickerViewController ()

@end

@implementation DatePickerViewController
@synthesize SelectedDateStr;
@synthesize datePicker_MyDate,dateType;

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString * language = [[NSLocale preferredLanguages] firstObject];
    
    
    NSDateFormatter *formate = [[NSDateFormatter alloc] init];
    if([[language substringToIndex:2] isEqualToString:@"ar"] && [[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]){
        [formate setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];;
    }
    if ([dateType isEqualToString:@"WOF"]) {
        
        [formate setDateFormat:@"MM/yyyy"];
    }
    else{
        [formate setDateFormat:@"dd/MM/yyyy"];
    }
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)Action_Cancel:(id)sender {
    [self.delegate DatePickerViewControllerCancel:self];
}

- (IBAction)Action_Done:(id)sender {
    NSString * language = [[NSLocale preferredLanguages] firstObject];
    
    NSDateFormatter *formate = [[NSDateFormatter alloc] init];
    if([[language substringToIndex:2] isEqualToString:@"ar"] && [[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]){
        [formate setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];;
    }
    if ([dateType isEqualToString:@"WOF"]) {
        
        [formate setDateFormat:@"MM/yyyy"];
    }
    else{
        [formate setDateFormat:@"dd/MM/yyyy"];
    }
    SelectedDateStr = [NSString stringWithFormat:@"%@",[formate stringFromDate:datePicker_MyDate.date]];
    [self.delegate DatePickerViewControllerDone:self];
}
@end



