//
//  ForgChangeViewController.h
//  Uber Reward
//
//  Created by Arvind on 22/03/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UtillClass.h"
#import "SlidingViewController.h"
#import "BSErrorMessageView.h"
#import "UITextField+BSErrorMessageView.h"
#import "MBProgressHUD.h"

@interface ForgChangeViewController : UIViewController<slideViewControllerDelegate>
{
    SlidingViewController *slideViewMenu;
    
}
@property (nonatomic, strong) NSString *str_type;
@property (nonatomic, strong) NSString *str_eorm;
@property (strong, nonatomic) IBOutlet UITextField *tf_newPsw;
@property (strong, nonatomic) IBOutlet UITextField *tf_confirmPsw;

@end
