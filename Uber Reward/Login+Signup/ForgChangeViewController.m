//
//  ForgChangeViewController.m
//  Uber Reward
//
//  Created by Arvind on 22/03/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import "ForgChangeViewController.h"

@interface ForgChangeViewController ()

@end

@implementation ForgChangeViewController
@synthesize str_eorm,str_type;
-(IBAction)backregtowel:(id)sender{
    
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"ForgetViewController"];
    [self presentViewController:vc animated:false completion:nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
     [_tf_newPsw setFont:[UIFont fontWithName:@"ClanPro-Book" size:12.6]];
    
     [_tf_confirmPsw setFont:[UIFont fontWithName:@"ClanPro-Book" size:12.6]];
    _tf_newPsw.layer.borderWidth= 1;
    _tf_newPsw.layer.borderColor=[UIColor colorWithRed:192/255.0f green:192/255.0f blue:192/255.0f alpha:1].CGColor;
    _tf_newPsw.layer.cornerRadius=1;
    
    _tf_confirmPsw.layer.borderWidth= 1;
    _tf_confirmPsw.layer.borderColor=[UIColor colorWithRed:192/255.0f green:192/255.0f blue:192/255.0f alpha:1].CGColor;
    _tf_confirmPsw.layer.cornerRadius=1;
     
}
-(void)viewDidAppear:(BOOL)animated {
    [[UtillClass sharedInstance]HideLoader];
    [self HideSlideMenu];
   // [[UtillClass sharedInstance] showbottom_items];
    
}
- (IBAction)select_submit:(id)sender {
    [self.view endEditing:YES];
    
 
    
    if (!_tf_newPsw.text.length) {
        
    
        
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"Please provide password." onTf:_tf_newPsw];
        }
        else{
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"يرجى إدخال كلمة المرور" onTf:_tf_newPsw];
            
        }
        
    }
//    else if (_tf_newPsw.text.length<8) {
//
//
//        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
//            [[UtillClass sharedInstance] ShowTfErrorMsg:@"Password should be made up of 8 characters." onTf:_tf_newPsw];
//        }
//        else{
//            [[UtillClass sharedInstance] ShowTfErrorMsg:@"يجب أن تتكون كلمة المرور من 8 أحرف"onTf:_tf_newPsw];
//
//        }
//    }
   else if (!_tf_confirmPsw.text.length) {

       
       if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
           [[UtillClass sharedInstance] ShowTfErrorMsg:@"Please confirm your password." onTf:_tf_confirmPsw];
       }
       else{
           [[UtillClass sharedInstance] ShowTfErrorMsg:@"تأكيد كلمة المرور الجديدة " onTf:_tf_confirmPsw];
           
       }
    }
   else if (_tf_confirmPsw.text.length<8 && _tf_newPsw.text.length<8) {
       
       
       if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
           [[UtillClass sharedInstance] showerrortoastmsg:@"Password should be made up of 8 characters."];
       }
       else{
           [[UtillClass sharedInstance]showerrortoastmsg:@"يجب أن تتكون كلمة المرور من 8 أحرف"];
           
       }
       
   }
    else if (![_tf_confirmPsw.text isEqualToString:_tf_newPsw.text]) {
     
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"New Password and Confirm Password do not match. Please re-enter your password." onTf:_tf_confirmPsw];
        }
        else{
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"كلمة المرور وتأكيد كلمة المرور غير متطابقين. يرجى إعادة إدخال كلمة المرور." onTf:_tf_confirmPsw];
            
        }
    }
   
    
    
    else{
        if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
            if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        };
            
        }
        else{
            [[UtillClass sharedInstance] ShowLoaderOn:self.parentViewController withtext:@"Please wait"];
            
            NSMutableDictionary*dicts = [[NSMutableDictionary alloc]init];
            NSString *strm = @"";
            NSString *stre = @"";
            if ([str_type isEqualToString:@"2"]) {
                strm = str_eorm;
                stre=@"";
            }
            else{
                stre = str_eorm;
                strm=@"";
            }
            [dicts setValue:strm forKey:@"MobileNo"];
            [dicts setValue:stre forKey:@"EmailID"];
            [dicts setValue:_tf_newPsw.text forKey:@"NewPassword"];
            [dicts setValue:_tf_confirmPsw.text forKey:@"ConfirmPassword"];
            [dicts setValue:[[UtillClass sharedInstance] getcultureID] forKey:@"CultureId"];
             [dicts setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"forgchangeccd"] forKey:@"CountryCode"];
            
            [[UtillClass sharedInstance] Postresponsetoservice:dicts withBearer:[[UtillClass sharedInstance] getapp_sessiontoken] withurl:[NSString stringWithFormat:@"Account/ForgetPasswordLinkMobile"] response:^(NSDictionary * dict_response) {
                [[UtillClass sharedInstance] HideLoader];
                if (dict_response ==nil) {
                    [[UtillClass sharedInstance] showerrortoastmsg:@"Server error, Please try after sometime."];
                    
                }
                else{
                    if ([[dict_response objectForKey:@"Status"] integerValue] == 1) {
                        [[UtillClass sharedInstance] showtoastmsg:@"Your Password has been reset successfully"];
                        
                        UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"LoginViewController"];
                        
                        [self presentViewController:vc animated:false completion:nil];
                        
                    }
                    else{
                        [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
                        
                        
                    }
                }
            }];
        }
    }
    
}
- (IBAction)select_sidemenu:(id)sender {
    
    if (slideViewMenu.view) {
        
        [self HideSlideMenu];
    } else {
        slideViewMenu = [[UtillClass sharedInstance] CallSlideMenuView:self yVal:0.0f];
        slideViewMenu.Str_isLogIn = @"noLogin";
    }
}

#pragma mark - slideView Delegate

-(void) slideViewControllerDone:(SlidingViewController *)sender
{
    if (sender) {
        [self HideSlideMenu];
    }
}
-(void) HideSlideMenu
{
    [UIView animateWithDuration:0.50 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        //code with animation
        // slideViewMenu.view.frame = CGRectMake(-self.view.frame.size.width, 0.0f, self.view.frame.size.width, self.view.frame.size.height - 0.0f);
        [slideViewMenu.view removeFromSuperview];
    } completion:^(BOOL finished) {
        //code for completion
        [slideViewMenu.view removeFromSuperview];
        slideViewMenu = nil;
        
    }];
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}
#pragma mark - UITextFieldDelegate


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    [textField layoutIfNeeded];
    [[UtillClass sharedInstance] HideTfErrorMsg];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
