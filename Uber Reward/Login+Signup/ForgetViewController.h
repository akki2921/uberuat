//
//  ForgetViewController.h
//  Uber Reward
//
//  Created by Arvind Seth on 08/03/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UtillClass.h"
#import "NIDropDown.h"
#import "SlidingViewController.h"

@interface ForgetViewController : UIViewController<NIDropDownDelegate, slideViewControllerDelegate>

{
    SlidingViewController *slideViewMenu;
    IBOutlet UIButton *btn_selcode;
    NSMutableArray*arr_imgflag;
    NSMutableArray*arr_cname;
    NSMutableArray*arr_cid;
    NSMutableArray*arr_mobdigit;
    NIDropDown *dropDown;
    NSString *strcode;
    
    int maxdigit;
    NSTimer *t;
    int count;
    NSString *str;
    NSString *strem;
    NSTimer*VerifyBtnTimer;
    
    BOOL  resend;
}
@property (strong, nonatomic) IBOutlet UIScrollView * scr_emailpage;
@property (strong, nonatomic) IBOutlet UILabel *lbl_otpshow;
@property (weak, nonatomic) IBOutlet UIView *view_timer_hidden;
@property (strong, nonatomic) IBOutlet UITextField *tf_email;
@property (strong, nonatomic) IBOutlet UIImageView *img_flaf;
@property (strong, nonatomic) IBOutlet UILabel *lbl_code;
@property (strong, nonatomic) IBOutlet UITextField *tf_mobile;
@property (strong, nonatomic) IBOutlet UIButton *btn_submit_;
@property (strong, nonatomic) IBOutlet UILabel *lbl_sub_hid;
@property (strong, nonatomic) IBOutlet UITextField *tf1;
@property (strong, nonatomic) IBOutlet UITextField *tf2;
@property (strong, nonatomic) IBOutlet UITextField *tf3;
@property (strong, nonatomic) IBOutlet UITextField *tf4;
@property (strong, nonatomic) IBOutlet UITextField *tf5;
@property (strong, nonatomic) IBOutlet UITextField *tf6;
@property (strong, nonatomic) IBOutlet UILabel *lbl_ver_hid;
@property (strong, nonatomic) IBOutlet UIButton *btn_verify;

@end
