//
//  ForgotUserNameViewController.h
//  Uber Reward
//
//  Created by Arvind on 21/03/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UtillClass.h"
#import "NIDropDown.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "BSErrorMessageView.h"
#import "UITextField+BSErrorMessageView.h"
#import "SlidingViewController.h"


@interface ForgotUserNameViewController : UIViewController<NIDropDownDelegate, slideViewControllerDelegate>
{
    SlidingViewController *slideViewMenu;
    NSMutableArray*arr_imgflag;
    NSMutableArray*arr_cname;
    NSMutableArray*arr_cid;
    NSMutableArray*arr_mobdigit;
    NIDropDown *dropDown;
    
    int maxdigit;
    
    IBOutlet UIImageView *img_flag;
    IBOutlet UILabel *lbl_ccode;
    IBOutlet UITextField *tf_mob;
    
}


- (IBAction)Action_Menu:(id)sender;
- (IBAction)Action_Back:(id)sender;
- (IBAction)Action_Submit:(id)sender;
- (IBAction)Action_MobileCode:(id)sender;




@end
