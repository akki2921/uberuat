//
//  ForgotUserNameViewController.m
//  Uber Reward
//
//  Created by Arvind on 21/03/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import "ForgotUserNameViewController.h"

@interface ForgotUserNameViewController ()

@end

@implementation ForgotUserNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"ios  Forget Username Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    // Do any additional setup after loading the view.
    
    arr_cname =[[NSMutableArray alloc]init];
    arr_imgflag = [[NSMutableArray alloc]init];
    arr_cid =[[NSMutableArray alloc]init];
    arr_mobdigit = [[NSMutableArray alloc]init];
    
    [self GetCountryCodes];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)GetCountryCodes{
    [self.view endEditing:YES];
    if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        };
        
    }
    else{
        [[UtillClass sharedInstance] ShowLoaderOn:self.parentViewController withtext:@"Please wait"];
        
        NSMutableDictionary*dicts = [[NSMutableDictionary alloc]init];
        [dicts setValue:[[UtillClass sharedInstance] getcultureID] forKey:@"CultureId"];
        
        
        [[UtillClass sharedInstance] getresponsefromservice:[NSString stringWithFormat:@"Account/GetCountry?CultureId=%@",[[UtillClass sharedInstance] getcultureID]] withbearer:@"" response:^(NSDictionary * dict_response) {
            [[UtillClass sharedInstance] HideLoader];
            if (dict_response ==nil) {
                [[UtillClass sharedInstance] showerrortoastmsg:@"Server error, Please try after sometime."];
                
            }
            else{
                if ([[dict_response objectForKey:@"Status"] integerValue] == 1) {
                    [[UtillClass sharedInstance] setUserdefaultofDict:dict_response withKayname:@"CountryDetail"];
                    NSArray *Arr = [dict_response objectForKey:@"ListData"];
                    for (int i=0; i<[Arr count]; i++) {
                        
                        [arr_mobdigit addObject:[[Arr objectAtIndex:i]objectForKey:@"MobileDigit"]];
                        [arr_cname addObject:[[Arr objectAtIndex:i]objectForKey:@"CountryName"]];
                        [arr_cid addObject:[[Arr objectAtIndex:i]objectForKey:@"CountryID"]];
                        [arr_imgflag addObject:[[Arr objectAtIndex:i]objectForKey:@"CountryImage"]];
                        
                    } 
                    lbl_ccode.text =[NSString stringWithFormat:@"+%@", [arr_cid objectAtIndex:0]];
                    
                    [img_flag sd_setImageWithURL:[NSURL URLWithString:[arr_imgflag objectAtIndex:0]]
                                 placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
                    maxdigit = [[arr_mobdigit objectAtIndex:0]intValue];
                    
                }
                else{
                    [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
                }
            }
        }];
    }
    
}

- (IBAction)Action_Menu:(id)sender {
    if (slideViewMenu.view) {
        [self HideSlideMenu];
    } else {
        slideViewMenu = [[UtillClass sharedInstance] CallSlideMenuView:self yVal:0.0f];
        slideViewMenu.Str_isLogIn = @"noLogin";
    }
}


#pragma mark - slideView Delegate

-(void) slideViewControllerDone:(SlidingViewController *)sender
{
    if (sender) {
        [self HideSlideMenu];
    }
}


-(void) HideSlideMenu
{
    [UIView animateWithDuration:0.50 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        //code with animation
        // slideViewMenu.view.frame = CGRectMake(-self.view.frame.size.width, 0.0f, self.view.frame.size.width, self.view.frame.size.height - 0.0f);
        [slideViewMenu.view removeFromSuperview];
    } completion:^(BOOL finished) {
        //code for completion
        [slideViewMenu.view removeFromSuperview];
        slideViewMenu = nil;
        
    }];
}





- (IBAction)Action_Back:(id)sender {
    [self dismissViewControllerAnimated:false completion:nil];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    [textField layoutIfNeeded];
    [[UtillClass sharedInstance] HideTfErrorMsg];
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [tf_mob bs_hideError];
}
- (IBAction)Action_Submit:(id)sender {
    [self.view endEditing:YES];
    
    if (!tf_mob.text.length) {
        
        
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"Please provide valid mobile number. E.g. 00966XXXXXXXXX" onTf:tf_mob];
        }
        else{
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"يرجى إدخال رقم جوال صحيح. مثلاً 00966XXXXXXXXX" onTf:tf_mob];
            
        }
    }
    else if ([tf_mob.text isEqualToString:@"000000000"]) {
        
        
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"Please provide valid mobile number. E.g. 00966XXXXXXXXX" onTf:tf_mob];
        }
        else{
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"يرجى إدخال رقم جوال صحيح. مثلاً 00966XXXXXXXXX" onTf:tf_mob];
            
        }
    }
    else if (tf_mob.text.length<maxdigit) {
        
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"Please provide valid mobile number. E.g. 00966XXXXXXXXX" onTf:tf_mob];
        }
        else{
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"يرجى إدخال رقم جوال صحيح. مثلاً 00966XXXXXXXXX" onTf:tf_mob];
            
        }
        
    }
    else{
        if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
            if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        };
            
        }
        else{
            [[UtillClass sharedInstance] ShowLoaderOn:self.parentViewController withtext:@"Please wait"];
            
            NSMutableDictionary*dicts = [[NSMutableDictionary alloc]init];
            
            
            
            [dicts setValue:tf_mob.text forKey:@"Email_Mobile"];
            [dicts setValue:@"2" forKey:@"IsEmail_Mobile"];
            [dicts setValue:@"" forKey:@"OTP"];
            [dicts setValue:[lbl_ccode.text stringByReplacingOccurrencesOfString:@"+" withString:@""] forKey:@"CountryCode"];
             
            [[UtillClass sharedInstance] Postresponsetoservice:dicts withBearer:nil withurl:[NSString stringWithFormat:@"Account/ForgetUserNameMobile?CultureId=%@",[[UtillClass sharedInstance]getcultureID]] response:^(NSDictionary * dict_response) {
                [[UtillClass sharedInstance] HideLoader];
                if (dict_response ==nil) {
                    [[UtillClass sharedInstance] showerrortoastmsg:@"Server error, Please try after sometime."];
                    
                }
                else{
                    if ([[dict_response objectForKey:@"Status"] integerValue] == 1) {
                        [[UtillClass sharedInstance] showtoastmsg:[dict_response objectForKey:@"msg"]];
//                        _btn_verify.enabled=YES;
//                        _tf1.enabled=YES;
//                        _tf2.enabled=YES;
//                        _tf3.enabled=YES;
//                        _tf4.enabled=YES;
//                        _tf5.enabled=YES;
//                        _tf6.enabled=YES;
//                        _lbl_hidden.hidden=YES;
//
//                        _tf_mob.enabled=NO;
//                        _lbl_hidden_submit.hidden=NO;
                        
                    }
                    else{
                        [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
                        
                    }
                }
            }];
        }
    }
    
}

- (IBAction)Action_MobileCode:(id)sender {
    [self OpenDropDown:sender Array:[arr_cid copy]];
}

- (void) niDropDownDelegateMethod: (NSString *)btntext withbtntag:(NSString *)btntag dropdown: (NIDropDown *) sender {
    dropDown = nil;
    
    lbl_ccode.text =[NSString stringWithFormat:@"%@", [[btntext componentsSeparatedByString:@","]objectAtIndex:0]];
    
    [img_flag sd_setImageWithURL:[NSURL URLWithString:[arr_imgflag objectAtIndex:[[[btntext componentsSeparatedByString:@","]objectAtIndex:1] intValue]]]
                 placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    
    maxdigit = [[arr_mobdigit objectAtIndex:[[[btntext componentsSeparatedByString:@","]objectAtIndex:1] intValue]] intValue];
    
}
#pragma mark - DropDown Delegate
- (void)OpenDropDown:(UIButton *)sender Array:(NSArray *) arr
{
    [self.view endEditing:YES];
    //    arr = [[NSArray alloc] init];
    
    NSArray * arrImage = [[NSArray alloc] init];
    
    arrImage = [arr_imgflag mutableCopy];
    
    
    if(dropDown == nil) {
        CGFloat f = 200;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :arrImage :@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        dropDown = nil;
    }
}


#pragma mark - TextField delgate and TextField Keyboard Handle

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (range.length > 0)
    {// We're deleting
        
        return YES;
    }
    NSString *numberRegEx = @"[0-9]";
    NSPredicate *testRegex = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegEx];
    
    if(![testRegex evaluateWithObject:string]){
        return NO;
    }
    
    if (textField.tag==0) {
        if (textField.text.length >= maxdigit) {
            return NO;
        }
        NSString *str;
        if (!textField.text.length) {
            str= [textField.text substringToIndex:0];
        }
        else{
            str= [textField.text substringToIndex:1];
        }
        
        if ([str isEqualToString:@""] && [string isEqualToString:@"0"] ) {
            return NO;
        }
    }
    return YES;
}


@end
