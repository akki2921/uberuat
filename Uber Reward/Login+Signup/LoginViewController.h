//
//  LoginViewController.h
//  Uber Reward
//
//  Created by Arvind Seth on 22/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"
#import "SlidingViewController.h"
#import "TextFieldValidator.h"
#import <Google/Analytics.h>

@interface LoginViewController : UIViewController<slideViewControllerDelegate>
{
    SlidingViewController *slideViewMenu;
}
@property (strong, nonatomic) IBOutlet TextFieldValidator *tf_username;
//@property (weak, nonatomic) IBOutlet UITextField *tf_username;
@property (weak, nonatomic) IBOutlet UITextField *tf_password;
@property (weak, nonatomic) IBOutlet UIImageView *img_check;
@property (strong, nonatomic) IBOutlet UILabel *lbl_login;

@end
