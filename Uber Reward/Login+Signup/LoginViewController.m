//
//  LoginViewController.m
//  Uber Reward
//
//  Created by Arvind Seth on 22/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import "LoginViewController.h"
#import "BSErrorMessageView.h"
#import "UITextField+BSErrorMessageView.h"
#import "MBProgressHUD.h"
#define REGEX_USER_NAME_LIMIT @"^.{3,10}$"
#define REGEX_USER_NAME @"[A-Za-z0-9]{3,10}"
#define REGEX_EMAIL @"[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
#define REGEX_PASSWORD_LIMIT @"^.{6,20}$"
#define REGEX_PASSWORD @"[A-Za-z0-9]{6,20}"
#define REGEX_PHONE_DEFAULT @"[0-9]{3}\\-[0-9]{3}\\-[0-9]{4}"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if([UIScreen mainScreen].bounds.size.height==812 ){
        
        [_img_check setFrame:CGRectMake(_img_check.frame.origin.x, _img_check.frame.origin.y+2, 20, 20)];
       
    
    }
    
    _tf_username.layer.borderColor=[UIColor colorWithRed:192/255.0f green:192/255.0f blue:192/255.0f alpha:1].CGColor;
    _tf_password.layer.borderWidth= 1;
    _tf_password.layer.borderColor=[UIColor colorWithRed:192/255.0f green:192/255.0f blue:192/255.0f alpha:1].CGColor;
    _tf_username.layer.borderWidth= 1;
    
    _tf_password.layer.cornerRadius=1;
    _tf_username.layer.cornerRadius=1;
    _tf_username.text=@"";
    _tf_password.text=@"";
   // _tf_username.presentInView=self.view;


   
}

-(IBAction)backregtowel:(id)sender{
    
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"WelcomeViewController"];
    [self presentViewController:vc animated:false completion:nil];
}
#pragma mark - Setup methods
- (void)setupLoginTextField
{
   
  
}

-(void)viewWillAppear:(BOOL)animated{
    [self HideSlideMenu];
    [[UtillClass sharedInstance] HideLoader];
    
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"ios  Login"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
}
- (IBAction)select_forgot_password:(id)sender {
        UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"ForgetViewController"];
      [self presentViewController:vc animated:false completion:nil];
     return;
}
- (IBAction)select_forgot_username:(id)sender {
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"ForgotUserNameViewController"];
    [self presentViewController:vc animated:false completion:nil];
    return;
}
- (IBAction)sekect_login:(id)sender {

    
    [self.view endEditing:YES];
    if (!_tf_username.text.length) {
        
    
        [_tf_username resignFirstResponder];
        
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
          [[UtillClass sharedInstance] ShowTfErrorMsg:@"Email cannot be blank." onTf:_tf_username];
        }
        else{
           [[UtillClass sharedInstance] ShowTfErrorMsg:@"البريد الإلكتروني الميدان لا يمكن أن يكون فارغا." onTf:_tf_username];
            
        }
    }
    else if (![[UtillClass sharedInstance] validateEmailWithString:_tf_username.text]) {
   
        [_tf_username resignFirstResponder];

        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"Please provide valid email address. E.g. example@example.com" onTf:_tf_username];
        }
        else{
[[UtillClass sharedInstance] ShowTfErrorMsg:@"يرجى إدخال بريد إلكتروني صحيح. مثلاً example@example.com" onTf:_tf_username];
            
        }
        
        
    }
    else if (_img_check.hidden==YES) {
        [_tf_username bs_showError];

    }
    else if (!_tf_password.text.length) {

        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"Password cannot be blank." onTf:_tf_password];
        }
        else{
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"حقل كلمة المرور لا يمكن أن يكون فارغا." onTf:_tf_password];
            
        }
     
          [_tf_password resignFirstResponder];
    }
    else{
        if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
            if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        };
            
        }
        else{
             [[UtillClass sharedInstance] ShowLoaderOn:self.parentViewController withtext:@"Please wait"];
            NSMutableDictionary*dicts = [[NSMutableDictionary alloc]init];
            [dicts setValue:[_tf_username.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"UserName"];
            [dicts setValue:_tf_password.text forKey:@"Password"];
            [dicts setValue:[[UtillClass sharedInstance]getdeviceid] forKey:@"DeviceId"];
            [dicts setValue:@"28.60292598" forKey:@"Lat"];
            [dicts setValue:@"77.35847287" forKey:@"Long"];
            [dicts setValue:@"05/Mar/2018 13:13:59" forKey:@"LoginDate"];
            [dicts setValue:@"192.168.29.1" forKey:@"IPAddress"];
            [dicts setValue:@"1" forKey:@"MediumTypeId"];
            [dicts setValue:[[UtillClass sharedInstance]getcultureID] forKey:@"CultureId"];
            [dicts setValue:[[UtillClass sharedInstance] getnotiid] forKey:@"Device_RegId"];
            NSLog(@"dictLogin%@",dicts);
            
            [[UtillClass sharedInstance] Postresponsetoservice:dicts withBearer:@"" withurl:@"Account/Login" response:^(NSDictionary * dict_response) {
                 [[UtillClass sharedInstance] HideLoader];
              
                if (dict_response ==nil) {
                    [[UtillClass sharedInstance] showerrortoastmsg:@"Server error, Please try after sometime."];
                    
                }
                else{
                    if ([[dict_response objectForKey:@"Status"] integerValue] == 1  && [[dict_response objectForKey:@"OTPFlag"] integerValue]==1 ) {
                         [[UtillClass sharedInstance] setUserdefaultofDict:dict_response withKayname:@"Login Details"];
                        [[NSUserDefaults standardUserDefaults] setValue:@"Loggedin" forKey:@"Userloginornot"];
                        [[NSUserDefaults standardUserDefaults] setObject:_tf_password.text forKey:@"psw"];
                        
                        id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
                        [tracker set:kGAIScreenName value:@"ios  Login"];
                        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"UserLogin"
                                                                              action:@"button_press"
                                                                               label:_tf_username.text
                                                                               value:nil] build]];
                        
                       
                        HomeViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
                        [self presentViewController:vc animated:false completion:nil];
                    }
                    else{
                       [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
                    }
                }
            }];
 }
}
    
}

- (IBAction)select_side_menu:(id)sender {
    if (slideViewMenu.view) {
        [self HideSlideMenu];
    } else {
          slideViewMenu.Str_isLogIn = @"noLogin";
        slideViewMenu = [[UtillClass sharedInstance] CallSlideMenuView:self yVal:0.0f];
      
    }
}


#pragma mark - slideView Delegate

-(void) slideViewControllerDone:(SlidingViewController *)sender
{
    if (sender) {
        [self HideSlideMenu];
    }
}
-(void) HideSlideMenu
{
    [UIView animateWithDuration:0.50 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        //code with animation
        // slideViewMenu.view.frame = CGRectMake(-self.view.frame.size.width, 0.0f, self.view.frame.size.width, self.view.frame.size.height - 0.0f);
        [slideViewMenu.view removeFromSuperview];
    } completion:^(BOOL finished) {
        //code for completion
        [slideViewMenu.view removeFromSuperview];
        slideViewMenu = nil;
        
    }];
}






#pragma mark - TextField delgate and TextField Keyboard Handle

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    if (textField==_tf_username) {
        if ([[UtillClass sharedInstance] validateEmailWithString:_tf_username.text]) {
            _img_check.hidden=NO;
        }
        else{
            _img_check.hidden=YES;
        }
    }
    [textField resignFirstResponder];
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
  
    [textField layoutIfNeeded];
    [[UtillClass sharedInstance] HideTfErrorMsg];
    
    if (_tf_username.text.length) {
        if ([[UtillClass sharedInstance] validateEmailWithString:_tf_username.text]) {
            _img_check.hidden=NO;
         
        }
        else{
            _img_check.hidden=YES;
            
        }
}
    else{
        _img_check.hidden=YES;
        
    }
    
    if (textField==_tf_username) {
        _img_check.hidden=YES;
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
