//
//  RegEmailViewController.h
//  Uber Reward
//
//  Created by Arvind on 23/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NIDropDown.h"
#import "UtillClass.h"
#import "BSErrorMessageView.h"
#import "UITextField+BSErrorMessageView.h"
#import "SlidingViewController.h"
@interface RegEmailViewController : UIViewController<NIDropDownDelegate,slideViewControllerDelegate>{
    NSMutableArray*arrdays;
    NSMutableArray *arr_month;
    NSMutableArray *arryear;
    NSMutableArray* arr_monthindex;
     NIDropDown *dropDown;
    int index;
    NSString*strday;
    SlidingViewController *slideViewMenu;
    BOOL date;
    BOOL date2;
}
@property (strong, nonatomic)  NSString * usermob;
@property (strong, nonatomic)  NSString * cid;

@property (strong, nonatomic) IBOutlet UIScrollView *scr_emailpage;
@property (weak, nonatomic) IBOutlet UIImageView *img_check1;
@property (weak, nonatomic) IBOutlet UIImageView *img_check2;
@property (weak, nonatomic) IBOutlet UITextField *tf_email;
@property (weak, nonatomic) IBOutlet UITextField *tf_conf_email;
@property (weak, nonatomic) IBOutlet UITextField *tf_psw;
@property (weak, nonatomic) IBOutlet UITextField *tf_conf_psw;
@property (weak, nonatomic) IBOutlet UIButton *btn_terms_condition;
@property (weak, nonatomic) IBOutlet UIButton *btn_checkt_c;
@property (weak, nonatomic) IBOutlet UIButton *btn_submit;
@property (strong, nonatomic) IBOutlet UIButton* btn_day;
@property (strong, nonatomic) IBOutlet UIButton* btn_month;
@property (strong, nonatomic) IBOutlet UIButton* btn_year;
@end
