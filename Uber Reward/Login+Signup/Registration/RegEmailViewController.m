//
//  RegEmailViewController.m
//  Uber Reward
//
//  Created by Arvind on 23/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import "RegEmailViewController.h"
#import "BSErrorMessageView.h"
#import "UITextField+BSErrorMessageView.h"
#import "SuccessViewController.h"
@interface RegEmailViewController ()

@end

@implementation RegEmailViewController
@synthesize usermob,cid;
-(IBAction)backregtowel:(id)sender{
    
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"RegistraionViewController"];
    [self presentViewController:vc animated:false completion:nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"ios  Registration Screen 2"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    BOOL date=NO;
    BOOL date2=NO;
    CGRect contentRect = CGRectZero;
    
    
    if([UIScreen mainScreen].bounds.size.height==812 ){
        
        [_img_check1 setFrame:CGRectMake(_img_check1.frame.origin.x, _img_check1.frame.origin.y+2, 20, 20)];
        [_img_check2 setFrame:CGRectMake(_img_check2.frame.origin.x, _img_check2.frame.origin.y+2, 20, 20)];
        
        
        
    }
    
    
    for (UIView *view in _scr_emailpage.subviews) {
        contentRect = CGRectUnion(contentRect, view.frame);
    }
    _scr_emailpage.contentSize = CGSizeMake(0, self.view.frame.size.height+120) ;
    // Do any additional setup after loading the view.
}
- (void)viewWillDisappear:(BOOL)animated{
     [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"dob"];
}
- (void)viewDidDisappear:(BOOL)animated{
     [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"dob"];
}
-(void)viewWillAppear:(BOOL)animated{
    
    [[NSUserDefaults standardUserDefaults] setValue:@"yes" forKey:@"dob"];
    _tf_psw.layer.borderWidth= 0.6;
    _tf_psw.layer.borderColor=[UIColor colorWithRed:192/255.0f green:192/255.0f blue:192/255.0f alpha:1].CGColor;
   _tf_psw.layer.cornerRadius=1;
    
    _tf_conf_psw.layer.borderWidth= 0.6;
    _tf_conf_psw.layer.borderColor=[UIColor colorWithRed:192/255.0f green:192/255.0f blue:192/255.0f alpha:1].CGColor;
    _tf_conf_psw.layer.cornerRadius=1;
    
    _tf_email.layer.borderWidth= 0.6;
    _tf_email.layer.borderColor=[UIColor colorWithRed:192/255.0f green:192/255.0f blue:192/255.0f alpha:1].CGColor;
    _tf_email.layer.cornerRadius=1;
    
    _tf_conf_email.layer.borderWidth= 0.6;
    _tf_conf_email.layer.borderColor=[UIColor colorWithRed:192/255.0f green:192/255.0f blue:192/255.0f alpha:1].CGColor;
    _tf_conf_email.layer.cornerRadius=1;
    
    _btn_day.layer.borderWidth= 0.6;
    _btn_day.layer.borderColor=[UIColor colorWithRed:192/255.0f green:192/255.0f blue:192/255.0f alpha:1].CGColor;
    _btn_day.layer.cornerRadius=1;
    
    _btn_month.layer.borderWidth= 0.6;
    _btn_month.layer.borderColor=[UIColor colorWithRed:192/255.0f green:192/255.0f blue:192/255.0f alpha:1].CGColor;
    _btn_month.layer.cornerRadius=1;
    
    _btn_year.layer.borderWidth= 0.6;
    _btn_year.layer.borderColor=[UIColor colorWithRed:192/255.0f green:192/255.0f blue:192/255.0f alpha:1].CGColor;
    _btn_year.layer.cornerRadius=1;
    
    
}
-(void)viewDidAppear:(BOOL)animated{
    
    [[UtillClass sharedInstance]HideLoader];
    [self HideSlideMenu];
    arrdays=[[NSMutableArray alloc]init];
    arr_month=[[NSMutableArray alloc]init];
     arr_monthindex=[[NSMutableArray alloc]init];
    arryear=[[NSMutableArray alloc]init];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    NSString *yearString = [formatter stringFromDate:[NSDate date]];
    
    
    int fy=  [yearString intValue]-70;
    int ly=  [yearString intValue]-17;
    
    for (int i = fy; i<ly; i++) {
        
        [arryear addObject:[NSString stringWithFormat:@"%i",i]];
        
    }
    for (int j = 1; j<32; j++) {
        
        [arrdays addObject:[NSString stringWithFormat:@"%i",j]];
        
    }
    if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
         [arr_month addObject:@"Jan"];
        [arr_month addObject:@"Feb"];
        [arr_month addObject:@"Mar"];
        [arr_month addObject:@"Apr"];
        [arr_month addObject:@"May"];
        [arr_month addObject:@"Jun"];
        [arr_month addObject:@"Jul"];
        [arr_month addObject:@"Aug"];
        [arr_month addObject:@"Sept"];
        [arr_month addObject:@"Oct"];
        [arr_month addObject:@"Nov"];
        [arr_month addObject:@"Dec"];
    }
    else{
        [arr_month addObject:@"يناير"];
        [arr_month addObject:@"فبراير"];
        [arr_month addObject:@"مارس"];
        [arr_month addObject:@"أبريل"];
        [arr_month addObject:@"مايو"];
        [arr_month addObject:@"يونيو"];
        [arr_month addObject:@"يوليو"];
        [arr_month addObject:@"أغسطس"];
        [arr_month addObject:@"سبتمبر"];
        [arr_month addObject:@"أكتوبر"];
        [arr_month addObject:@"نوفمبر"];
        [arr_month addObject:@"ديسمبر"];
    }
    
    [arr_monthindex addObject:@"1"];
    [arr_monthindex addObject:@"2"];
    [arr_monthindex addObject:@"3"];
    [arr_monthindex addObject:@"4"];
    [arr_monthindex addObject:@"5"];
    [arr_monthindex addObject:@"6"];
    [arr_monthindex addObject:@"7"];
    [arr_monthindex addObject:@"8"];
    [arr_monthindex addObject:@"9"];
    [arr_monthindex addObject:@"10"];
    [arr_monthindex addObject:@"11"];
    [arr_monthindex addObject:@"12"];
    
}
- (IBAction)select_submit:(id)sender {
    
    [self.view endEditing:YES];
  
   if (!_tf_email.text.length) {
        _img_check1.hidden=YES;

       if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
           [[UtillClass sharedInstance] ShowTfErrorMsg:@"Please provide valid email address. E.g. example@example.com" onTf:_tf_email];
       }
       else{
           [[UtillClass sharedInstance] ShowTfErrorMsg:@"يرجى إدخال بريد إلكتروني صحيح. مثل example@example.com" onTf:_tf_email];
           
       }
       
       
        [_tf_email resignFirstResponder];
    }
   else if (![[UtillClass sharedInstance] validateEmailWithString:_tf_email.text]) {
       _img_check1.hidden=YES;

       if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
           [[UtillClass sharedInstance] ShowTfErrorMsg:@"Please provide valid email address. E.g. example@example.com" onTf:_tf_email];
       }
       else{
           [[UtillClass sharedInstance] ShowTfErrorMsg:@"يرجى إدخال بريد إلكتروني صحيح. مثل example@example.com" onTf:_tf_email];
           
       }
       
       [_tf_email resignFirstResponder];
   }
    else if (!_tf_conf_email.text.length) {
        _img_check2.hidden=YES;
  
        [_tf_conf_email resignFirstResponder];
        
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"Please provide valid email address. E.g. example@example.com" onTf:_tf_conf_email];
        }
        else{
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"يرجى إدخال بريد إلكتروني صحيح. مثل example@example.com" onTf:_tf_conf_email];
            
        }
    }
    
    
    
    else if (![[UtillClass sharedInstance] validateEmailWithString:_tf_conf_email.text]) {
        _img_check2.hidden=YES;
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"Please provide valid email address. E.g. example@example.com" onTf:_tf_conf_email];
        }
        else{
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"يرجى إدخال بريد إلكتروني صحيح. مثل example@example.com" onTf:_tf_conf_email];
            
        }
        [_tf_conf_email resignFirstResponder];
    }
    else if ([_tf_email.text caseInsensitiveCompare:_tf_conf_email.text] != NSOrderedSame) {
        _img_check2.hidden=YES;

        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"Email Address and Confirm Email Address do not match. Please re-enter your email." onTf:_tf_conf_email];
        }
        else{
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"البريد الإلكتروني و تأكيد البريد الإلكتروني غير متطابقين. يرجى إعادة إدخال بريدك الإلكتروني" onTf:_tf_conf_email];
            
        }
        
        [_tf_conf_email resignFirstResponder];
    }
    else if ([_btn_day.titleLabel.text isEqualToString:@"DD"]) {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Alert!!"
                                     message:@"Please enter date"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Ok"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        //Handle your yes please button action here
                                       
                                    }];
  
        [alert addAction:yesButton];
      
        
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please select Date of Birth."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"يرجى تحديد تاريخ الميلاد"];
            
        }
    }
    else if ([_btn_month.titleLabel.text isEqualToString:@"MM"]) {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Alert!!"
                                     message:@"Please enter month"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Ok"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                    
                                    }];
        
        [alert addAction:yesButton];
        
        
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please select Date of Birth."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"يرجى تحديد تاريخ الميلاد"];
            
        }
    }
    else if ([_btn_year.titleLabel.text isEqualToString:@"YYYY"]) {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Alert!!"
                                     message:@"Please enter year"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Ok"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        
                                    }];
        
        [alert addAction:yesButton];
        
        
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
             [[UtillClass sharedInstance] showerrortoastmsg:@"Please select Date of Birth."];
        }
        else{
              [[UtillClass sharedInstance] showerrortoastmsg:@"يرجى تحديد تاريخ الميلاد"];
            
        }
      
        
    }
    else if (date==NO){
        
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Date of Birth must be in between 18 to 70 years."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"يجب أن يكون تاريخ الميلاد بين 18 إلى 70 سنة."];
            
        }
        
 }
    else if (date2==NO){
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Date of Birth must be in between 18 to 70 years."];
        }
        else{
           [[UtillClass sharedInstance] showerrortoastmsg:@"يجب أن يكون تاريخ الميلاد بين 18 إلى 70 سنة."];
            
        }
       
        
    }
    else if (!_tf_psw.text.length) {
  
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"Please provide password." onTf:_tf_psw];
        }
        else{
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"يرجى تقديم كلمة المرور" onTf:_tf_psw];
            
        }
        
        [_tf_psw resignFirstResponder];
    }
    
    
    else if (!_tf_conf_psw.text.length) {
  
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"Please confirm your password." onTf:_tf_conf_psw];
        }
        else{
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"تأكيد كلمة المرور الجديدة" onTf:_tf_conf_psw];
            
        }
        
        
        [_tf_conf_psw resignFirstResponder];
    }
    else if (_tf_conf_psw.text.length<8 && _tf_conf_psw.text.length<8) {
        
        
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Password should be made up of 8 characters."];
        }
        else{
            [[UtillClass sharedInstance]showerrortoastmsg:@"يجب أن تتكون كلمة المرور من 8 أحرف"];
            
        }
        
    }
//    else if (_tf_conf_psw.text.length<8) {
//      
//  
//        
//        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
//            [[UtillClass sharedInstance] showerrortoastmsg:@"Password should be made up of 8 characters"];
//        }
//        else{
//            [[UtillClass sharedInstance] ShowTfErrorMsg:@"يجب أن تتكون كلمة المرور من 8 أحرف" onTf:_tf_conf_psw];
//            
//        }
//        
//    }
    else if (![_tf_psw.text isEqualToString:_tf_conf_psw.text]) {
        
      
        [_tf_conf_psw resignFirstResponder];
        
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"Password and Confirm Password do not match. Please re-enter your password." onTf:_tf_conf_psw];
        }
        else{
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"كلمة المرور وتأكيد كلمة المرور غير متطابقين. يرجى إعادة إدخال كلمة المرور.  " onTf:_tf_conf_psw];
            
        }
        
    }
    else if (_btn_checkt_c.tag==1) {
       
        
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
             [[UtillClass sharedInstance] showerrortoastmsg:@"Please accept the Uber PLATINUM Terms & Conditions"];
        }
        else{
         [[UtillClass sharedInstance] showerrortoastmsg:@"أقبل شروط وأحكام برنامج أوبرللمكافآت"];
            
        }
        
    }
    
    else{
        [self.view endEditing:YES];
        
        if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
            if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        };
            
        }
        else{
            [[UtillClass sharedInstance] ShowLoaderOn:self.parentViewController withtext:@"Please wait"];
            NSMutableDictionary*dicts = [[NSMutableDictionary alloc]init];
            
            
            NSString *strmonth;
            if (![[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                if ([_btn_month.titleLabel.text isEqualToString:@"يناير"]) {
                    strmonth  =@"Jan";
                }
                else if ([_btn_month.titleLabel.text isEqualToString:@"فبراير"]) {
                    strmonth  =@"Feb";
                }
                else if ([_btn_month.titleLabel.text isEqualToString:@"مارس"]) {
                    strmonth  =@"Mar";
                }
                else if ([_btn_month.titleLabel.text isEqualToString:@"أبريل"]) {
                    strmonth  =@"Apr";
                }
                else if ([_btn_month.titleLabel.text isEqualToString:@"مايو"]) {
                    strmonth  =@"May";
                } else if ([_btn_month.titleLabel.text isEqualToString:@"يونيو"]) {
                    strmonth  =@"Jun";
                }
                else if ([_btn_month.titleLabel.text isEqualToString:@"أغسطس"]) {
                    strmonth  =@"Jul";
                }
                else if ([_btn_month.titleLabel.text isEqualToString:@"سبتمبر"]) {
                    strmonth  =@"Aug";
                }
                else if ([_btn_month.titleLabel.text isEqualToString:@"سبتمبر"]) {
                    strmonth  =@"Sept";
                }
                else if ([_btn_month.titleLabel.text isEqualToString:@"أكتوبر"]) {
                    strmonth  =@"Oct";
                }
                else if ([_btn_month.titleLabel.text isEqualToString:@"نوفمبر"]) {
                    strmonth  =@"Nov";
                }
                else if ([_btn_month.titleLabel.text isEqualToString:@"ديسمبر"]) {
                    strmonth  =@"Dec";
                }
            }
            else{
                strmonth = _btn_month.titleLabel.text;
            }
            
            [dicts setValue:[[UtillClass sharedInstance] getcultureID] forKey:@"CultureCurrentId"];
            [dicts setValue:cid forKey:@"CountryId"];
            [dicts setValue:@"aa" forKey:@"FirstName"];
            [dicts setValue:@"aa" forKey:@"LastName"];
            [dicts setValue:_tf_conf_psw.text forKey:@"NPassword"];
            [dicts setValue:[_tf_conf_email.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"Email"];
            [dicts setValue:[[UtillClass sharedInstance] getcultureID] forKey:@"CultureId"];
            [dicts setValue:[NSString stringWithFormat:@"%@/%@/%@",_btn_day.titleLabel.text,strmonth,_btn_year.titleLabel.text] forKey:@"DOB"];
            [dicts setValue:@"aa" forKey:@"SecurityAnswer"];
            [dicts setValue:@"" forKey:@"SQId"];
            [dicts setValue:@"" forKey:@"TitleId"];
            [dicts setValue:usermob forKey:@"Mobile"];
            [dicts setValue:cid forKey:@"CountryCodeIdId"];
            [dicts setValue:@"" forKey:@"CustomerQuestionId"];
            [dicts setValue:@"" forKey:@"BottleNumber"];
            [dicts setValue:@"" forKey:@"BrandName"];
            [dicts setValue:@"" forKey:@"UserImage"];
            [dicts setValue:[[UtillClass sharedInstance] getnotiid] forKey:@"Device_RegId"];
            
            
            [[UtillClass sharedInstance] Postresponsetoservice:dicts withBearer:nil withurl:[NSString stringWithFormat:@"Account/UserRegistration?CultureId=%@&MediumTypeId=2&deviceId=%@",[[UtillClass sharedInstance] getcultureID],[[UtillClass sharedInstance] getdeviceid]] response:^(NSDictionary * dict_response) {
                [[UtillClass sharedInstance] HideLoader];
                
                if (dict_response ==nil) {
                    [[UtillClass sharedInstance] showerrortoastmsg:@"Server error, Please try after sometime."];
                    
                }
                else{
                    if ([[dict_response objectForKey:@"Status"] integerValue] == 1) {
                       
                        [[UtillClass sharedInstance]showtoastmsg:[dict_response objectForKey:@"msg"]];
                        SuccessViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"SuccessViewController"];
                      
                        
                        [self presentViewController:vc animated:false completion:nil];
                    }
                    else{
                        [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
                       
                    }
                }
            }];
        }
        
        
    }
}
-(void)showtf_error:(UITextField*)tf{
    
    
    
}
- (IBAction)selcet_date:(id)sender {
    [self OpenDropDown:sender Array:[arrdays copy]];
    
    
}
- (IBAction)selcet_month:(id)sender {
    [self OpenDropDown:sender Array:[arr_month copy]];
    
    
}
- (IBAction)selcet_year:(id)sender {
    [self OpenDropDown:sender Array:[arryear copy]];
    
    
    
    
}

- (void) niDropDownDelegateMethod: (NSString *)btntext withbtntag:(NSString *)btntag dropdown: (NIDropDown *) sender {
    dropDown = nil;
    NSString *strmonth;
    //    if([[btntext substringToIndex:1]  isEqualToString:@"+"]){
    //
    //
    //        NSRange range = NSMakeRange(0,1);
    //        btntext = [btntext stringByReplacingCharactersInRange:range withString:@""];
    //
    //    }
    NSString * language = [[NSLocale preferredLanguages] firstObject];
    
    if([[language substringToIndex:2] isEqualToString:@"ar"]){
        
        strmonth = _btn_month.titleLabel.text;
    }
    else{
        if (![[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"] ) {
            if ([_btn_month.titleLabel.text isEqualToString:@"يناير"]) {
                strmonth  =@"Jan";
            }
            else if ([_btn_month.titleLabel.text isEqualToString:@"فبراير"]) {
                strmonth  =@"Feb";
            }
            else if ([_btn_month.titleLabel.text isEqualToString:@"مارس"]) {
                strmonth  =@"Mar";
            }
            else if ([_btn_month.titleLabel.text isEqualToString:@"أبريل"]) {
                strmonth  =@"Apr";
            }
            else if ([_btn_month.titleLabel.text isEqualToString:@"مايو"]) {
                strmonth  =@"May";
            } else if ([_btn_month.titleLabel.text isEqualToString:@"يونيو"]) {
                strmonth  =@"Jun";
            }
            else if ([_btn_month.titleLabel.text isEqualToString:@"أغسطس"]) {
                strmonth  =@"Jul";
            }
            else if ([_btn_month.titleLabel.text isEqualToString:@"سبتمبر"]) {
                strmonth  =@"Aug";
            }
            else if ([_btn_month.titleLabel.text isEqualToString:@"سبتمبر"]) {
                strmonth  =@"Sept";
            }
            else if ([_btn_month.titleLabel.text isEqualToString:@"أكتوبر"]) {
                strmonth  =@"Oct";
            }
            else if ([_btn_month.titleLabel.text isEqualToString:@"نوفمبر"]) {
                strmonth  =@"Nov";
            }
            else if ([_btn_month.titleLabel.text isEqualToString:@"ديسمبر"]) {
                strmonth  =@"Dec";
            }
        }
        else{
            strmonth = _btn_month.titleLabel.text;
        }
    }
    
    if([btntag isEqualToString:@"2"])
    {
        [_btn_day setTitle:[[NSString stringWithFormat:@"%@", [[btntext componentsSeparatedByString:@","]objectAtIndex:0]] substringFromIndex:1] forState:UIControlStateNormal];
        strday =[[NSString stringWithFormat:@"%@", [[btntext componentsSeparatedByString:@","]objectAtIndex:0]] substringFromIndex:1];
        if (![[[NSString stringWithFormat:@"%@", [[btntext componentsSeparatedByString:@","]objectAtIndex:0]] substringFromIndex:1] isEqualToString:@"DD"] && ![_btn_month.titleLabel.text isEqualToString:@"MM"] && ![_btn_year.titleLabel.text isEqualToString:@"YYYY"]  ) {
            NSDateFormatter *formate = [[NSDateFormatter alloc] init];
            [formate setDateFormat:@"dd/MM/yyyy"];
            
            if([[language substringToIndex:2] isEqualToString:@"ar"] && [[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]){
                [formate setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];;
            }
            NSDate *nextDate = [formate dateFromString:[NSString stringWithFormat:@"%@/%@/%@",[[NSString stringWithFormat:@"%@", [[btntext componentsSeparatedByString:@","]objectAtIndex:0]] substringFromIndex:1],strmonth,_btn_year.titleLabel.text]];
            
            NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
            dayComponent.day = 1;
            NSCalendar *theCalendar = [NSCalendar currentCalendar];
            NSDate *date1 = [theCalendar dateByAddingComponents:dayComponent toDate:nextDate options:0];
            
            
            unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
            NSDate *now = [NSDate date];
            NSLog(@"TodayDate = %@",now);
            NSCalendar *gregorian = [NSCalendar currentCalendar];
            NSDateComponents *comps = [gregorian components:unitFlags fromDate:now];
            [comps setYear:[comps year] - 70];
            NSDate *SeventyYearsAgo = [gregorian dateFromComponents:comps];
            
            NSDateComponents *comps2 = [gregorian components:unitFlags fromDate:nextDate];
            [comps2 setYear:[comps2 year] + 18];
            NSDate *EighteenYearAfter = [gregorian dateFromComponents:comps2];
            
            
            date =NO;
            date2 =NO;
            
            if([SeventyYearsAgo compare:date1] == NSOrderedAscending)
            {
                NSLog(@"today is less -2 ");
                date = YES;
            }
            else if([SeventyYearsAgo compare:date1] == NSOrderedDescending)
            {
                NSLog(@"newDate is less-2 ");
            }
            else
            {
                NSLog(@"Both dates are same-2");
            }
            //Eighteen
            
            if([EighteenYearAfter compare:now] == NSOrderedDescending)
            {
                NSLog(@"today is less -2 ");
                date2 = NO;
            }
            
            else
            {
                NSLog(@"Both dates are same-2");
                date2 =YES;
            }
            
            
            
            if (date==NO) {
                if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                    [[UtillClass sharedInstance] showerrortoastmsg:@"Date of Birth must be in between 18 to 70 years."];
                }
                else{
                    [[UtillClass sharedInstance] showerrortoastmsg:@"يجب أن يكون تاريخ الميلاد بين 18 إلى 70 سنة."];
                    
                }
            }
            else if (date2==NO) {
                if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                    [[UtillClass sharedInstance] showerrortoastmsg:@"Date of Birth must be in between 18 to 70 years."];
                }
                else{
                    [[UtillClass sharedInstance] showerrortoastmsg:@"يجب أن يكون تاريخ الميلاد بين 18 إلى 70 سنة."];
                    
                }
            }
            
        }
    }
    else if([btntag isEqualToString:@"3"])
    {
        [_btn_month setTitle:[[NSString stringWithFormat:@"%@", [[btntext componentsSeparatedByString:@","]objectAtIndex:0]] substringFromIndex:0] forState:UIControlStateNormal];
        
        index = [[arr_monthindex objectAtIndex:[[[btntext componentsSeparatedByString:@","]objectAtIndex:1] intValue]]intValue];
        if (![_btn_day.titleLabel.text isEqualToString:@"DD"] && ![[[NSString stringWithFormat:@"%@", [[btntext componentsSeparatedByString:@","]objectAtIndex:0]] substringFromIndex:0] isEqualToString:@"MM"] && ![_btn_year.titleLabel.text isEqualToString:@"YYYY"]  ) {
            NSDateFormatter *formate = [[NSDateFormatter alloc] init];
            [formate setDateFormat:@"dd/MM/yyyy"];
            if([[language substringToIndex:2] isEqualToString:@"ar"] && [[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]){
                [formate setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];;
            }
            NSDate *nextDate;
            if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                nextDate = [formate dateFromString:[NSString stringWithFormat:@"%@/%@/%@",_btn_day.titleLabel.text,[[NSString stringWithFormat:@"%@", [[btntext componentsSeparatedByString:@","]objectAtIndex:0]] substringFromIndex:0],_btn_year.titleLabel.text]];
            }
            else{
                if ([[language substringToIndex:2] isEqualToString:@"ar"]){
                    nextDate = [formate dateFromString:[NSString stringWithFormat:@"%@/%@/%@",_btn_day.titleLabel.text,[[NSString stringWithFormat:@"%@", [[btntext componentsSeparatedByString:@","]objectAtIndex:0]] substringFromIndex:0],_btn_year.titleLabel.text]];
                    
                }
                [formate dateFromString:[NSString stringWithFormat:@"%@/%@/%@",_btn_day.titleLabel.text,[[NSString stringWithFormat:@"%@", [[btntext componentsSeparatedByString:@","]objectAtIndex:1]] substringFromIndex:0],_btn_year.titleLabel.text]];
                
            }
            NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
            dayComponent.day = 1;
            NSCalendar *theCalendar = [NSCalendar currentCalendar];
            NSDate *date1 = [theCalendar dateByAddingComponents:dayComponent toDate:nextDate options:0];
            
            unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
            NSDate *now = [NSDate date];
            NSLog(@"TodayDate = %@",now);
            NSCalendar *gregorian = [NSCalendar currentCalendar];
            NSDateComponents *comps = [gregorian components:unitFlags fromDate:now];
            [comps setYear:[comps year] - 70];
            NSDate *SeventyYearsAgo = [gregorian dateFromComponents:comps];
            
            
            NSDateComponents *comps2 = [gregorian components:unitFlags fromDate:nextDate];
            [comps2 setYear:[comps2 year] + 18];
            NSDate *EighteenYearAfter = [gregorian dateFromComponents:comps2];
            
            
            date =NO;
            date2 =NO;
            
            if([SeventyYearsAgo compare:date1] == NSOrderedAscending)
            {
                NSLog(@"today is less -2 ");
                date = YES;
            }
            else if([SeventyYearsAgo compare:date1] == NSOrderedDescending)
            {
                NSLog(@"newDate is less-2 ");
            }
            else
            {
                NSLog(@"Both dates are same-2");
            }
            //Eighteen
            
            if([EighteenYearAfter compare:now] == NSOrderedDescending)
            {
                NSLog(@"today is less -2 ");
                date2 = NO;
            }
            
            else
            {
                NSLog(@"Both dates are same-2");
                date2 =YES;
            }
            
            
            
            if (date==NO) {
                if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                    [[UtillClass sharedInstance] showerrortoastmsg:@"Date of Birth must be in between 18 to 70 years."];
                }
                else{
                    [[UtillClass sharedInstance] showerrortoastmsg:@"يجب أن يكون تاريخ الميلاد بين 18 إلى 70 سنة."];
                    
                }
            }
            else if (date2==NO) {
                if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                    [[UtillClass sharedInstance] showerrortoastmsg:@"Date of Birth must be in between 18 to 70 years."];
                }
                else{
                    [[UtillClass sharedInstance] showerrortoastmsg:@"يجب أن يكون تاريخ الميلاد بين 18 إلى 70 سنة."];
                    
                }
            }
            
        }
    }
    else if([btntag isEqualToString:@"4"])
    {
        [_btn_year setTitle:[[NSString stringWithFormat:@"%@", [[btntext componentsSeparatedByString:@","]objectAtIndex:0]] substringFromIndex:1] forState:UIControlStateNormal];
        
        if ([self getdaysinmoth:index andyear:[[[btntext componentsSeparatedByString:@","]objectAtIndex:0] intValue]]< [strday intValue] ) {
            
            [_btn_day setTitle:[NSString stringWithFormat:@"%d", [self getdaysinmoth:index andyear:[[[btntext componentsSeparatedByString:@","]objectAtIndex:0] intValue]]] forState:UIControlStateNormal];
            
        }
        if (![_btn_day.titleLabel.text isEqualToString:@"DD"] && ![_btn_month.titleLabel.text isEqualToString:@"MM"] && ![[[NSString stringWithFormat:@"%@", [[btntext componentsSeparatedByString:@","]objectAtIndex:0]] substringFromIndex:1] isEqualToString:@"YYYY"]  ) {
            
            
            NSDateFormatter *formate = [[NSDateFormatter alloc] init];
            [formate setDateFormat:@"dd/MM/yyyy"];
            if([[language substringToIndex:2] isEqualToString:@"ar"] && [[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]){
                [formate setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];;
            }
            
            NSDate *nextDate = [formate dateFromString:[NSString stringWithFormat:@"%@/%@/%@",_btn_day.titleLabel.text,strmonth,[[NSString stringWithFormat:@"%@", [[btntext componentsSeparatedByString:@","]objectAtIndex:0]] substringFromIndex:1]]];
            
            NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
            dayComponent.day = 1;
            NSCalendar *theCalendar = [NSCalendar currentCalendar];
            NSDate *date1 = [theCalendar dateByAddingComponents:dayComponent toDate:nextDate options:0];
            
            unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
            NSDate *now = [NSDate date];
            NSLog(@"TodayDate = %@",now);
            NSCalendar *gregorian = [NSCalendar currentCalendar];
            NSDateComponents *comps = [gregorian components:unitFlags fromDate:now];
            [comps setYear:[comps year] - 70];
            NSDate *SeventyYearsAgo = [gregorian dateFromComponents:comps];
            
            
            NSDateComponents *comps2 = [gregorian components:unitFlags fromDate:nextDate];
            [comps2 setYear:[comps2 year] + 18];
            NSDate *EighteenYearAfter = [gregorian dateFromComponents:comps2];
            
            
            date =NO;
            date2 =NO;
            
            if([SeventyYearsAgo compare:date1] == NSOrderedAscending)
            {
                NSLog(@"today is less -2 ");
                date = YES;
            }
            else if([SeventyYearsAgo compare:date1] == NSOrderedDescending)
            {
                NSLog(@"newDate is less-2 ");
            }
            else
            {
                NSLog(@"Both dates are same-2");
            }
            //Eighteen
            
            if([EighteenYearAfter compare:now] == NSOrderedDescending)
            {
                NSLog(@"today is less -2 ");
                date2 = NO;
            }
            
            else
            {
                NSLog(@"Both dates are same-2");
                date2 =YES;
            }
            
            
            
            if (date==NO) {
                if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                    [[UtillClass sharedInstance] showerrortoastmsg:@"Date of Birth must be in between 18 to 70 years."];
                }
                else{
                    [[UtillClass sharedInstance] showerrortoastmsg:@"يجب أن يكون تاريخ الميلاد بين 18 إلى 70 سنة."];
                    
                }
            }
            else if (date2==NO) {
                if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                    [[UtillClass sharedInstance] showerrortoastmsg:@"Date of Birth must be in between 18 to 70 years."];
                }
                else{
                    [[UtillClass sharedInstance] showerrortoastmsg:@"يجب أن يكون تاريخ الميلاد بين 18 إلى 70 سنة."];
                    
                }
            }
        }
    }
    
    
}
-(int)getdaysinmoth:(int )month andyear:(int )year{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    
    // Set your year and month here
    [components setYear:year];
    [components setMonth:month];
    
    NSDate *date = [calendar dateFromComponents:components];
    NSRange range = [calendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:date];
    
    NSLog(@"%d", (int)range.length);
    return (int)range.length;
}
#pragma mark - DropDown Delegate
- (void)OpenDropDown:(UIButton *)sender Array:(NSArray *) arr
{
    [self.view endEditing:YES];
    //    arr = [[NSArray alloc] init];
    
    NSArray * arrImage = [[NSArray alloc] init];
   
    if(dropDown == nil) {
        CGFloat f = 200;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :arrImage :@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        dropDown = nil;
        
    }
}



-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    if (textField==_tf_email) {
        if ([[UtillClass sharedInstance] validateEmailWithString:_tf_email.text]) {
            _img_check1.hidden=NO;
        }
        else{
            _img_check1.hidden=YES;
        }
    }
    if (textField==_tf_conf_email) {
        if ([[UtillClass sharedInstance] validateEmailWithString:_tf_conf_email.text]) {
            _img_check2.hidden=NO;
        }
        else{
            _img_check2.hidden=YES;
        }
    }
    CGPoint scrollPoint = CGPointMake(0, 0);
    
    [_scr_emailpage setContentOffset:scrollPoint animated:YES];
    [textField resignFirstResponder];
    return YES;
}
#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    [textField layoutIfNeeded];
    [[UtillClass sharedInstance] HideTfErrorMsg];
    if (_tf_email.text.length) {
        if ([[UtillClass sharedInstance] validateEmailWithString:_tf_email.text]) {
            _img_check1.hidden=NO;
            
        }
        else{
            _img_check1.hidden=YES;
            
            
        }
        
    }
    else if(_tf_conf_email.text.length) {
        if ([[UtillClass sharedInstance] validateEmailWithString:_tf_conf_email.text]) {
            _img_check2.hidden=NO;
            
        }
        else{
            _img_check2.hidden=YES;
            
            
        }
        
    }
    else{
        _img_check1.hidden=YES;
        
    }
    
    float scrollOffset = _scr_emailpage.contentOffset.y;
    if (scrollOffset!=0) {
        
    }
    else{
    if ((textField.tag==7) || (textField.tag==8)) {
        CGPoint scrollPoint = CGPointMake(0, 160);
        
        [_scr_emailpage setContentOffset:scrollPoint animated:YES];
    }
    }
    if (textField==_tf_email) {
        _img_check1.hidden=YES;
    }
    else if (textField==_tf_conf_email) {
        _img_check2.hidden=YES;
    }
}

- (IBAction)select_sidemenu:(id)sender {
    if (slideViewMenu.view) {
        [self HideSlideMenu];
    } else {
        slideViewMenu = [[UtillClass sharedInstance] CallSlideMenuView:self yVal:0.0f];
        slideViewMenu.Str_isLogIn = @"noLogin";
    }
}

#pragma mark - slideView Delegate

-(void) slideViewControllerDone:(SlidingViewController *)sender
{
    if (sender) {
        [self HideSlideMenu];
    }
}
-(void) HideSlideMenu
{
    [UIView animateWithDuration:0.50 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        //code with animation
        // slideViewMenu.view.frame = CGRectMake(-self.view.frame.size.width, 0.0f, self.view.frame.size.width, self.view.frame.size.height - 0.0f);
        [slideViewMenu.view removeFromSuperview];
    } completion:^(BOOL finished) {
        //code for completion
        [slideViewMenu.view removeFromSuperview];
        slideViewMenu = nil;
        
        
    }];
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField==_tf_email) {
        if ([[UtillClass sharedInstance] validateEmailWithString:_tf_email.text]) {
            _img_check1.hidden=NO;
        }
        else{
            _img_check1.hidden=YES;
        }
    }
    if (textField==_tf_conf_email) {
        if ([[UtillClass sharedInstance] validateEmailWithString:_tf_conf_email.text]) {
            _img_check2.hidden=NO;
        }
        else{
            _img_check2.hidden=YES;
        }
    }
    
    float scrollOffset = _scr_emailpage.contentOffset.y;
    if (scrollOffset!=0) {
        
    }else{
    [_scr_emailpage setContentOffset:CGPointMake(0,0) animated:NO];
    }
}
    
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)select_check:(id)sender {
    if ([_btn_checkt_c.currentBackgroundImage isEqual:[UIImage imageNamed:@"checkbox.png"]]) {
        _btn_checkt_c.tag=2;
        [_btn_checkt_c setBackgroundImage:[UIImage imageNamed:@"checkbox-tick.png"] forState:UIControlStateNormal];
    }
    else{
        _btn_checkt_c.tag=1;
        [_btn_checkt_c setBackgroundImage:[UIImage imageNamed:@"checkbox.png"] forState:UIControlStateNormal];
    }
}
- (IBAction)TandC:(id)sender {
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    app.ViewWebTypeStr = @"T&C";
   
    noLOginWebViewViewController *controller = [[noLOginWebViewViewController alloc] initWithNibName:[[UtillClass sharedInstance] getnologinxib] bundle:nil];
    [self presentViewController:controller animated:false completion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
