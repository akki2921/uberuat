//
//  RegistraionViewController.h
//  Uber Reward
//
//  Created by Arvind Seth on 22/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UtillClass.h"
#import "NIDropDown.h"
#import "SlidingViewController.h"

@interface RegistraionViewController : UIViewController<NIDropDownDelegate,slideViewControllerDelegate>{
    
    IBOutlet UIButton *btn_selcode;
    SlidingViewController *slideViewMenu;
    NSMutableArray*arr_imgflag;
    NSMutableArray*arr_cname;
    NSMutableArray*arr_cid;
    NSMutableArray*arr_mobdigit;
    NIDropDown *dropDown;
    NSString *strcode;
    int maxdigit;
    NSTimer*VerifyBtnTimer;
    NSTimer *t;
    int count;
    BOOL resend;
}
@property (strong, nonatomic) IBOutlet UIView *view_bottomhide;
@property (strong, nonatomic) IBOutlet UIScrollView *scr_emailpage;
@property (strong, nonatomic) IBOutlet UILabel *lbl_otpshow;
@property (weak, nonatomic) IBOutlet UIButton *btn_check;
@property (weak, nonatomic) IBOutlet UIView *view_timer_hidden;
@property (weak, nonatomic) IBOutlet UILabel *lbl_hidden_submit;
@property (strong, nonatomic) IBOutlet UILabel *lbl_hidden;
@property (weak, nonatomic) IBOutlet UIImageView *img_flag;
@property (weak, nonatomic) IBOutlet UILabel *lbl_ccode;
@property (weak, nonatomic) IBOutlet UITextField *tf_mob;
@property (weak, nonatomic) IBOutlet UIButton *btn_submit;
@property (weak, nonatomic) IBOutlet UITextField *tf1;
@property (weak, nonatomic) IBOutlet UITextField *tf2;
@property (weak, nonatomic) IBOutlet UITextField *tf3;
@property (weak, nonatomic) IBOutlet UITextField *tf4;
@property (weak, nonatomic) IBOutlet UITextField *tf5;
@property (weak, nonatomic) IBOutlet UITextField *tf6;
@property (weak, nonatomic) IBOutlet UIButton* btn_day;
@property (weak, nonatomic) IBOutlet UIButton* btn_month;
@property (weak, nonatomic) IBOutlet UIButton* btn_year;
@property (weak, nonatomic) IBOutlet UIButton *btn_verify;

@end
