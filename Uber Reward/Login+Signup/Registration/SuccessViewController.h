//
//  SuccessViewController.h
//  Uber Reward
//
//  Created by Arvind on 23/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlidingViewController.h"
#import <MessageUI/MessageUI.h>

@interface SuccessViewController : UIViewController<slideViewControllerDelegate,MFMailComposeViewControllerDelegate>{
    
    SlidingViewController *slideViewMenu;
    
}

@end
