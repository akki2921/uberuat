//
//  ViewController.h
//  Uber Reward
//
//  Created by Arvind Seth on 22/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UtillClass.h"
#import "LoginViewController.h"

@interface SplashbeforeRegViewController : UIViewController{
    
        IBOutlet UIImageView *imgsplash;
    
}
@property (strong, nonatomic) IBOutlet UIImageView *img_splashlogo;
@property (strong, nonatomic) IBOutlet UILabel *lbl_arabic_Sel;
@property (weak, nonatomic) IBOutlet UIButton *btn_english;
@property (weak, nonatomic) IBOutlet UIButton *btn_arabic;


@end

