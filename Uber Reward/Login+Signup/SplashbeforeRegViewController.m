
//
//  ViewController.m
//  Uber Reward
//
//  Created by Arvind Seth on 22/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import "SplashbeforeRegViewController.h"

@interface SplashbeforeRegViewController ()

@end

@implementation SplashbeforeRegViewController
-(void) viewWillAppear:(BOOL)animated{
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"ios  Splash Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
   
}
- (void)viewDidLoad {
    [super viewDidLoad];
    if([UIScreen mainScreen].bounds.size.height==812 ){
       _lbl_arabic_Sel.hidden=YES;
        _img_splashlogo.hidden=YES;
        [imgsplash setImage:[UIImage imageNamed:@"splash_x.jpg"]];
        [_btn_arabic setFrame:CGRectMake(_btn_arabic.frame.origin.x, _btn_arabic.frame.origin.y, _btn_arabic.frame.size.width, 424)];
        [_btn_english setFrame:CGRectMake(_btn_english.frame.origin.x, _btn_english.frame.origin.y, _btn_english.frame.size.width, 424)];
        
    }
    // Do any additional setup after loading the view, typically from a nib.
}
-(IBAction)select_btn_language:(UIButton*)sender{
    if (sender.tag==1) {
        [[UtillClass sharedInstance] set_app_language:@"English"];
        
      }
    else{
      [[UtillClass sharedInstance] set_app_language:@"Arabic"];
    }
    
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"WelcomeViewController"];
    [self presentViewController:vc animated:false completion:nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
