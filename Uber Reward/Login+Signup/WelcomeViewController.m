//
//  WelcomeViewController.m
//  Uber Reward
//
//  Created by Arvind on 23/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import "WelcomeViewController.h"

@interface WelcomeViewController ()

@end

@implementation WelcomeViewController

-(void)viewWillAppear:(BOOL)animated{
  
    [self HideSlideMenu];
    [[UtillClass sharedInstance] HideLoader];
  
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"ios  Welcome Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (IBAction)select_sidemenu:(id)sender {
    if (slideViewMenu.view) {
        [self HideSlideMenu];
    } else {
        slideViewMenu = [[UtillClass sharedInstance] CallSlideMenuView:self yVal:0.0f];
        slideViewMenu.Str_isLogIn = @"noLogin";
    }
}
- (IBAction)select_back:(id)sender {
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"SplashbeforeRegViewController"];
    [self presentViewController:vc animated:false completion:nil];
    
}
- (IBAction)select_Login:(id)sender {
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"LoginViewController"];
            [self presentViewController:vc animated:false completion:nil];
    
}
- (IBAction)select_CreatReg:(id)sender {
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"RegistraionViewController"];
           [self presentViewController:vc animated:false completion:nil];
    
}
#pragma mark - slideView Delegate

-(void) slideViewControllerDone:(SlidingViewController *)sender
{
    if (sender) {
        [self HideSlideMenu];
    }
}
- (IBAction)clickhereW:(id)sender {
    
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    app.ViewWebTypeStr = @"Welcome";
    noLOginWebViewViewController *controller = [[noLOginWebViewViewController alloc] initWithNibName:[[UtillClass sharedInstance] getnologinxib] bundle:nil];
    [self presentViewController:controller animated:false completion:nil];
    
}
-(void) HideSlideMenu
{
    [UIView animateWithDuration:0.50 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        //code with animation
        // slideViewMenu.view.frame = CGRectMake(-self.view.frame.size.width, 0.0f, self.view.frame.size.width, self.view.frame.size.height - 0.0f);
        [slideViewMenu.view removeFromSuperview];
    } completion:^(BOOL finished) {
        //code for completion
        [slideViewMenu.view removeFromSuperview];
        slideViewMenu = nil;
        
        
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)select_clicking_here:(id)sender {
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
