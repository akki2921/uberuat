//
//  AbhiTxtFeildValidation.h
//  AbhiTextFeildValidation
//
//  Created by 9Dim on 23/03/18.
//  Copyright © 2018 9Dim. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AbhiTxtFeildValidation;
@protocol AbhiTxtFeildValidationDelegate
- (void) AbhiTxtFeildValidationDelegateMethod: (AbhiTxtFeildValidation *) sender;
@end

@interface AbhiTxtFeildValidation : UIView
{
    UITextField *tempTxtFeild;
}
@property(nonatomic, strong) UIView *BGviewBottom;

@property (nonatomic, retain) id <AbhiTxtFeildValidationDelegate> delegate;

- (id)showAbhiTxtFeildValidation:(NSString *) ErrorMsgStr txtFeild:(UITextField *) txtFeild;

-(void) setHidden:(BOOL)hidden;

@end
