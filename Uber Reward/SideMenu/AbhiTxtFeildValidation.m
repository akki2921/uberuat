//
//  AbhiTxtFeildValidation.m
//  AbhiTextFeildValidation
//
//  Created by 9Dim on 23/03/18.
//  Copyright © 2018 9Dim. All rights reserved.
//

#import "AbhiTxtFeildValidation.h"

@implementation AbhiTxtFeildValidation
@synthesize BGviewBottom;


- (id)showAbhiTxtFeildValidation:(NSString *) ErrorMsgStr txtFeild:(UITextField *) txtFeild
{
    BGviewBottom = (UIView *)[super init];
    BGviewBottom.userInteractionEnabled = YES;
    
    if (self) {
        
        tempTxtFeild = txtFeild;
        
        
        UILabel *lbl_NameTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 60, txtFeild.frame.size.width - 20, 21)];
        lbl_NameTitle.textColor = [UIColor whiteColor];
        lbl_NameTitle.backgroundColor = [UIColor blackColor];
        [lbl_NameTitle setFont:[UIFont fontWithName:@"ClanPro-Book" size:12.6f]];
        lbl_NameTitle.text = [NSString stringWithFormat:@"%@",ErrorMsgStr];
        
        CGSize stringBoundingBox = [ErrorMsgStr sizeWithAttributes:
                       @{NSFontAttributeName:[UIFont fontWithName:@"ClanPro-Book" size:12.6f]}];
        
        // Values are fractional -- you should take the ceilf to get equivalent values
        CGSize adjustedSize = CGSizeMake(ceilf(stringBoundingBox.width), ceilf(stringBoundingBox.height));

        float widVal = txtFeild.frame.size.width - txtFeild.frame.size.width/3;//adjustedSize.width;
        float heigVal = 31;//adjustedSize.height;
        
        lbl_NameTitle.frame = CGRectMake(0, 60, widVal, 21);
        
        if ([self isLabelTruncated:lbl_NameTitle]) {
            widVal = txtFeild.frame.size.width - 10;
            lbl_NameTitle.frame = CGRectMake(0, 60, widVal, 21);
            
           // heigVal = [self getLabelHeight:lbl_NameTitle];
            if ([self isLabelTruncated:lbl_NameTitle]) {
                heigVal= 51;
                widVal = txtFeild.frame.size.width - 10;
            }
            else{
               heigVal= 31;
                if (lbl_NameTitle.text.length>24) {
                 widVal = txtFeild.frame.size.width - 20;
                }
                else{
                 widVal = txtFeild.frame.size.width - txtFeild.frame.size.width/4;
                }
            }
            lbl_NameTitle.numberOfLines = 3;
        }
        
        lbl_NameTitle.frame = CGRectMake(4, 57, widVal - 8, heigVal);
        
        
        
        BGviewBottom = [[UIView alloc] initWithFrame:CGRectMake(txtFeild.frame.origin.x + (txtFeild.frame.size.width - widVal), txtFeild.frame.origin.y-4, widVal, 60 + heigVal)];
        BGviewBottom.backgroundColor = [UIColor clearColor];
        //        BGviewBottom.alpha = 0.6f;
        BGviewBottom.layer.cornerRadius = 4.0f;
        BGviewBottom.clipsToBounds = YES;
        
        UILabel *badLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 57, widVal, heigVal)];
        badLabel.text = @"";
        badLabel.backgroundColor = [UIColor blackColor];
        [BGviewBottom addSubview:badLabel];
        
        
        
        UIImageView *btnImg;
        if([UIScreen mainScreen].bounds.size.height==812 ){
           btnImg = [[UIImageView alloc] initWithFrame:CGRectMake(widVal -27, 13, 20, 20)];
        }
        else{
            btnImg = [[UIImageView alloc] initWithFrame:CGRectMake(widVal -27, 8.5, 20, 20)];
            
        }
        btnImg.backgroundColor = [UIColor clearColor];
        [btnImg setImage:[UIImage imageNamed:@"abhi_error_icon.png"]];
        [BGviewBottom addSubview:btnImg];
        
        UIImageView *rowImg = [[UIImageView alloc] initWithFrame:CGRectMake(-25, 31, widVal + 30, 30)];
        rowImg.backgroundColor = [UIColor clearColor];
        [rowImg setImage:[UIImage imageNamed:@"abhi_active_bar.png"]];
        [BGviewBottom addSubview:rowImg];
        
//        UILabel *lbl_NameTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, txtFeild.frame.size.width - 20, 40)];
//        lbl_NameTitle.textColor = [UIColor whiteColor];
//        lbl_NameTitle.backgroundColor = [UIColor blackColor];
//        lbl_NameTitle.text = [NSString stringWithFormat:@"%@",ErrorMsgStr];
//        [lbl_NameTitle setFont:[UIFont fontWithName:@"Ubuntu" size:12.5f]];
        [BGviewBottom addSubview:lbl_NameTitle];
        
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(fluidFillTap:)];
        tapGesture.numberOfTapsRequired = 1;
        [BGviewBottom addGestureRecognizer:tapGesture];

        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        [UIView commitAnimations];
        
        [txtFeild.superview addSubview:BGviewBottom];
        
    }
    
    
    return self;
}
- (BOOL)isLabelTruncated:(UILabel *)label
{
    CGSize sizeOfText = [label.text boundingRectWithSize: CGSizeMake(label.bounds.size.width, CGFLOAT_MAX)
                                                 options: (NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                              attributes: [NSDictionary dictionaryWithObject:[UIFont fontWithName:@"ClanPro-Book" size:12.6f] forKey:NSFontAttributeName] context: nil].size;
    
    if (21 < ceilf(sizeOfText.height)) {
        return YES;
    }
    return NO;
}
-(void) setHidden:(BOOL)hidden
{
    if (hidden) {
        BGviewBottom.frame = CGRectMake(0, 0, 0, 0);
        [self myDelegate];
    }
}

- (void) myDelegate {
    [self.delegate AbhiTxtFeildValidationDelegateMethod:self];
}


-(void)fluidFillTap:(UIGestureRecognizer*)tapGesture{
    
//    CGPoint pt = [tapGesture locationInView:imgView_ScreenShot];
    
    [tempTxtFeild becomeFirstResponder];
}

- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:[UIFont fontWithName:@"ClanPro-Book" size:12.6f]}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}


@end
