//
//  AccSettingViewController.h
//  Uber Reward
//
//  Created by Arvind on 23/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UtillClass.h"
#import "PECropViewController.h"
#import "NIDropDown.h"
#import "SlidingViewController.h"
@interface AccSettingViewController : UIViewController<PECropViewControllerDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,NIDropDownDelegate,slideViewControllerDelegate>{
    SlidingViewController *slideViewMenu;
    BOOL cam_active;
    NSMutableArray*arr_imgflag;
    NSMutableArray*arr_cname;
    NSMutableArray*arr_cid;
    NSMutableArray*arr_mobdigit;
    NSString *strbase64;
    int  maxdigit;
    NIDropDown* dropDown;
    NSString*clang;
    NSMutableArray*arr_lang;
}
@property (weak, nonatomic) IBOutlet UILabel *lbl_cm;
@property (weak, nonatomic) IBOutlet UILabel *lbl_ce;

@property (weak, nonatomic) IBOutlet UITextField *tf_mob;

@property (weak, nonatomic) IBOutlet UIImageView *img_prof;
@property (weak, nonatomic) IBOutlet UITextField *tf_email;

@property (weak, nonatomic) IBOutlet UIButton *btn_cpsw;
@property (weak, nonatomic) IBOutlet UIButton *btn_lang;
@property (weak, nonatomic) IBOutlet UIImageView *img_cflag;
@property (weak, nonatomic) IBOutlet UILabel *lbl_code;
@end
