//
//  AccSettingViewController.m
//  Uber Reward
//
//  Created by Arvind on 23/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import "AccSettingViewController.h"
#import "UtillClass.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImage+ImageCompress.h"
#import "ChangeMobileViewController.h"
#import "ChangeEmailViewController.h"
@interface AccSettingViewController ()

@end

@implementation AccSettingViewController
- (IBAction)select_sidemenu:(id)sender {
    
    if (slideViewMenu.view) {
        [self HideSlideMenu];
    } else {
        slideViewMenu = [[UtillClass sharedInstance] CallSlideMenuView:self yVal:0.0f];
    }
}

#pragma mark - slideView Delegate

-(void) slideViewControllerDone:(SlidingViewController *)sender
{
    if (sender) {
        [self HideSlideMenu];
    }
}
-(void) HideSlideMenu
{
    [UIView animateWithDuration:0.50 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        //code with animation
        // slideViewMenu.view.frame = CGRectMake(-self.view.frame.size.width, 0.0f, self.view.frame.size.width, self.view.frame.size.height - 0.0f);
        [slideViewMenu.view removeFromSuperview];
    } completion:^(BOOL finished) {
        //code for completion
        [slideViewMenu.view removeFromSuperview];
        slideViewMenu = nil;
        
    }];
}
-(void)viewDidAppear:(BOOL)animated {
    clang =[[UtillClass sharedInstance] getcultureID];
    
    [[UtillClass sharedInstance]HideLoader];
    [self HideSlideMenu];
    
    [[UtillClass sharedInstance] showbottom_items];
    if (cam_active==YES) {
        
    }
    else{
    [self GetCountryCodes];
    }
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"ios  Account Setting"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    if ([UIScreen mainScreen].bounds.size.height==568) {
        [_lbl_cm setFrame:CGRectMake(227, 388, 62,1)];
        
        [_lbl_ce setFrame:CGRectMake(208, 307, 81,1)];
    }
    
    arr_lang = [[NSMutableArray alloc]init];
    arr_cname =[[NSMutableArray alloc]init];
    arr_imgflag = [[NSMutableArray alloc]init];
    
    arr_cid =[[NSMutableArray alloc]init];
    arr_mobdigit = [[NSMutableArray alloc]init];
    if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
        [arr_lang addObject:@"English"];
        [arr_lang addObject:@"Arabic"];
    }
    else{
        [arr_lang addObject:@"الإنجليزية"];
        [arr_lang addObject:@"العربية"];
        
    }
    
    if([UIScreen mainScreen].bounds.size.height==812 ){
        [_img_prof setFrame:CGRectMake(_img_prof.frame.origin.x, _img_prof.frame.origin.y, 127, 127)];
    
        
    }
    
    _img_prof.layer.cornerRadius= _img_prof.frame.size.width/2;
    _img_prof.clipsToBounds=YES;
    _tf_email.layer.borderWidth= 0.6;
    _tf_email.layer.borderColor=[UIColor colorWithRed:192/255.0f green:192/255.0f blue:192/255.0f alpha:1].CGColor;
    _tf_email.layer.cornerRadius=1;
    
    
    
    _btn_cpsw.layer.borderWidth= 0.6;
    _btn_cpsw.layer.borderColor=[UIColor colorWithRed:192/255.0f green:192/255.0f blue:192/255.0f alpha:1].CGColor;
    _btn_cpsw.layer.cornerRadius=1;
    
    
    _btn_lang.layer.borderWidth= 0.6;
    _btn_lang.layer.borderColor=[UIColor colorWithRed:192/255.0f green:192/255.0f blue:192/255.0f alpha:1].CGColor;
    _btn_lang.layer.cornerRadius=1;
    
}

    


-(void)get_profinfo
{
    [self.view endEditing:YES];
    
    if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        };
        
    }
    else{
        [[UtillClass sharedInstance] ShowLoaderOn:self.parentViewController withtext:@"Please wait"];
        
      
       
        [[UtillClass sharedInstance] getresponsefromservice:[NSString stringWithFormat:@"Account/UserInfo?altId=0&CultureId=%@&CustomerId=%@",[[UtillClass sharedInstance] getcultureID],[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"Login Details"]objectForKey:@"CustomerId"]] withbearer:[[UtillClass sharedInstance] getapp_sessiontoken] response:^(NSDictionary * dict_response) {
            
            [[UtillClass sharedInstance] HideLoader];
            if (dict_response ==nil) {
                [[UtillClass sharedInstance] showerrortoastmsg:@"Server error, Please try after sometime."];
                
            }
            else{
                if ([[dict_response objectForKey:@"Status"] integerValue] == 1) {
                    
                    [[UtillClass sharedInstance] setUserdefaultofDict:dict_response withKayname:@"UserInfo"];
                    _tf_email.text= [dict_response objectForKey:@"Email"];
                    _tf_mob.text= [dict_response objectForKey:@"Mobile"];
                    if ([[dict_response objectForKey:@"UserImage"] isKindOfClass:[NSNull class]] || [[dict_response objectForKey:@"UserImage"] isEqualToString:@""] ) {
                        [_img_prof sd_setImageWithURL:[NSURL URLWithString:@""]
                                     placeholderImage:[UIImage imageNamed:@"Pro.png"]];
                    }else{
                        
                    [_img_prof sd_setImageWithURL:[NSURL URLWithString:[dict_response objectForKey:@"UserImage"]]
                                      placeholderImage:[UIImage imageNamed:@"Pro.png"]];
                        
                    }
                    _lbl_code.text =[NSString stringWithFormat:@"+%@",[dict_response objectForKey:@"CountryId"]];
                    
                    @try{
                    [_img_cflag sd_setImageWithURL:[NSURL URLWithString:[arr_imgflag objectAtIndex:[arr_cid indexOfObject:[dict_response objectForKey:@"CountryId"]]]]
                                 placeholderImage:[UIImage imageNamed:@"kent_default.png"]];
                    }
                    @catch(NSException *e){
                        
                    }
                    @finally{
                        
                        [_img_cflag sd_setImageWithURL:[NSURL URLWithString:[arr_imgflag objectAtIndex:0]]
                                      placeholderImage:[UIImage imageNamed:@"kent_default.png"]];
                    }
                    
                }
                else{
                    [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
                }
            }
        }];
    }
    
    
}
-(void)GetCountryCodes{
    [self.view endEditing:YES];
    if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        };
        
    }
    else{
        [[UtillClass sharedInstance] ShowLoaderOn:self.parentViewController withtext:@"Please wait"];
        
        NSMutableDictionary*dicts = [[NSMutableDictionary alloc]init];
        [dicts setValue:[[UtillClass sharedInstance] getcultureID] forKey:@"CultureId"];
        
        
        [[UtillClass sharedInstance] getresponsefromservice:[NSString stringWithFormat:@"Account/GetCountry?CultureId=%@",[[UtillClass sharedInstance] getcultureID]] withbearer:@"" response:^(NSDictionary * dict_response) {
            [[UtillClass sharedInstance] HideLoader];
            if (dict_response ==nil) {
                [[UtillClass sharedInstance] showerrortoastmsg:@"Server error, Please try after sometime."];
                
            }
            else{
                if ([[dict_response objectForKey:@"Status"] integerValue] == 1) {
                    [[UtillClass sharedInstance] setUserdefaultofDict:dict_response withKayname:@"CountryDetail"];
                    NSArray *Arr = [dict_response objectForKey:@"ListData"];
                    for (int i=0; i<[Arr count]; i++) {
                        
                        [arr_mobdigit addObject:[[Arr objectAtIndex:i]objectForKey:@"MobileDigit"]];
                        [arr_cname addObject:[[Arr objectAtIndex:i]objectForKey:@"CountryName"]];
                        [arr_cid addObject:[[Arr objectAtIndex:i]objectForKey:@"CountryID"]];
                        [arr_imgflag addObject:[[Arr objectAtIndex:i]objectForKey:@"CountryImage"]];
                        maxdigit = [[arr_mobdigit objectAtIndex:0]intValue];
                    }
                    
                 [self get_profinfo];
                    
                    
                    
                }
                else{
                    [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
                }
            }
        }];
        
    }
    /*
     UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"RegEmailViewController"];
     [self presentViewController:vc animated:false completion:nil];
     */
}
-(IBAction)changeemail:(id)sender{
    ChangeEmailViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"ChangeEmailViewController"];
   
    vc.stremail= _tf_email.text;
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"ios  Account Setting"];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Change Email"
                                                          action:@"button_press"
                                                           label:[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"Login Details"]objectForKey:@"CustomerId"]
                                                           value:nil] build]];

    [self presentViewController:vc animated:false completion:nil];
    
}
-(IBAction)changemob:(id)sender{
    
    ChangeMobileViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"ChangeMobileViewController"];
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"ios  Account Setting"];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Change mob"
                                                          action:@"button_press"
                                                           label:[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"Login Details"]objectForKey:@"CustomerId"]
                                                           value:nil] build]];

    vc.str_ccde = [_lbl_code.text stringByReplacingOccurrencesOfString:@"+" withString:@""];
    vc.str_premob= _tf_mob.text;
    vc.maxdigit =maxdigit;
    [self presentViewController:vc animated:false completion:nil];
    
}

-(IBAction)camera:(UIButton *)sender{
    cam_active = YES;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController *pickers = [[UIImagePickerController alloc] init];
        pickers.delegate = self;
        [pickers setSourceType:UIImagePickerControllerSourceTypeCamera];
        pickers.allowsEditing = YES;
        
        [self presentViewController:pickers animated:true completion:nil];
}
  
}
-(IBAction)backregtowel:(id)sender{
    
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
    [self presentViewController:vc animated:false completion:nil];
}
-(IBAction)gallery:(UIButton *)sender{
    cam_active = YES;
      if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
                                            UIImagePickerController *pickerss = [[UIImagePickerController alloc] init];
                                            pickerss.delegate = self;
                                            [pickerss setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
                                            pickerss.allowsEditing = true;
                                            [self presentViewController:pickerss animated:true completion:nil];
                                            
                                        }
}
-(IBAction)changepsw:(id)sender{
    
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"ChangePswViewController"];
    [self presentViewController:vc animated:false completion:nil];
}

#pragma mark -=-=-=-=- Delegate of image picker and croper -=-=-
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    PECropViewController *controller = [[PECropViewController alloc] init];
    controller.delegate = self;
    controller.image = image;
    [controller setCropRect:CGRectMake(0, 200, [UIScreen mainScreen].bounds.size.width, 350)];
    [self dismissViewControllerAnimated:false completion:nil];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
    [self presentViewController:navigationController animated:YES completion:NULL];
    
    
}
-(void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    cam_active=YES;
    [self dismissViewControllerAnimated:false completion:nil];
}
- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
    
    //    self.imageView.image = croppedImage;
    
    UIImage* croppedImagecomp =[UIImage compressImage:croppedImage
                                        compressRatio:0.2f];
    NSData *myImageData = UIImageJPEGRepresentation(croppedImagecomp, 0.2f);
    
    UIImage *myImage = [UIImage imageWithData:myImageData];
    
    
    _img_prof.image= myImage;
    
    strbase64 =[self encodeToBase64String:myImage];
     cam_active=YES;
    
}

- (void) niDropDownDelegateMethod: (NSString *)btntext withbtntag:(NSString *)btntag dropdown: (NIDropDown *) sender {
    dropDown = nil;
    
    [_btn_lang setTitle:[NSString stringWithFormat:@"%@", [[btntext componentsSeparatedByString:@","]objectAtIndex:0]] forState:UIControlStateNormal];
    
    
    
    
}

#pragma mark - DropDown Delegate
- (void)OpenDropDown:(UIButton *)sender Array:(NSArray *) arr
{
    [self.view endEditing:YES];
    //    arr = [[NSArray alloc] init];
    NSArray * arrImage = [[NSArray alloc] init];
    
   if(dropDown == nil) {
        CGFloat f = 200;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :arrImage :@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        dropDown = nil;
    }
}

- (NSString *)encodeToBase64String:(UIImage *)image {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)action_select_ac_sett_lang:(id)sender {
    [self OpenDropDown:sender Array:[arr_lang copy]];
}
- (IBAction)sekect_updateprofile:(id)sender {
    
  [self.view endEditing:YES];
    
        if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
            if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        };
            
        }
        else{
            [[UtillClass sharedInstance] ShowLoaderOn:self.parentViewController withtext:@"Please wait"];
            
            NSDictionary*dictdata= [[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"UserInfo"];
    
     
            NSMutableDictionary*dicts = [[NSMutableDictionary alloc]init];
          
            [dicts setValue:@"" forKey:@"TitleId"];
            [dicts setValue:[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"Login Details"]objectForKey:@"CustomerId"] forKey:@"customerId"];
            [dicts setValue:[dictdata objectForKey:@"FirstName"] forKey:@"FirstName"];
            [dicts setValue:[dictdata objectForKey:@"LastName"] forKey:@"LastName"];
            [dicts setValue:[dictdata objectForKey:@"Email"] forKey:@"Email"];
            [dicts setValue:[dictdata objectForKey:@"CountryCodeIdId"] forKey:@"CountryCodeIdId"];
            [dicts setValue:[dictdata objectForKey:@"Mobile"] forKey:@"Mobile"];
            [dicts setValue:[dictdata objectForKey:@"DOB"] forKey:@"DOB"];
            [dicts setValue:[dictdata objectForKey:@"CultureId"] forKey:@"CultureId"];
            [dicts setValue:[dictdata objectForKey:@"CountryId"] forKey:@"CountryId"];
            [dicts setValue:@"" forKey:@"SQId"];
            [dicts setValue:@"" forKey:@"SecurityAnswer"];
            [dicts setValue:[dictdata objectForKey:@"NPassword"] forKey:@"NPassword"];
            [dicts setValue:@"" forKey:@"City"];
            [dicts setValue:@"0" forKey:@"IsCodeRegBlock"];
            
            if(strbase64 ==nil){
               [dicts setValue:@"" forKey:@"UserImage"];
            }else{
             [dicts setValue:strbase64 forKey:@"UserImage"];
            }
            
            
            [[UtillClass sharedInstance] Postresponsetoservice:dicts withBearer:[[UtillClass sharedInstance] getapp_sessiontoken] withurl:[NSString stringWithFormat:@"Account/UpdateProfile?CultureId=%@",[[UtillClass sharedInstance] getcultureID]] response:^(NSDictionary * dict_response) {
                [[UtillClass sharedInstance] HideLoader];
                
                if (dict_response ==nil) {
                    [[UtillClass sharedInstance] showerrortoastmsg:@"Server error, Please try after sometime."];
                    
                }
                else{
                    if ([[dict_response objectForKey:@"Status"] integerValue] == 1) {
                        
                        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                            if ([_btn_lang.titleLabel.text isEqualToString:@"English"]) {
                                [[UtillClass sharedInstance] set_app_language:@"English"];
                                
                            }
                            else if([_btn_lang.titleLabel.text isEqualToString:@"Arabic"]) {
                                [[UtillClass sharedInstance] set_app_language:@"Arabic"];
                                
                                
                            }
                            else{
                                
                            }
                        }
                        else{
                            
                          if ([_btn_lang.titleLabel.text isEqualToString:@"الإنجليزية"]) {
                                [[UtillClass sharedInstance] set_app_language:@"English"];
                                
                            }
                            else if([_btn_lang.titleLabel.text isEqualToString:@"العربية"]) {
                                [[UtillClass sharedInstance] set_app_language:@"Arabic"];
                                
                                
                            }
                            else{
                                
                            }
                        }
                        [[UtillClass sharedInstance] showtoastmsg:[dict_response objectForKey:@"msg"]];
                        if ([clang isEqualToString:[[UtillClass sharedInstance] getcultureID]]) {
                            UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"AccSettingViewController"];
                            [self presentViewController:vc animated:false completion:nil];
                        }else{
                            [[SDImageCache sharedImageCache]clearMemory];
                        
                        UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
                        [self presentViewController:vc animated:false completion:nil];
                    }
                    }
                    else{
                        [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
                    }
                }
            }];
        }
}
    


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
