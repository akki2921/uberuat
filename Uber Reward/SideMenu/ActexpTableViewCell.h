//
//  ActexpTableViewCell.h
//  Uber Reward
//
//  Created by Arvind Seth on 17/03/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActexpTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lbl_pointexpire;

@property (strong, nonatomic) IBOutlet UILabel *lbl_day;
@end
