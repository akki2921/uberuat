//
//  ActivityViewController.h
//  Uber Reward
//
//  Created by Arvind on 23/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlidingViewController.h"
#import "UtillClass.h"
@interface ActivityViewController : UIViewController<slideViewControllerDelegate>{
    
    NSMutableArray *arr_day;
     NSMutableArray *arr_points;
    SlidingViewController *slideViewMenu;
}
@property (strong, nonatomic) IBOutlet UIImageView * img_badge;
@property (strong, nonatomic) IBOutlet UILabel *lbl_cpb;
@property (strong, nonatomic) IBOutlet UILabel *lbl_ped;
@property (strong, nonatomic) IBOutlet UILabel *lbl_prd;
@property (strong, nonatomic) IBOutlet UILabel *lbl_pexd;
@property (strong, nonatomic) IBOutlet UITableView *tbl_activity;


- (IBAction)Action_ViewActivity:(id)sender;

@end
