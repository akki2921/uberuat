//
//  ActivityViewController.m
//  Uber Reward
//
//  Created by Arvind on 23/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import "ActivityViewController.h"
#import "ActexpTableViewCell.h"
@interface ActivityViewController ()

@end

@implementation ActivityViewController
-(IBAction)backregtowel:(id)sender{
    
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
    [self presentViewController:vc animated:false completion:nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    if([UIScreen mainScreen].bounds.size.height==812 ){
       
        [_img_badge setFrame:CGRectMake(_img_badge.frame.origin.x, _img_badge.frame.origin.y, 33, 42)];
        
    }
    
    arr_day=[[NSMutableArray alloc]init];
    arr_points=[[NSMutableArray alloc]init];
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"partnertype"] isEqualToString:@"1"]) {
         _img_badge.image = [UIImage imageNamed:@"gold.png"];
    }
    else if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"partnertype"] isEqualToString:@"2"]) {
      _img_badge.image = [UIImage imageNamed:@"silver.png"];
    }
    else if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"partnertype"] isEqualToString:@"3"]) {
      _img_badge.image = [UIImage imageNamed:@"bronze.png"];
    }
    else{
        _img_badge.image = [UIImage imageNamed:@"platinum.png"];
    }
    
  
    // Do any additional setup after loading the view.
}
-(IBAction)viewActivity:(id)sender{
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"ViewActivityViewController"];
    [self presentViewController:vc animated:false completion:nil];
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arr_day count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"ActexpTableViewCell";
    
    ActexpTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier forIndexPath: indexPath];
    
    cell.lbl_day.text= [arr_day objectAtIndex:indexPath.row];
    cell.lbl_pointexpire.text= [NSString stringWithFormat:@"%@",[[UtillClass sharedInstance] getpointformatofint:[[arr_points objectAtIndex:indexPath.row] intValue]]];
    
    return cell;
    
}
- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section
{
    //Set the background color of the View
    view.tintColor = [UIColor clearColor];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 38
    
    ;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
}
-(void)viewDidAppear:(BOOL)animated {
    [[UtillClass sharedInstance]HideLoader];
    [self HideSlideMenu];
    [[UtillClass sharedInstance] showbottom_items];
    
    
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"ios  View Activity"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [self Getactivitydata];
}
-(void)Getactivitydata{
    
    [self.view endEditing:YES];
    if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        };
        
    }
    else{
        [[UtillClass sharedInstance] ShowLoaderOn:self.parentViewController withtext:@"Please wait"];
        
        [[UtillClass sharedInstance] getresponsefromservice:[NSString stringWithFormat:@"Statement/getPointStatementMobile?CultureId=%@&CustomerId=%@&altId=0",[[UtillClass sharedInstance] getcultureID],[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"Login Details"]objectForKey:@"CustomerId"]] withbearer:[[UtillClass sharedInstance] getapp_sessiontoken] response:^(NSDictionary * dict_response) {
            [[UtillClass sharedInstance] HideLoader];
            if (dict_response ==nil) {
                [[UtillClass sharedInstance] showerrortoastmsg:@"Server error, Please try after sometime."];
            }
            else{
                if ([[dict_response objectForKey:@"Status"] integerValue] == 1) {
                    
                    
                    _lbl_cpb.text= [[UtillClass sharedInstance] getpointformatofint:[[dict_response objectForKey:@"PointBalance"]intValue]];
                    _lbl_ped.text= [[UtillClass sharedInstance] getpointformatofint:[[dict_response objectForKey:@"TotalEarned"]intValue]];
                    _lbl_prd.text=[[UtillClass sharedInstance] getpointformatofint:[[dict_response objectForKey:@"TotalRedeem"]intValue]];
                    _lbl_pexd.text=[[UtillClass sharedInstance] getpointformatofint:[[dict_response objectForKey:@"TotalExpired"]intValue]];
                    
                    NSMutableArray *TempArray = [[NSMutableArray alloc] init];
                    TempArray = [[dict_response objectForKey:@"lstMonths"] mutableCopy];
                    arr_day = [[NSMutableArray alloc] init];
                    arr_points = [[NSMutableArray alloc] init];
                    
                    if (TempArray.count) {
                        for (int i = 0; i < TempArray.count; i++) {
                            
                            if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                                
                            
                            [arr_day addObject:[[TempArray objectAtIndex:i] valueForKey:@"Key"]];
                            [arr_points addObject:[[TempArray objectAtIndex:i] valueForKey:@"Value"]];
                            }
                            else{
                                
                                NSString *strdat =[[[[TempArray objectAtIndex:i] valueForKey:@"Key"] componentsSeparatedByString:@" "] objectAtIndex:1];
                                
                                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                formatter.dateFormat = @"yyyy";
                                
                                NSDate *date = [formatter dateFromString:strdat];
                                
                                
                                
                                NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"ar"];
                                [formatter setLocale:usLocale];
                                NSString *dateString = [formatter stringFromDate:date];
                                NSString*dateneedtoadd= [NSString stringWithFormat:@"%@ %@",[[[[TempArray objectAtIndex:i] valueForKey:@"Key"] componentsSeparatedByString:@" "] objectAtIndex:0],dateString];
                                [arr_day addObject:dateneedtoadd];
                                [arr_points addObject:[[TempArray objectAtIndex:i] valueForKey:@"Value"]];
                            }
                        }
                    }
                    
                    [_tbl_activity reloadData];
                }
                else{
                    [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
                }
            }
        }];
    }
    
    
    /*
     UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"RegEmailViewController"];
     [self presentViewController:vc animated:false completion:nil];
     */
}
- (IBAction)select_sidemenu:(id)sender {
    
    if (slideViewMenu.view) {
        [self HideSlideMenu];
    } else {
        slideViewMenu = [[UtillClass sharedInstance] CallSlideMenuView:self yVal:0.0f];
    }
}

#pragma mark - slideView Delegate

-(void) slideViewControllerDone:(SlidingViewController *)sender
{
    if (sender) {
        [self HideSlideMenu];
    }
}
-(void) HideSlideMenu
{
    [UIView animateWithDuration:0.50 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        //code with animation
        // slideViewMenu.view.frame = CGRectMake(-self.view.frame.size.width, 0.0f, self.view.frame.size.width, self.view.frame.size.height - 0.0f);
        [slideViewMenu.view removeFromSuperview];
    } completion:^(BOOL finished) {
        //code for completion
        [slideViewMenu.view removeFromSuperview];
        slideViewMenu = nil;
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)Action_ViewActivity:(id)sender {
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"ViewActivityViewController"];
    [self presentViewController:vc animated:false completion:nil];
    
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"ios  View Activity"];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"View Activity"
                                                          action:@"button_press"
                                                           label:[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"Login Details"]objectForKey:@"CustomerId"]
                                                           value:nil] build]];
}
@end
