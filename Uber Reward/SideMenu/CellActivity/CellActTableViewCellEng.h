//
//  CellActTableViewCellEng.h
//  Uber Reward
//
//  Created by Arvind on 26/03/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellActTableViewCellEng : UITableViewCell

@property (strong, nonatomic) IBOutlet UIView *view_BG;
@property (strong, nonatomic) IBOutlet UILabel *lbl_ActivityDate;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Points;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Balance;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Voucher;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Type;

@property (strong, nonatomic) IBOutlet UIButton *btn_Voucher;
@end
