//
//  ChangeEmailViewController.h
//  Uber Reward
//
//  Created by Arvind Seth on 24/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlidingViewController.h"
@interface ChangeEmailViewController : UIViewController<slideViewControllerDelegate>{
    SlidingViewController *slideViewMenu;
    
}
@property (strong, nonatomic) NSString *stremail;
@property (weak, nonatomic) IBOutlet UIImageView *img_check1;
@property (weak, nonatomic) IBOutlet UIImageView *img_check2;
@property (weak, nonatomic) IBOutlet UIImageView *img_check3;
@property (strong, nonatomic) IBOutlet UITextField *tf_oldemail;
@property (strong, nonatomic) IBOutlet UITextField *tf_newemail;
@property (strong, nonatomic) IBOutlet UITextField *tf_confemail;

@end
