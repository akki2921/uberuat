//
//  ChangeEmailViewController.m
//  Uber Reward
//
//  Created by Arvind Seth on 24/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import "ChangeEmailViewController.h"
#import "BSErrorMessageView.h"
#import "UITextField+BSErrorMessageView.h"

@interface ChangeEmailViewController ()

@end

@implementation ChangeEmailViewController
@synthesize stremail;
-(void)viewDidAppear:(BOOL)animated {
    [[UtillClass sharedInstance]HideLoader];
    [self HideSlideMenu];
    [[UtillClass sharedInstance] showbottom_items];
}
-(IBAction)changeemailback:(id)sender{
    
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"AccSettingViewController"];
    [self presentViewController:vc animated:false completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
     [_tf_oldemail setFont:[UIFont fontWithName:@"ClanPro-Book" size:12.6]];
     [_tf_newemail setFont:[UIFont fontWithName:@"ClanPro-Book" size:12.6]];
     [_tf_confemail setFont:[UIFont fontWithName:@"ClanPro-Book" size:12.6]];
    
    
    if([UIScreen mainScreen].bounds.size.height==812 ){
        
        [_img_check1 setFrame:CGRectMake(_img_check1.frame.origin.x, _img_check1.frame.origin.y+2, 20, 20)];
        [_img_check2 setFrame:CGRectMake(_img_check2.frame.origin.x, _img_check2.frame.origin.y+2, 20, 20)];
        [_img_check3 setFrame:CGRectMake(_img_check3.frame.origin.x, _img_check3.frame.origin.y+2, 20, 20)];

        
        
    }
    
    _tf_oldemail.text=stremail;
    _tf_oldemail.layer.borderWidth= 0.6;
    _tf_oldemail.layer.borderColor=[UIColor colorWithRed:192/255.0f green:192/255.0f blue:192/255.0f alpha:1].CGColor;
    _tf_oldemail.layer.cornerRadius=1;
    
    _tf_newemail.layer.borderWidth= 0.6;
    _tf_newemail.layer.borderColor=[UIColor colorWithRed:192/255.0f green:192/255.0f blue:192/255.0f alpha:1].CGColor;
    _tf_newemail.layer.cornerRadius=1;
    
    
    _tf_confemail.layer.borderWidth= 0.6;
    _tf_confemail.layer.borderColor=[UIColor colorWithRed:192/255.0f green:192/255.0f blue:192/255.0f alpha:1].CGColor;
    _tf_confemail.layer.cornerRadius=1;
    _tf_oldemail.text= stremail;
    
    _tf_oldemail.userInteractionEnabled=NO;
    // Do any additional setup after loading the view.
}
- (IBAction)select_submit:(id)sender {
    [self.view endEditing:YES];
    
    NSDictionary*dictdata= [[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"UserInfo"];


 if (!_tf_newemail.text.length) {
         _img_check2.hidden=YES;

     
     if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
         [[UtillClass sharedInstance] ShowTfErrorMsg:@"Please provide valid email address. E.g. example@example.com" onTf:_tf_newemail];
     }
     else{
         [[UtillClass sharedInstance] ShowTfErrorMsg:@"يرجى إدخال بريد إلكتروني صحيح. مثلاً example@example.com" onTf:_tf_newemail];
         
     }
     
    }
    else if (![[UtillClass sharedInstance] validateEmailWithString:_tf_newemail.text]) {
        _img_check2.hidden=YES;
  
        
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"Please provide valid email address. E.g. example@example.com" onTf:_tf_newemail];
        }
        else{
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"يرجى إدخال بريد إلكتروني صحيح. مثلاً example@example.com" onTf:_tf_newemail];
        }
        
    }
   else if (!_tf_confemail.text.length) {
        _img_check3.hidden=YES;
 
      
       if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
           [[UtillClass sharedInstance] ShowTfErrorMsg:@"Please provide valid email address. E.g. example@example.com" onTf:_tf_confemail];
       }
       else{
           [[UtillClass sharedInstance] ShowTfErrorMsg:@"يرجى إدخال بريد إلكتروني صحيح. مثلاً example@example.com" onTf:_tf_confemail];
       }
    }
    else if (![[UtillClass sharedInstance] validateEmailWithString:_tf_confemail.text]) {
         _img_check3.hidden=YES;

        
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"Please provide valid email address. E.g. example@example.com" onTf:_tf_confemail];
        }
        else{
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"يرجى إدخال بريد إلكتروني صحيح. مثلاً example@example.com" onTf:_tf_confemail];
        }
    }
   
       else if ([_tf_confemail.text caseInsensitiveCompare:_tf_newemail.text] != NSOrderedSame) {
        
        _img_check3.hidden=YES;

           
           if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
               [[UtillClass sharedInstance] ShowTfErrorMsg:@"New email address and Confirm email address do not match. Please re-enter your email address" onTf:_tf_confemail];
           }
           else{
               [[UtillClass sharedInstance] ShowTfErrorMsg:@"البريد الإلكتروني الجديد وتأكيد بريد الإلكتروني غير متطابقين. يرجى إعادة إدخال البريد الإلكتروني" onTf:_tf_confemail];
           }
    }
    else{
        if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
            if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        };
            
        }
        else{
            [[UtillClass sharedInstance] ShowLoaderOn:self.parentViewController withtext:@"Please wait"];
            
            NSMutableDictionary*dicts = [[NSMutableDictionary alloc]init];
            [dicts setValue:[[UtillClass sharedInstance] getcultureID] forKey:@"CultureId"];
            [dicts setValue:[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"Login Details"]objectForKey:@"CustomerId"] forKey:@"CustomerId"];
            [dicts setValue:_tf_newemail.text forKey:@"NewEmailID"];
            [dicts setValue:_tf_confemail.text forKey:@"ConfirmEmailID"];
            [dicts setValue:_tf_oldemail.text forKey:@"EmailID"];
            
           
            [[UtillClass sharedInstance] Postresponsetoservice:dicts withBearer:[[UtillClass sharedInstance] getapp_sessiontoken] withurl:[NSString stringWithFormat:@"Account/resetEmailId?"] response:^(NSDictionary * dict_response) {
                [[UtillClass sharedInstance] HideLoader];
                if (dict_response ==nil) {
                    [[UtillClass sharedInstance] showerrortoastmsg:@"Server error, Please try after sometime."];
                    
                }
                else{
                    if ([[dict_response objectForKey:@"Status"] integerValue] == 1) {
                         if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                        [[UtillClass sharedInstance] showtoastmsg:@"Your email address has been reset successfully."];
                         }else{
                             [[UtillClass sharedInstance] showtoastmsg:@"تمّ تغيير عنوان بريدك الإلكتروني بنجاح"];
                         }
                         [self performSelector:@selector(callvce) withObject:nil afterDelay:1.8];
                    }
                    else{
                        
                        [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
                    }
                }
            }];
            
        }
    }
    
}
-(void)callvce{
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"AccSettingViewController"];
    [self presentViewController:vc animated:false completion:nil];
}
- (IBAction)select_sidemenu:(id)sender {
    
    if (slideViewMenu.view) {
        
        [self HideSlideMenu];
    } else {
        slideViewMenu = [[UtillClass sharedInstance] CallSlideMenuView:self yVal:0.0f];
    }
}

#pragma mark - slideView Delegate

-(void) slideViewControllerDone:(SlidingViewController *)sender
{
    if (sender) {
        [self HideSlideMenu];
    }
}
-(void) HideSlideMenu
{
    [UIView animateWithDuration:0.50 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        //code with animation
        // slideViewMenu.view.frame = CGRectMake(-self.view.frame.size.width, 0.0f, self.view.frame.size.width, self.view.frame.size.height - 0.0f);
        [slideViewMenu.view removeFromSuperview];
    } completion:^(BOOL finished) {
        //code for completion
        [slideViewMenu.view removeFromSuperview];
        slideViewMenu = nil;
        
    }];
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    if (textField==_tf_oldemail) {
        if ([[UtillClass sharedInstance] validateEmailWithString:_tf_oldemail.text]) {
            _img_check1.hidden=NO;
        }
        else{
            _img_check1.hidden=YES;
            
        }
    }
    if (textField==_tf_newemail) {
        if ([[UtillClass sharedInstance] validateEmailWithString:_tf_newemail.text]) {
            _img_check2.hidden=NO;
            
        }
        else{
            _img_check2.hidden=YES;
            
        }
    }
    if (textField==_tf_confemail) {
        if ([[UtillClass sharedInstance] validateEmailWithString:_tf_confemail.text]) {
            _img_check3.hidden=NO;
        }
        else{
            _img_check3.hidden=YES;
        
        }
    }
    [textField resignFirstResponder];
    return YES;
}
#pragma mark - UITextFieldDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (textField==_tf_oldemail) {
        if ([[UtillClass sharedInstance] validateEmailWithString:_tf_oldemail.text]) {
            _img_check1.hidden=NO;
        }
        else{
            _img_check1.hidden=YES;
            
        }
    }
    if (textField==_tf_newemail) {
        if ([[UtillClass sharedInstance] validateEmailWithString:_tf_newemail.text]) {
            _img_check2.hidden=NO;
            
        }
        else{
            _img_check2.hidden=YES;
            
        }
    }
    if (textField==_tf_confemail) {
        if ([[UtillClass sharedInstance] validateEmailWithString:_tf_confemail.text]) {
            _img_check3.hidden=NO;
        }
        else{
            _img_check3.hidden=YES;
            
        }
    }
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    [textField layoutIfNeeded];
    [[UtillClass sharedInstance] HideTfErrorMsg];
    if (_tf_oldemail.text.length) {
        if ([[UtillClass sharedInstance] validateEmailWithString:_tf_oldemail.text]) {
            _img_check1.hidden=NO;
            
        }
        else{
            _img_check1.hidden=YES;
            
            
        }
        
    }
    if (_tf_newemail.text.length) {
        if ([[UtillClass sharedInstance] validateEmailWithString:_tf_newemail.text]) {
            _img_check2.hidden=NO;
            
        }
        else{
            _img_check2.hidden=YES;
            
            
        }
        
    }
     if(_tf_confemail.text.length) {
        if ([[UtillClass sharedInstance] validateEmailWithString:_tf_confemail.text]) {
            _img_check3.hidden=NO;
            
        }
        else{
            _img_check3.hidden=YES;
            
            
        }
        
    }

    
    if (textField==_tf_oldemail) {
        _img_check1.hidden=YES;
    }
   else if (textField==_tf_newemail) {
        _img_check2.hidden=YES;
    }
    else if (textField==_tf_confemail) {
        _img_check3.hidden=YES;
    }
    
    
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
