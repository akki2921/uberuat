//
//  ChangeMobileViewController.m
//  Uber Reward
//
//  Created by Arvind Seth on 09/03/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import "ChangeMobileViewController.h"
#import "BSErrorMessageView.h"
#import "UITextField+BSErrorMessageView.h"

@interface ChangeMobileViewController ()

@end

@implementation ChangeMobileViewController
@synthesize str_ccde,str_premob,maxdigit;
- (IBAction)btn_if_timer_on:(id)sender {
    
    
}


-(IBAction)changemobback:(id)sender{
    
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"AccSettingViewController"];
    [self presentViewController:vc animated:false completion:nil];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (range.length > 0)
    {
        // We're deleting
        
        
        if (textField.tag==6 && textField.text.length==1) {
            _tf6.text=@"";
            [_tf5 becomeFirstResponder];
        }
        else if (textField.tag==5 && textField.text.length==1) {
            _tf5.text=@"";
            [_tf4 becomeFirstResponder];
        }
        else if (textField.tag==4 && textField.text.length==1) {
            _tf4.text=@"";
            [_tf3 becomeFirstResponder];
        }
        else if (textField.tag==3 && textField.text.length==1) {
            _tf3.text=@"";
            [_tf2 becomeFirstResponder];
        }
        else if (textField.tag==2 && textField.text.length==1) {
            _tf2.text=@"";
            [_tf1 becomeFirstResponder];
        }
        else if (textField.tag==1 && textField.text.length==1) {
            
            if (textField.text.length==1) {
                _tf1.text=@"";
                return NO;
            }
        }
        else{
            if (textField.text.length==1) {
                return YES;
            }
            else{
                return YES;
            }
            
            
        }
        
        
        // return YES;
    }
    NSString *numberRegEx = @"[0-9]";
    NSPredicate *testRegex = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegEx];
    
    if(![testRegex evaluateWithObject:string]){
        return NO;
    }
    if (textField.tag==18) {
        if (textField.text.length >= maxdigit) {
            return NO;
        }
        NSString *str;
        if (!textField.text.length) {
            str= [textField.text substringToIndex:0];
        }
        else{
            str= [textField.text substringToIndex:1];
        }
        
        if ([str isEqualToString:@""] && [string isEqualToString:@"0"] ) {
            return NO;
        }
    }
    else{
        if (textField.tag==1 && textField.text.length==1) {
            
            [_tf2 becomeFirstResponder];
            if (_tf2.text.length==1) {
                return NO;
            }
        }
        
        else if (textField.tag==2 && textField.text.length==1) {
            [_tf3 becomeFirstResponder];
            if (_tf3.text.length==1) {
                return NO;
            }
        }
        else if (textField.tag==3 && textField.text.length==1) {
            [_tf4 becomeFirstResponder];
            if (_tf4.text.length==1) {
                return NO;
            }
        }
        else if (textField.tag==4 && textField.text.length==1) {
            [_tf5 becomeFirstResponder];
            if (_tf5.text.length==1) {
                return NO;
            }
        }
        else if (textField.tag==5 && textField.text.length==1) {
            [_tf6 becomeFirstResponder];
            if (_tf6.text.length==1) {
                return NO;
            }
        }
        else if (textField.tag==6 && textField.text.length==1) {
            
            if (textField.text.length==1) {
                return NO;
            }
        }
        else{
            if (textField.text.length>=1) {
                return NO;
            }
            else{
                return YES;
            }
            
            
        }
        if (textField.text.length>1) {
            return NO;
        }
    }
    
    
    return YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
     count=0;
    _tf_previous_mob.text =str_premob;
    
    [_tf_mob setFont:[UIFont fontWithName:@"ClanPro-Book" size:12.6]];
    CGRect contentRect = CGRectZero;
    
    for (UIView *view in _scr_emailpage.subviews) {
        contentRect = CGRectUnion(contentRect, view.frame);
    }
    _scr_emailpage.contentSize = CGSizeMake(0, self.view.frame.size.height+120) ;
    
    _tf_previous_mob.layer.borderColor=[UIColor colorWithRed:192/255.0f green:192/255.0f blue:192/255.0f alpha:1].CGColor;
    _tf_previous_mob.layer.borderWidth= 1;
    _tf_previous_mob.layer.cornerRadius=1;

    _tf_mob.layer.borderColor=[UIColor colorWithRed:192/255.0f green:192/255.0f blue:192/255.0f alpha:1].CGColor;
    _tf_mob.layer.borderWidth= 1;
    _tf_mob.layer.cornerRadius=1;

    

    // Do any additional setup after loading the view.
}
- (IBAction)select_sidemenu:(id)sender {
    
    if (slideViewMenu.view) {
        
        [self HideSlideMenu];
    } else {
        slideViewMenu = [[UtillClass sharedInstance] CallSlideMenuView:self yVal:0.0f];
    }
}

#pragma mark - slideView Delegate

-(void) slideViewControllerDone:(SlidingViewController *)sender
{
    if (sender) {
        [self HideSlideMenu];
    }
}
-(void) HideSlideMenu
{
    [UIView animateWithDuration:0.50 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        //code with animation
        // slideViewMenu.view.frame = CGRectMake(-self.view.frame.size.width, 0.0f, self.view.frame.size.width, self.view.frame.size.height - 0.0f);
        [slideViewMenu.view removeFromSuperview];
    } completion:^(BOOL finished) {
        //code for completion
        [slideViewMenu.view removeFromSuperview];
        slideViewMenu = nil;
        
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)select_safterverify {
    [self.view endEditing:YES];

        if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
            if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        };
            
        }
        else{
            [[UtillClass sharedInstance] ShowLoaderOn:self.parentViewController withtext:@"Please wait"];
            
            NSMutableDictionary*dicts = [[NSMutableDictionary alloc]init];
            
            
             [dicts setValue:[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"Login Details"]objectForKey:@"CustomerId"] forKey:@"CustomerId"];
             [dicts setValue:[[UtillClass sharedInstance] getcultureID] forKey:@"CultureId"];
            [dicts setValue:_tf_mob.text forKey:@"NewMobileNo"];
            [dicts setValue:_tf_mob.text forKey:@"ConfirmMobileNo"];
            [dicts setValue:str_ccde forKey:@"CountryCode"];
              [dicts setValue:_tf_previous_mob.text forKey:@"MobileNo"];
           
           [[UtillClass sharedInstance] Postresponsetoservice:dicts withBearer:[[UtillClass sharedInstance] getapp_sessiontoken] withurl:[NSString stringWithFormat:@"Account/resetMobileNo"] response:^(NSDictionary * dict_response) {
                [[UtillClass sharedInstance] HideLoader];
                if (dict_response ==nil) {
                    [[UtillClass sharedInstance] showerrortoastmsg:@"Server error, Please try after sometime."];
                    
                }
                else{
                    if ([[dict_response objectForKey:@"Status"] integerValue] == 1) {
                         if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                      [[UtillClass sharedInstance]showtoastmsg:@"Your Mobile number has been reset successfully."];
                         }
                         else{
                          [[UtillClass sharedInstance]showtoastmsg:@"تم تغيير رقم الجوال الخاص بك بنجاح"];
                         }
                        [self performSelector:@selector(callvc) withObject:nil afterDelay:1.8];
                    }
                    else{
                        [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
                        
                    }
                }
            }];
        }
    
    
}
-(void)callvc{
    
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"AccSettingViewController"];
    [self presentViewController:vc animated:false completion:nil];
}
- (IBAction)select_submit:(id)sender {
    [self.view endEditing:YES];
    [dropDown hideDropDown:_btn_submit];
    dropDown = nil;
    if (!_tf_mob.text.length) {
 
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"Please provide valid mobile number. E.g. 00966XXXXXXXXX" onTf:_tf_mob];
        }
        else{
            [[UtillClass sharedInstance] ShowTfErrorMsg:@" يرجى إدخال رقم جوال صحيح. مثلاً 00966XXXXXXXXX" onTf:_tf_mob];
        }
        
    }
    else if (_tf_mob.text.length<maxdigit) {
        
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"Please provide valid mobile number. E.g. 00966XXXXXXXXX" onTf:_tf_mob];
        }
        else{
            [[UtillClass sharedInstance] ShowTfErrorMsg:@" يرجى إدخال رقم جوال صحيح. مثلاً 00966XXXXXXXXX" onTf:_tf_mob];
        }
    }
    else if ([_tf_mob.text  isEqualToString:@"000000000"]) {
        
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"Please provide valid mobile number. E.g. 00966XXXXXXXXX" onTf:_tf_mob];
        }
        else{
            [[UtillClass sharedInstance] ShowTfErrorMsg:@" يرجى إدخال رقم جوال صحيح. مثلاً 00966XXXXXXXXX" onTf:_tf_mob];
        }
    }
    
    else{
        if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
            if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        };
            
        }
        else{
            [[UtillClass sharedInstance] ShowLoaderOn:self.parentViewController withtext:@"Please wait"];
            
            NSMutableDictionary*dicts = [[NSMutableDictionary alloc]init];
            
            
            
            [dicts setValue:_tf_mob.text forKey:@"Email_Mobile"];
            [dicts setValue:@"2" forKey:@"IsEmail_Mobile"];
            [dicts setValue:@"" forKey:@"OTP"];
            [dicts setValue:[_lbl_ccode.text stringByReplacingOccurrencesOfString:@"+" withString:@""] forKey:@"CountryCode"];
            
            
            
            [[UtillClass sharedInstance] Postresponsetoservice:dicts withBearer:nil withurl:[NSString stringWithFormat:@"Account/ChangeMobileNoOTP?CultureId=%@",[[UtillClass sharedInstance]getcultureID]] response:^(NSDictionary * dict_response) {
                [[UtillClass sharedInstance] HideLoader];
                if (dict_response ==nil) {
                    [[UtillClass sharedInstance] showerrortoastmsg:@"Server error, Please try after sometime."];
                    
                }
                else{
                    if ([[dict_response objectForKey:@"Status"] integerValue] == 1) {
                         resend=YES;
                        [[UtillClass sharedInstance] showtoastmsg:[dict_response objectForKey:@"msg"]];
                        
                        _lbl_otpshow.text = [NSString stringWithFormat:@"OTP: %@",[dict_response objectForKey:@"OTP_Temp"]];
                        
                        _tf_mob.enabled=NO;
                        self.btn_submit.enabled=NO;
                      
                       
                        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                            [self.btn_submit setBackgroundImage:[UIImage imageNamed:@"submit_large_grey.png"] forState:UIControlStateNormal];
                        }
                        else{
                            [self.btn_submit setBackgroundImage:[UIImage imageNamed:@"SAG"] forState:UIControlStateNormal];
                        }
                        
                        
                        self.btn_verify.userInteractionEnabled = YES;
                        _btn_verify.enabled=YES;
                        [_btn_verify setBackgroundColor:[UIColor colorWithRed:197/255.0f green:21/255.0f blue:74/255.0f alpha:1]];
                        _lbl_hidden.hidden=YES;
                        
                        
                        _tf1.enabled=YES;
                        _tf2.enabled=YES;
                        _tf3.enabled=YES;
                        _tf4.enabled=YES;
                        _tf5.enabled=YES;
                        _tf6.enabled=YES;
                        
                    }
                    else{
                        [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
                        
                    }
                }
            }];
        }
    }
    
}
- (IBAction)ResendCm:(id)sender {
    [self.view endEditing:YES];
    [dropDown hideDropDown:_btn_submit];
    dropDown = nil;
   
        if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
            if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        };
            
        }
        else{
            
            
            if (resend==NO) {
                
            }
            else{
            [[UtillClass sharedInstance] ShowLoaderOn:self.parentViewController withtext:@"Please wait"];
            
            NSMutableDictionary*dicts = [[NSMutableDictionary alloc]init];
            
            
            
            [dicts setValue:_tf_mob.text forKey:@"Email_Mobile"];
            [dicts setValue:@"2" forKey:@"IsEmail_Mobile"];
            [dicts setValue:@"" forKey:@"OTP"];
            [dicts setValue:[_lbl_ccode.text stringByReplacingOccurrencesOfString:@"+" withString:@""] forKey:@"CountryCode"];
            
            
            
            [[UtillClass sharedInstance] Postresponsetoservice:dicts withBearer:nil withurl:[NSString stringWithFormat:@"Account/ChangeMobileNoOTP?CultureId=%@",[[UtillClass sharedInstance]getcultureID]] response:^(NSDictionary * dict_response) {
                [[UtillClass sharedInstance] HideLoader];
                if (dict_response ==nil) {
                    [[UtillClass sharedInstance] showerrortoastmsg:@"Server error, Please try after sometime."];
                    
                }
                else{
                    if ([[dict_response objectForKey:@"Status"] integerValue] == 1) {
                        [[UtillClass sharedInstance] showtoastmsg:[dict_response objectForKey:@"msg"]];
                        
                        _lbl_otpshow.text = [NSString stringWithFormat:@"OTP: %@",[dict_response objectForKey:@"OTP_Temp"]];
                        _btn_verify.enabled=YES;
                        _tf1.enabled=YES;
                        _tf2.enabled=YES;
                        _tf3.enabled=YES;
                        _tf4.enabled=YES;
                        _tf5.enabled=YES;
                        _tf6.enabled=YES;
                        _lbl_hidden.hidden=YES;
                        
                        _tf_mob.enabled=NO;
                        // _lbl_hidden_submit.hidden=NO;
                        self.btn_submit.enabled=NO;
                        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                            [self.btn_submit setBackgroundImage:[UIImage imageNamed:@"submit_large_grey.png"] forState:UIControlStateNormal];
                        }
                        else{
                            [self.btn_submit setBackgroundImage:[UIImage imageNamed:@"SAG"] forState:UIControlStateNormal];
                        }
                        
                        
                    }
                    else{
                        [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
                        
                    }
                }
            }];
        }
        }
    
}

- (IBAction)selcet_verify:(id)sender {
    [self.view endEditing:YES];
    strcode = [NSString stringWithFormat:@"%@%@%@%@%@%@",_tf1.text,_tf2.text,_tf3.text,_tf4.text,_tf5.text,_tf6.text];
    if (!strcode.length) {
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please enter code sent to your mobile number"];
        }
        else{
           [[UtillClass sharedInstance] showerrortoastmsg:@"يرجى إدخال الرمز الذي تم إرساله إلى رقم جوالك"];
        };
        
        
       
    }
    else if (strcode.length!=6) {
       
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please enter valid code"];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء ادخال الرمز الصحيح"];
        };
        
    }
    else{
        [self.view endEditing:YES];
        
        if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
            if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        };
            
        }
        else{
            [[UtillClass sharedInstance] ShowLoaderOn:self.parentViewController withtext:@"Please wait"];
            NSMutableDictionary*dicts = [[NSMutableDictionary alloc]init];
            
            
            
            
            [dicts setValue:_tf_mob.text forKey:@"Email_Mobile"];
            [dicts setValue:@"2" forKey:@"IsEmail_Mobile"];
            [dicts setValue:strcode forKey:@"OTP"];
            [dicts setValue:[_lbl_ccode.text stringByReplacingOccurrencesOfString:@"+" withString:@""] forKey:@"CountryCode"];
            
            
            [[UtillClass sharedInstance] Postresponsetoservice:dicts withBearer:nil withurl:[NSString stringWithFormat:@"Account/CheckEmailMobileOTP?CultureId=%@",[[UtillClass sharedInstance] getcultureID]] response:^(NSDictionary * dict_response) {
                [[UtillClass sharedInstance] HideLoader];
                
                if (dict_response ==nil) {
                    [[UtillClass sharedInstance] showerrortoastmsg:@"Server error, Please try after sometime."];
                    
                }
                else{
                    if ([[dict_response objectForKey:@"Status"] integerValue] == 1) {
                        
                         [[UtillClass sharedInstance] HideLoader];
                        [[UtillClass sharedInstance]showtoastmsg:[dict_response objectForKey:@"msg"]];
                        
                        
                        [self select_safterverify];
                     
                    }
                    else{
                         [[UtillClass sharedInstance] HideLoader];
                        [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
                        if (count<3) {
                            
                            count++;
                        } else {
                            count=0;
                            NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
                            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                            [[UtillClass sharedInstance] HideLoader];
                            if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                                [[UtillClass sharedInstance]showerrortoastmsg:@"You have entered an incorrect code. please wait for 5 minutes and request for another code."];
                            }
                            else{
                                [[UtillClass sharedInstance]showerrortoastmsg:@"الرمز المُدخل غير صحيح ، يرجى الانتظار لمدة 5 دقائق وقم بطلب رمز جديد آخر."];
                            }
                            [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"Timerbolck"];
                            
                            
                            _view_timer_hidden.hidden=NO;
                            
                            _tf1.enabled=NO;
                            _tf2.enabled=NO;
                            _tf3.enabled=NO;
                            _tf4.enabled=NO;
                            _tf5.enabled=NO;
                            _tf6.enabled=NO;
                            
                            _tf_mob.enabled=NO;
                            self.btn_submit.enabled=NO;
                            if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                                [self.btn_submit setBackgroundImage:[UIImage imageNamed:@"submit_large_grey.png"] forState:UIControlStateNormal];
                            }
                            else{
                                [self.btn_submit setBackgroundImage:[UIImage imageNamed:@"SAG"] forState:UIControlStateNormal];
                            }
                            
                           
                            
                            [self.btn_verify setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
                            [self.btn_verify setBackgroundColor:[UIColor colorWithRed:154.0/255.0 green:154.0/255.0 blue:154.0/255.0 alpha:1.0]];
                            if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                                [self.btn_verify setTitle:@"Wait 05 : 00" forState:UIControlStateNormal];
                            }
                            else{
                                [self.btn_verify setTitle:@"05 : 00 إنتظر" forState:UIControlStateNormal];
                            }
                            self.btn_verify.userInteractionEnabled=NO;
                            
                            
                            //                            [NSTimer scheduledTimerWithTimeInterval:300.0
                            //                                                                 target: self
                            //                                                               selector:@selector(onTick)
                            //                                                               userInfo: nil repeats:NO];
                            
                            
                            [self MAkeAllViewNotClickAble];
                            VerifyBtnTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                                              target: self selector:@selector(ChangeVerifyLabel)
                                                                            userInfo: nil repeats:YES];
                        }
                    }
                }
            }];
        }
        
        
    }
}
-(void) ChangeVerifyLabel
{
    NSString *tempTitleLbl = [NSString stringWithFormat:@"%@",self.btn_verify.currentTitle];
    tempTitleLbl = [tempTitleLbl stringByReplacingOccurrencesOfString:@"Wait" withString:@""];
    NSArray *tempArray = [tempTitleLbl componentsSeparatedByString:@":"];
    if (tempArray.count) {
        NSInteger minVal = [tempArray[0] integerValue];
        NSInteger secVal = [tempArray[1] integerValue];
        if (secVal) {
            secVal = secVal - 1;
        } else {
            minVal = minVal - 1;
            secVal = 59;
        }
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            tempTitleLbl = [NSString stringWithFormat:@"Wait %02d : %02d",(long)minVal,(long)secVal];
        }
        else{
            //Arabic29
            tempTitleLbl = [NSString stringWithFormat:@"%02d : %02d إنتظر",(long)minVal,(long)secVal];
            
        }
    
        if ((minVal <= 0) && (secVal <= 0)) {
            [VerifyBtnTimer invalidate];
            VerifyBtnTimer = nil;
            
            if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                tempTitleLbl = [NSString stringWithFormat:@"VERIFY"];
            }
            else{
                tempTitleLbl = [NSString stringWithFormat:@"تحقق"];
            }
            [self.btn_verify setBackgroundColor:[UIColor colorWithRed:154.0/255.0 green:154.0/255.0 blue:154.0/255.0 alpha:1.0]];//colorWithRed:197.0/255.0 green:21.0/255.0 blue:74.0/255.0 alpha:1.0]];
            [self MAkeAllViewClickAble];
        }
    }
    
    [self.btn_verify setTitle:tempTitleLbl forState:UIControlStateNormal];
}

-(void) MAkeAllViewNotClickAble
{
    _tf1.text=@"";
    _tf2.text=@"";
    _tf3.text=@"";
    _tf4.text=@"";
    _tf5.text=@"";
    _tf6.text=@"";
   
    
    self.btn_submit.userInteractionEnabled = NO;
    self.btn_submit.enabled=NO;
    if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
        [self.btn_submit setBackgroundImage:[UIImage imageNamed:@"submit_large_grey.png"] forState:UIControlStateNormal];
    }
    else{
        [self.btn_submit setBackgroundImage:[UIImage imageNamed:@"SAG"] forState:UIControlStateNormal];
    }
    
    
    self.tf_mob.userInteractionEnabled = NO;
    self.btn_verify.userInteractionEnabled = NO;
    
    resend =NO;
    
}
-(void) MAkeAllViewClickAble
{
    _view_timer_hidden.hidden=YES;
    if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
        [self.btn_submit setBackgroundImage:[UIImage imageNamed:@"submit_btn_l.png"] forState:UIControlStateNormal];
    }else{
        [self.btn_submit setBackgroundImage:[UIImage imageNamed:@"SA"] forState:UIControlStateNormal];
    }
    self.btn_submit.userInteractionEnabled = YES;
    self.btn_submit.enabled=YES;
    self.tf_mob.userInteractionEnabled = YES;
    
    
    self.btn_verify.userInteractionEnabled = NO;
    [self.btn_verify setBackgroundColor:[UIColor colorWithRed:154.0/255.0 green:154.0/255.0 blue:154.0/255.0 alpha:1.0]];
    
    resend =NO;
    
}
-(void)viewWillAppear:(BOOL)animated{
    [self HideSlideMenu];
    [[UtillClass sharedInstance] HideLoader];
    _view_timer_hidden.hidden=YES;
    
    NSString *tempChkStr = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"Timerbolck"]];
    if (tempChkStr.length) {
        NSDate *dates = [[NSUserDefaults standardUserDefaults] objectForKey:@"Timerbolck"];
        
        NSTimeInterval secondsBetween = [[NSDate date] timeIntervalSinceDate:dates];
        
        if (secondsBetween <= 300) {
            [self MAkeAllViewNotClickAble];
            _view_timer_hidden.hidden=YES;
            
            secondsBetween = 300 - secondsBetween;
            _lbl_hidden.hidden = YES;
            [self.btn_verify setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
            [self.btn_verify setBackgroundColor:[UIColor colorWithRed:154.0/255.0 green:154.0/255.0 blue:154.0/255.0 alpha:1.0]];
            
            NSInteger minVal = floor(secondsBetween/60);
            NSInteger secVal = round(secondsBetween - minVal * 60);
            
            if (secVal) {
                secVal -= 1;
            } else {
                minVal -= 1;
                secVal = 59;
            }
            NSString *tempTitleLbl ;
            //Arabic29
            if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                tempTitleLbl = [NSString stringWithFormat:@"Wait %02d : %02d",(long)minVal,secVal];
            }else{
                tempTitleLbl = [NSString stringWithFormat:@"%02d : %02d إنتظر",(long)minVal,secVal];
            }
            
            
            if ((minVal <= 0) && (secVal <= 0)) {
                [VerifyBtnTimer invalidate];
                VerifyBtnTimer = nil;
            }
            [self.btn_verify setTitle:tempTitleLbl forState:UIControlStateNormal];
            
            VerifyBtnTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                              target: self selector:@selector(ChangeVerifyLabel)
                                                            userInfo: nil repeats:YES];
            
            
        } else {
            [self MAkeAllViewClickAble];
        }
    }
    else{
        
        [self MAkeAllViewClickAble];
    }
    
    
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    [textField layoutIfNeeded];
    [[UtillClass sharedInstance] HideTfErrorMsg];
    float scrollOffset = _scr_emailpage.contentOffset.y;
    if (scrollOffset!=0) {
        
    }
    else{
        if ((textField.tag==1) || (textField.tag==2)|| (textField.tag==3)|| (textField.tag==4)|| (textField.tag==5)|| (textField.tag==6)) {
            CGPoint scrollPoint = CGPointMake(0, 70);
            
            [_scr_emailpage setContentOffset:scrollPoint animated:YES];
        }
    }
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    float scrollOffset = _scr_emailpage.contentOffset.y;
    if (scrollOffset!=0) {
        
    }
    else{
        [_scr_emailpage setContentOffset:CGPointMake(0,0) animated:NO];
    }
}
#pragma mark - TextField delgate and TextField Keyboard Handle

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [_scr_emailpage setContentOffset:CGPointMake(0,0) animated:NO];
    [textField resignFirstResponder];
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
