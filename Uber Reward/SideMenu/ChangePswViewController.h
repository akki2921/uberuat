//
//  ChangePswViewController.h
//  Uber Reward
//
//  Created by Arvind Seth on 24/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlidingViewController.h"

@interface ChangePswViewController : UIViewController<slideViewControllerDelegate>{
    SlidingViewController *slideViewMenu;
}
@property (strong, nonatomic) IBOutlet UITextField *tf_oldpsw;
@property (strong, nonatomic) IBOutlet UITextField *tf_newpsw;
@property (strong, nonatomic) IBOutlet UITextField *tf_confpsw;

@end
