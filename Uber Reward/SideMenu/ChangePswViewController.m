//
//  ChangePswViewController.m
//  Uber Reward
//
//  Created by Arvind Seth on 24/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import "ChangePswViewController.h"
#import "UtillClass.h"
#import "BSErrorMessageView.h"
#import "UITextField+BSErrorMessageView.h"
@interface ChangePswViewController ()

@end

@implementation ChangePswViewController
-(void)viewDidAppear:(BOOL)animated {
    [[UtillClass sharedInstance]HideLoader];
    [self HideSlideMenu];
    
  [[UtillClass sharedInstance] showbottom_items];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [_tf_oldpsw setFont:[UIFont fontWithName:@"ClanPro-Book" size:12.6]];
    [_tf_newpsw setFont:[UIFont fontWithName:@"ClanPro-Book" size:12.6]];
    [_tf_confpsw setFont:[UIFont fontWithName:@"ClanPro-Book" size:12.6]];
    
    _tf_oldpsw.layer.borderWidth= 0.6;
    _tf_oldpsw.layer.borderColor=[UIColor colorWithRed:192/255.0f green:192/255.0f blue:192/255.0f alpha:1].CGColor;
    _tf_oldpsw.layer.cornerRadius=1;
    
    
    _tf_newpsw.layer.borderWidth= 0.6;
    _tf_newpsw.layer.borderColor=[UIColor colorWithRed:192/255.0f green:192/255.0f blue:192/255.0f alpha:1].CGColor;
    _tf_newpsw.layer.cornerRadius=1;
    
    
    _tf_confpsw.layer.borderWidth= 0.6;
    _tf_confpsw.layer.borderColor=[UIColor colorWithRed:192/255.0f green:192/255.0f blue:192/255.0f alpha:1].CGColor;
    _tf_confpsw.layer.cornerRadius=1;
    
    
    // Do any additional setup after loading the view.
}
- (IBAction)select_submit:(id)sender {
    [self.view endEditing:YES];
    
    if (!_tf_oldpsw.text.length) {
        
   
        
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"Please provide current password." onTf:_tf_oldpsw];
        }
        else{
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"كلمة المرور الحالية" onTf:_tf_oldpsw];
            
        }
        
    }

    else if (!_tf_newpsw.text.length) {
    
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"Please provide new password." onTf:_tf_newpsw];
        }
        else{
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"يرجى تقديم كلمة المرور الجديدة" onTf:_tf_newpsw];
            
        }
    }

   else if (!_tf_confpsw.text.length) {
        
 
       if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
           [[UtillClass sharedInstance] ShowTfErrorMsg:@"Please confirm your password." onTf:_tf_confpsw];
       }
       else{
           [[UtillClass sharedInstance] ShowTfErrorMsg:@"تأكيد كلمة المرور الجديدة" onTf:_tf_confpsw];
           
       }
       
    }

   else if (_tf_confpsw.text.length<8 && _tf_newpsw.text.length<8) {
       
       
       if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
           [[UtillClass sharedInstance] showerrortoastmsg:@"Password should be made up of 8 characters."];
       }
       else{
           [[UtillClass sharedInstance]showerrortoastmsg:@"يجب أن تتكون كلمة المرور من 8 أحرف"];
           
       }
       
   }
    
    else if (![_tf_confpsw.text isEqualToString:_tf_newpsw.text]) {
     
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"New password and Confirm password do not match. Please re-enter new password" onTf:_tf_confpsw];
        }
        else{
            [[UtillClass sharedInstance] ShowTfErrorMsg:@"كلمة المرور الجديدة وتأكيد كلمة المرور لا تتطابقان. الرجاء إعادة إدخال كلمة مرور الجديدة." onTf:_tf_confpsw];
            
        }
    }
    else{
        if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
            if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        };
            
        }
        else{
            [[UtillClass sharedInstance] ShowLoaderOn:self.parentViewController withtext:@"Please wait"];
            
            NSMutableDictionary*dicts = [[NSMutableDictionary alloc]init];
            
            
            
            [dicts setValue:[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"Login Details"]objectForKey:@"CustomerId"] forKey:@"CustomerId"];
            [dicts setValue:_tf_oldpsw.text forKey:@"OldPassword"];
            [dicts setValue:_tf_newpsw.text forKey:@"NewPassword"];
            [dicts setValue:_tf_confpsw.text forKey:@"ConfirmPassword"];
            [dicts setValue:[[UtillClass sharedInstance] getcultureID] forKey:@"CultureId"];
            
            [[UtillClass sharedInstance] Postresponsetoservice:dicts withBearer:[[UtillClass sharedInstance] getapp_sessiontoken] withurl:[NSString stringWithFormat:@"Account/resetPassword?"] response:^(NSDictionary * dict_response) {
                [[UtillClass sharedInstance] HideLoader];
                if (dict_response ==nil) {
                    [[UtillClass sharedInstance] showerrortoastmsg:@"Server error, Please try after sometime."];
                    
                }
                else{
                    if ([[dict_response objectForKey:@"Status"] integerValue] == 1) {
                        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                           [[UtillClass sharedInstance] showtoastmsg:@"Your Password has been reset successfully."];
                        }
                        else{
                           [[UtillClass sharedInstance] showtoastmsg:@"تم تغيير كلمة المرور بنجاح"];
                        }
                       
                         [self performSelector:@selector(callvccp) withObject:nil afterDelay:1.8];
                       
                    }
                    else{
                        [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
                        
                        
                    }
                }
            }];
        }
    }
    
}
-(void)callvccp{
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"AccSettingViewController"];
    [self presentViewController:vc animated:false completion:nil];
}
- (IBAction)select_sidemenu:(id)sender {
    
    if (slideViewMenu.view) {
        
        [self HideSlideMenu];
    } else {
        slideViewMenu = [[UtillClass sharedInstance] CallSlideMenuView:self yVal:0.0f];
    }
}

#pragma mark - slideView Delegate

-(void) slideViewControllerDone:(SlidingViewController *)sender
{
    if (sender) {
        [self HideSlideMenu];
    }
}
-(void) HideSlideMenu
{
    [UIView animateWithDuration:0.50 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        //code with animation
        // slideViewMenu.view.frame = CGRectMake(-self.view.frame.size.width, 0.0f, self.view.frame.size.width, self.view.frame.size.height - 0.0f);
        [slideViewMenu.view removeFromSuperview];
    } completion:^(BOOL finished) {
        //code for completion
        [slideViewMenu.view removeFromSuperview];
        slideViewMenu = nil;
        
    }];
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}
#pragma mark - UITextFieldDelegate


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    [textField layoutIfNeeded];
    [[UtillClass sharedInstance] HideTfErrorMsg];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
