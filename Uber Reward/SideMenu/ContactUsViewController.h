//
//  ContactUsViewController.h
//  Uber Reward
//
//  Created by Arvind on 23/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NIDropDown.h"
#import "UtillClass.h"
#import "SlidingViewController.h"
@interface ContactUsViewController : UIViewController<NIDropDownDelegate,slideViewControllerDelegate>{
    
    NIDropDown * dropDown;
    NSMutableArray *arr_reason;
    NSMutableArray*arr_reasonid;
    SlidingViewController* slideViewMenu;
    NSString*strresonid;
}
@property (strong, nonatomic) IBOutlet UILabel *lbl_lineprevmsg;
@property (strong, nonatomic) IBOutlet UILabel *lbl;
@property (strong, nonatomic) IBOutlet UITextField *tf_reason;
@property (strong, nonatomic) IBOutlet UITextView *tv_msg;
@property (strong, nonatomic) IBOutlet UIButton *btn_reason;
@property (strong, nonatomic) IBOutlet UIScrollView *scr_contactus;

@end
