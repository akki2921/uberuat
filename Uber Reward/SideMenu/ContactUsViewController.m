//
//  ContactUsViewController.m
//  Uber Reward
//
//  Created by Arvind on 23/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import "ContactUsViewController.h"

@interface ContactUsViewController ()

@end

@implementation ContactUsViewController
-(IBAction)back:(id)sender{
    
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
    [self presentViewController:vc animated:false completion:nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    if ([UIScreen mainScreen].bounds.size.height==568) {
        [_lbl_lineprevmsg setFrame:CGRectMake(186, 395, 88,1)];
        
    }
    arr_reason = [[NSMutableArray alloc]init];
    arr_reasonid = [[NSMutableArray alloc]init];
    
    _tf_reason.layer.borderWidth= 0.6;
    _tf_reason.layer.borderColor=[UIColor colorWithRed:192/255.0f green:192/255.0f blue:192/255.0f alpha:1].CGColor;
    _tf_reason.layer.cornerRadius=1;
    
    _tv_msg.layer.borderWidth= 0.6;
    _tv_msg.layer.borderColor=[UIColor colorWithRed:192/255.0f green:192/255.0f blue:192/255.0f alpha:1].CGColor;
    _tv_msg.layer.cornerRadius=1;
    
    UIToolbar* keyboardToolbar = [[UIToolbar alloc] init];
    [keyboardToolbar sizeToFit];
    UIBarButtonItem *flexBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                      target:nil action:nil];
    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                      target:self action:@selector(yourTextViewDoneButtonPressed)];
    keyboardToolbar.items = @[flexBarButton, doneBarButton];
    _tv_msg.inputAccessoryView = keyboardToolbar;
    
    
    
    CGRect contentRect = CGRectZero;
    
    for (UIView *view in _scr_contactus.subviews) {
        contentRect = CGRectUnion(contentRect, view.frame);
    }
    _scr_contactus.contentSize = CGSizeMake(0, self.view.frame.size.height+170) ;
   
    
    
    // Do any additional setup after loading the view.
}
-(void)yourTextViewDoneButtonPressed
{
    CGPoint scrollPoint = CGPointMake(0, 0);
    [_scr_contactus setContentOffset:scrollPoint animated:YES];
    
    [_tv_msg resignFirstResponder];
}
-(void)viewDidAppear:(BOOL)animated {
    [[UtillClass sharedInstance]HideLoader];
    [self HideSlideMenu];
    [[UtillClass sharedInstance] showbottom_items];
    [self GetReasons];
}
- (void) niDropDownDelegateMethod: (NSString *)btntext withbtntag:(NSString *)btntag dropdown: (NIDropDown *) sender {
    dropDown = nil;
    
    _tf_reason.text= [NSString stringWithFormat:@"%@", [[btntext componentsSeparatedByString:@","]objectAtIndex:0]] ;
    _tv_msg.text=@"";
    strresonid = [arr_reasonid objectAtIndex:[[[btntext componentsSeparatedByString:@","]objectAtIndex:1] intValue]];
    
}
-(IBAction)backregtowel:(id)sender{
    
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
    [self presentViewController:vc animated:false completion:nil];
}
-(IBAction)Viewprevious:(id)sender{
    
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"PreviousMsgViewController"];
    [self presentViewController:vc animated:false completion:nil];
}
- (IBAction)select_sidemenu:(id)sender {
    
    if (slideViewMenu.view) {
        
        [self HideSlideMenu];
    } else {
        slideViewMenu = [[UtillClass sharedInstance] CallSlideMenuView:self yVal:0.0f];
    }
}

#pragma mark - slideView Delegate

-(void) slideViewControllerDone:(SlidingViewController *)sender
{
    if (sender) {
        [self HideSlideMenu];
    }
}
-(void) HideSlideMenu
{
    [UIView animateWithDuration:0.50 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        //code with animation
        // slideViewMenu.view.frame = CGRectMake(-self.view.frame.size.width, 0.0f, self.view.frame.size.width, self.view.frame.size.height - 0.0f);
        [slideViewMenu.view removeFromSuperview];
    } completion:^(BOOL finished) {
        //code for completion
        [slideViewMenu.view removeFromSuperview];
        slideViewMenu = nil;
        
    }];
}

#pragma mark - DropDown Delegate
- (void)OpenDropDown:(UIButton *)sender Array:(NSArray *) arr
{
    [self.view endEditing:YES];
    //    arr = [[NSArray alloc] init];
    NSArray * arrImage = [[NSArray alloc] init];
    
    if(dropDown == nil) {
        CGFloat f = 200;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :arrImage :@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        dropDown = nil;
    }
}

- (IBAction)action_select_ac_sett_lang:(id)sender {
    [self OpenDropDown:sender Array:[arr_reason copy]];
}
    - (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)GetReasons{
  
    if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        };
        
    }
    else{
        [[UtillClass sharedInstance] ShowLoaderOn:self.parentViewController withtext:@"Please wait"];
        
        NSMutableDictionary*dicts = [[NSMutableDictionary alloc]init];
        [dicts setValue:[[UtillClass sharedInstance] getcultureID] forKey:@"CultureId"];
        
        
        [[UtillClass sharedInstance] getresponsefromservice:[NSString stringWithFormat:@"Account/GetReason?CultureId=%@",[[UtillClass sharedInstance] getcultureID]] withbearer:[[UtillClass sharedInstance]getapp_sessiontoken] response:^(NSDictionary * dict_response) {
            [[UtillClass sharedInstance] HideLoader];
            if (dict_response ==nil) {
                [[UtillClass sharedInstance] showerrortoastmsg:@"Server error, Please try after sometime."];
                
            }
            else{
                if ([[dict_response objectForKey:@"Status"] integerValue] == 1) {
                    NSArray *Arr = [dict_response objectForKey:@"ReasonList"];
                   
                    for (int i=0; i<[Arr count]; i++) {
                        [arr_reason addObject:[[Arr objectAtIndex:i]objectForKey:@"ReasonName"]];
                        [arr_reasonid addObject:[[Arr objectAtIndex:i]objectForKey:@"ReasonId"]];
                    }
                  
                }
                else{
                    [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
                }
            }
        }];
    }
   
}

- (IBAction)select_submit:(id)sender {
    [self.view endEditing:YES];
    
    if (!_tf_reason.text.length) {
        
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
[[UtillClass sharedInstance] showerrortoastmsg:@"Please select issue type"];
            
        }
        else{
[[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء إختيار نوع المشكلة"];
            
        }
        
      
    }
    else if (!_tv_msg.text.length || [_tv_msg.text isEqualToString:@"Message"] || [_tv_msg.text isEqualToString:@"إرسال"]) {
        
     
        
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
             [[UtillClass sharedInstance] showerrortoastmsg:@"Please enter message"];
            
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء إدخال رسالة"];
            
        }
    }
    
    else{
        if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
            if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        };
            
        }
        else{
            [[UtillClass sharedInstance] ShowLoaderOn:self.parentViewController withtext:@"Please wait"];
         
            NSMutableDictionary*dicts = [[NSMutableDictionary alloc]init];
            [dicts setValue:[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"UserInfo"]objectForKey:@"TitleId"] forKey:@"TitleId"];
            [dicts setValue:[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"UserInfo"]objectForKey:@"Mobile"] forKey:@"PhoneNumber"];
            [dicts setValue:[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"UserInfo"]objectForKey:@"FirstName"] forKey:@"Name"];
            [dicts setValue:_tv_msg.text forKey:@"Message"];
            [dicts setValue:[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"UserInfo"]objectForKey:@"Email"] forKey:@"Email"];
            
            [dicts setValue:[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"UserInfo"]objectForKey:@"CountryCodeIdId"] forKey:@"CountryCodeIdId"];
            [dicts setValue:[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"Login Details"]objectForKey:@"CustomerId"] forKey:@"CustomerId"];
            [dicts setValue:[[UtillClass sharedInstance] getcultureID] forKey:@"CultureId"];
            [dicts setValue:strresonid forKey:@"ReasonId"];
         
            
            [[UtillClass sharedInstance] Postresponsetoservice:dicts withBearer:[[UtillClass sharedInstance] getapp_sessiontoken] withurl:[NSString stringWithFormat:@"Email/MobileContactUSMail"] response:^(NSDictionary * dict_response) {
                [[UtillClass sharedInstance] HideLoader];
                if (dict_response ==nil) {
                    [[UtillClass sharedInstance] showerrortoastmsg:@"Server error, Please try after sometime."];
                    
                }
                else{
                    if ([[dict_response objectForKey:@"Status"] integerValue] == 1) {
                        [[UtillClass sharedInstance] showtoastmsg:[dict_response objectForKey:@"msg"]];
                        
//                        UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
//                        [self presentViewController:vc animated:false completion:nil];
                      
                    }
                    else{
                        
                        [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
                    }
                }
            }];
            
        }
    }
    
}
- (void)textViewDidBeginEditing:(UITextView *)textView;
{
    CGPoint scrollPoint = CGPointMake(0, 70);
  [_scr_contactus setContentOffset:scrollPoint animated:YES];
        _lbl.hidden = YES;
   
}
- (void)textViewDidEndEditing:(UITextView *) textView
{
    if (![textView hasText]) {
        _lbl.hidden = NO;
    }
}

- (void) textViewDidChange:(UITextView *)textView
{
    if(![textView hasText]) {
        _lbl.hidden = NO;
    }
    else{
        _lbl.hidden = YES;
        
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
