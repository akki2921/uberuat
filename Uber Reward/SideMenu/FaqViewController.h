//
//  FaqViewController.h
//  Uber Reward
//
//  Created by Arvind on 23/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlidingViewController.h"
@interface FaqViewController : UIViewController<slideViewControllerDelegate>{
    SlidingViewController *slideViewMenu;
}
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
