//
//  FaqViewController.m
//  Uber Reward
//
//  Created by Arvind on 23/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import "FaqViewController.h"
#import "UtillClass.h"
#import "MBProgressHUD.h"
@interface FaqViewController ()

@end

@implementation FaqViewController
-(void)viewDidAppear:(BOOL)animated {
    [[UtillClass sharedInstance]HideLoader];
    [self HideSlideMenu];
   // [[UtillClass sharedInstance] showbottom_items];
}
-(IBAction)back:(id)sender{
    
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
    [self presentViewController:vc animated:false completion:nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        };
        
    }
    else{
        FaqViewController *vc= [[FaqViewController alloc]init];
      
        //[MBProgressHUD showHUDAddedTo:self.view animated:YES];
        AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
        
       
       
        
        
        NSURL *targetURL = [NSURL URLWithString:@"https://www.google.co.in"];
        
        NSString*str= [[NSUserDefaults standardUserDefaults] objectForKey:@"applanguage"];
        
        if ([app.ViewWebTypeStr isEqualToString:@"FAQs"]) {
            if ([str isEqualToString:@"English"]) {
                targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@Account/ContentPageMobile/70",[[UtillClass sharedInstance] getwebviewURL]]];
            }
            else{
                targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@Account/ContentPageMobile/72",[[UtillClass sharedInstance] getwebviewURL]]];
            }
        } else if ([app.ViewWebTypeStr isEqualToString:@"About"]) {
            if ([str isEqualToString:@"English"]) {
                targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@Account/ContentPageMobile/74",[[UtillClass sharedInstance] getwebviewURL]]];
            }
            else{
                targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@Account/ContentPageMobile/76",[[UtillClass sharedInstance] getwebviewURL]]];
            }
        } else {
            if ([str isEqualToString:@"English"]) {
                targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@Account/ContentPageMobile/65",[[UtillClass sharedInstance] getwebviewURL]]];
            }
            else{
                targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@Account/ContentPageMobile/67",[[UtillClass sharedInstance] getwebviewURL]]];
            } 
        }
       
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:targetURL cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15.0];
        [_webView loadRequest:theRequest];
        
        
        
        
    }
    
    // Do any additional setup after loading the view.
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
     [MBProgressHUD hideHUDForView:self.view animated:YES];
}
- (void)webViewDidStartLoad:(UIWebView *)webView{
    
      [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
  
     [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}
- (IBAction)select_sidemenu:(id)sender {
    
    if (slideViewMenu.view) {
    
        [self HideSlideMenu];
    } else {
        slideViewMenu = [[UtillClass sharedInstance] CallSlideMenuView:self yVal:0.0f];
    }
}

#pragma mark - slideView Delegate

-(void) slideViewControllerDone:(SlidingViewController *)sender
{
    if (sender) {
        [self HideSlideMenu];
    }
}
-(void) HideSlideMenu
{
    [UIView animateWithDuration:0.50 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        //code with animation
        // slideViewMenu.view.frame = CGRectMake(-self.view.frame.size.width, 0.0f, self.view.frame.size.width, self.view.frame.size.height - 0.0f);
        [slideViewMenu.view removeFromSuperview];
    } completion:^(BOOL finished) {
        //code for completion
        [slideViewMenu.view removeFromSuperview];
        slideViewMenu = nil;
        
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
