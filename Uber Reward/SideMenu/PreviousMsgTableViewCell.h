//
//  PreviousMsgTableViewCell.h
//  Uber Reward
//
//  Created by Arvind Seth on 09/03/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PreviousMsgTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lbl_date;
@property (strong, nonatomic) IBOutlet UILabel *lbl_msg1;
@property (strong, nonatomic) IBOutlet UILabel *lbl_msg2;
@property (strong, nonatomic) IBOutlet UIView *view_top_content;

@end
