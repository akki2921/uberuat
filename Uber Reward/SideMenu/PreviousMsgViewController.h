//
//  PreviousMsgViewController.h
//  Uber Reward
//
//  Created by Arvind Seth on 09/03/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlidingViewController.h"
#import "UtillClass.h"
#import "PreviousMsgTableViewCell.h"
@interface PreviousMsgViewController : UIViewController<slideViewControllerDelegate>
{
    SlidingViewController *slideViewMenu;
    NSMutableArray *arr_date;
    NSMutableArray *arr_msg1;
    NSMutableArray*arr_msg2;
    NSMutableArray *arr_msg3;
    PreviousMsgTableViewCell *cell;
}
@property (strong, nonatomic) IBOutlet UITableView *tbl_prvmsg;

@end
