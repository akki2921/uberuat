//
//  PreviousMsgViewController.m
//  Uber Reward
//
//  Created by Arvind Seth on 09/03/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import "PreviousMsgViewController.h"
#import "PreviousMsgTableViewCell.h"
#import "UtillClass.h"
@interface PreviousMsgViewController ()

@end

@implementation PreviousMsgViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
arr_date=[[NSMutableArray alloc]init];
    arr_msg1=[[NSMutableArray alloc]init];
    arr_msg2=[[NSMutableArray alloc]init];
  arr_msg3=[[NSMutableArray alloc]init];
    
    
    // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated{
    [[UtillClass sharedInstance]HideLoader];
    [self HideSlideMenu];
    [self Getprevoiusmsg];
    
}
-(void)Getprevoiusmsg{
    [self.view endEditing:YES];
    if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        };
        
    }
    else{
        [[UtillClass sharedInstance] ShowLoaderOn:self.parentViewController withtext:@"Please wait"];
        
        NSMutableDictionary*dicts = [[NSMutableDictionary alloc]init];
        [dicts setValue:[[UtillClass sharedInstance] getcultureID] forKey:@"CultureId"];
        
        
        [[UtillClass sharedInstance] getresponsefromservice:[NSString stringWithFormat:@"Account/GetContactUsDetails?CultureId=%@&CustomerId=%@",[[UtillClass sharedInstance] getcultureID],[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"Login Details"]objectForKey:@"CustomerId"]] withbearer:[[UtillClass sharedInstance] getapp_sessiontoken] response:^(NSDictionary * dict_response) {
            [[UtillClass sharedInstance] HideLoader];
            if (dict_response ==nil) {
                [[UtillClass sharedInstance] showerrortoastmsg:@"Server error, Please try after sometime."];
                
            }
            else{
                if ([[dict_response objectForKey:@"Status"] integerValue] == 1) {
                    NSArray *Arr = [dict_response objectForKey:@"ListData"];
                    if (![Arr isKindOfClass:[NSNull class]]) {
                        
                  
                    for (int i=0; i<[Arr count]; i++) {
                      
                        [arr_date addObject:[[Arr objectAtIndex:i]objectForKey:@"CreatedDate"]];
                        [arr_msg1 addObject:[[Arr objectAtIndex:i]objectForKey:@"Message"]];
                        [arr_msg2 addObject:[[Arr objectAtIndex:i]objectForKey:@"ShortMesg"]];
                        [arr_msg3 addObject:[[Arr objectAtIndex:i]objectForKey:@"LongMesg"]];
                        
                    }
                    if (!arr_date.count) {
//                         [[UtillClass sharedInstance] showerrortoastmsg:@"No data found"];
//                         [self dismissViewControllerAnimated:YES completion:nil];
                    }
                    else{
                    [_tbl_prvmsg reloadData];
                    }
                          }
                  
                }
                else{
                    [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
                    [self dismissViewControllerAnimated:false completion:nil];
                }
            }
        }];
    }
    
    
    /*
     UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"RegEmailViewController"];
     [self presentViewController:vc animated:false completion:nil];
     */
}
-(IBAction)backregtowel:(id)sender{
    
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"ContactUsViewController"];
    [self presentViewController:vc animated:false completion:nil];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [arr_date count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"PreviousMsgTableViewCell";
    
    PreviousMsgTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier forIndexPath: indexPath];
    
     cell.lbl_date.text = [arr_date objectAtIndex:indexPath.section];
     cell.lbl_msg1.text = [arr_msg1 objectAtIndex:indexPath.section];
     cell.lbl_msg2.text = [arr_msg2 objectAtIndex:indexPath.section];
    cell.selectionStyle =UITableViewCellSelectionStyleNone;
    
    cell.layer.borderWidth= 1;
    cell.layer.borderColor=[UIColor colorWithRed:192/255.0f green:192/255.0f blue:192/255.0f alpha:1].CGColor;
    [cell.view_top_content setFrame:CGRectMake(cell.view_top_content.frame.origin.x, cell.view_top_content.frame.origin.y, cell.view_top_content.frame.size.width, 34)];
    
    cell.layer.cornerRadius=1;
    return cell;
    
}
- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section
{
    //Set the background color of the View
    view.tintColor = [UIColor clearColor];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *str=[arr_msg2 objectAtIndex:indexPath.section];
    
    if (str.length<50) {
        return 57;
    }
    else if ((str.length>50) && (str.length<100)){
        return 85;
    }
    return 121;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 8;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@""
                                 message:[arr_msg3 objectAtIndex:indexPath.section]
                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                    
                                }];
 
    [alert addAction:yesButton];
 
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}
- (IBAction)select_side_menu:(id)sender {
    if (slideViewMenu.view) {
        [self HideSlideMenu];
    } else {
        slideViewMenu = [[UtillClass sharedInstance] CallSlideMenuView:self yVal:0.0f];
        //slideViewMenu.Str_isLogIn = @"noLogin";
    }
}


#pragma mark - slideView Delegate

-(void) slideViewControllerDone:(SlidingViewController *)sender
{
    if (sender) {
        [self HideSlideMenu];
    }
}
-(void) HideSlideMenu
{
    [UIView animateWithDuration:0.50 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        //code with animation
        // slideViewMenu.view.frame = CGRectMake(-self.view.frame.size.width, 0.0f, self.view.frame.size.width, self.view.frame.size.height - 0.0f);
        [slideViewMenu.view removeFromSuperview];
    } completion:^(BOOL finished) {
        //code for completion
        [slideViewMenu.view removeFromSuperview];
        slideViewMenu = nil;
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
