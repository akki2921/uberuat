//
//  T&CViewController.m
//  Uber Reward
//
//  Created by Arvind on 23/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import "T&CViewController.h"
#import "UtillClass.h"
@interface T_CViewController ()

@end

@implementation T_CViewController
-(void)viewDidAppear:(BOOL)animated {
    
    [[UtillClass sharedInstance]HideLoader];
    [self HideSlideMenu];
    
    [[UtillClass sharedInstance] showbottom_items];
}
-(IBAction)back:(id)sender{
    
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
    [self presentViewController:vc animated:false completion:nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        };
        
    }
    else{
        
        [[UtillClass sharedInstance] ShowLoaderOn:[self parentViewController] withtext:@"Please wait..."];
        
        NSURL *targetURL = [NSURL URLWithString:@"https://www.kent.co.in/customer-care-service"];
        NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
        [_WebView loadRequest:request];
        
    }
    
    // Do any additional setup after loading the view.
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    //if you want to do anything else after loading the content of webview you can write code here. for example after loading the content you want to show a button on controller you can show it here.
    [[UtillClass sharedInstance]HideLoader];
    
}


- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [[UtillClass sharedInstance]HideLoader];
    [[UtillClass sharedInstance]showtoastmsg:@"Something went wrong, please retry!!"];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)select_sidemenu:(id)sender {
    
    if (slideViewMenu.view) {
        
        [self HideSlideMenu];
    
    } else {
        slideViewMenu = [[UtillClass sharedInstance] CallSlideMenuView:self yVal:0.0f];
    }
}

#pragma mark - slideView Delegate

-(void) slideViewControllerDone:(SlidingViewController *)sender
{
    if (sender) {
        [self HideSlideMenu];
    }
}
-(void) HideSlideMenu
{
    [UIView animateWithDuration:0.50 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        //code with animation
        // slideViewMenu.view.frame = CGRectMake(-self.view.frame.size.width, 0.0f, self.view.frame.size.width, self.view.frame.size.height - 0.0f);
        [slideViewMenu.view removeFromSuperview];
    } completion:^(BOOL finished) {
        //code for completion
        [slideViewMenu.view removeFromSuperview];
        slideViewMenu = nil;
        
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
