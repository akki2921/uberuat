//
//  ViewActivityViewController.h
//  Uber Reward
//
//  Created by Arvind Seth on 17/03/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlidingViewController.h"
#import "CellActTableViewCell.h"
#import "DatePickerViewController.h"
#import "NIDropDown.h"
#import "CellActTableViewCellEng.h"


@interface ViewActivityViewController : UIViewController<slideViewControllerDelegate,myDatePickerViewControllerDelegate,NIDropDownDelegate,UIDocumentInteractionControllerDelegate,UINavigationControllerDelegate>
{
    
    IBOutlet UIImageView *img_drop;
    NSMutableArray *arrdrkey;
    NSMutableArray*arrdrval;
    NSString *TypeStr;
    SlidingViewController *slideViewMenu;
    DatePickerViewController *datePickerView;
    NIDropDown *dropDown;
    
    IBOutlet UITextField *tf_Date1;
    IBOutlet UITextField *tf_Date2;
    
    IBOutlet UIButton *tf_AllType;
    IBOutlet UIButton *btn_Search;
    IBOutlet UIButton *btn_Reset;
    IBOutlet UIButton *btn_download;
    NSString *strurlpdf;
    IBOutlet UIScrollView *Scroll_View;
    IBOutlet UILabel *lbl_DisplayRecord;
    IBOutlet UITableView *Table_ViewAct;
    
    NSMutableArray *Array_TableData;
    NSArray *TempDropDownArr;
}
- (IBAction)Action_Back:(id)sender;
- (IBAction)Action_Menu:(id)sender;
- (IBAction)Action_Search:(id)sender;
- (IBAction)Action_Reset:(id)sender;
- (IBAction)Action_DownLoadSat:(id)sender;
- (IBAction)Action_AllType:(id)sender;






@end
