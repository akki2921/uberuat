//
//  ViewActivityViewController.m
//  Uber Reward
//
//  Created by Arvind Seth on 17/03/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import "ViewActivityViewController.h"
//#import "AlwaysOpaqueImageView.h"
//#include <objc/runtime.h>
@interface ViewActivityViewController ()

@end

@implementation ViewActivityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
     if([UIScreen mainScreen].bounds.size.height==812 ){
         
    [img_drop setFrame:CGRectMake(img_drop.frame.origin.x, img_drop.frame.origin.y+1, img_drop.frame.size.width, 20)];
    [btn_Search setFrame:CGRectMake(btn_Search.frame.origin.x, btn_Search.frame.origin.y, btn_Search.frame.size.width, 36)];
    [btn_Reset setFrame:CGRectMake(btn_Reset.frame.origin.x, btn_Reset.frame.origin.y, btn_Reset.frame.size.width, 36)];
    [btn_download setFrame:CGRectMake(btn_download.frame.origin.x, btn_download.frame.origin.y, btn_download.frame.size.width, 36)];
         
    [tf_AllType setFrame:CGRectMake(tf_AllType.frame.origin.x, tf_AllType.frame.origin.y, tf_AllType.frame.size.width, 36)];
         
     }
    
    arrdrkey = [[NSMutableArray alloc]init];
    arrdrval= [[NSMutableArray alloc]init];
    
    // Do any additional setup after loading the view.
    Array_TableData = [[NSMutableArray alloc] init];
    TempDropDownArr = [NSArray arrayWithObjects:@"All", @"Earn", @"Spend", @"Expiry", nil];
    if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
        
        [Table_ViewAct registerNib:[UINib nibWithNibName:@"CellActTableViewCellEng" bundle:nil] forCellReuseIdentifier:@"CellActTableViewCellEng"];
    }
    else{
    [Table_ViewAct registerNib:[UINib nibWithNibName:@"CellActTableViewCell" bundle:nil] forCellReuseIdentifier:@"CellActTableViewCell"];
    }
   
    Scroll_View.scrollEnabled = YES;
    Scroll_View.userInteractionEnabled = YES;
    Scroll_View.contentSize = CGSizeMake(Table_ViewAct.frame.size.width,0);
    
    tf_Date1.layer.borderColor=[UIColor colorWithRed:192/255.0f green:192/255.0f blue:192/255.0f alpha:1].CGColor;
    tf_Date1.layer.borderWidth= 1;
    tf_Date1.layer.cornerRadius=1;
    
    tf_Date2.layer.borderColor=[UIColor colorWithRed:192/255.0f green:192/255.0f blue:192/255.0f alpha:1].CGColor;
    tf_Date2.layer.borderWidth= 1;
    tf_Date2.layer.cornerRadius=1;
    
    tf_AllType.layer.borderColor=[UIColor colorWithRed:192/255.0f green:192/255.0f blue:192/255.0f alpha:1].CGColor;
    tf_AllType.layer.borderWidth= 1;
    tf_AllType.layer.cornerRadius=1;
    
   //
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) onClickReset
{
    [tf_AllType setTitle:[NSString stringWithFormat:@"%@",arrdrval[0]] forState:UIControlStateNormal];
    TypeStr = arrdrkey[0];
    tf_Date2.text = @"";
    tf_Date1.text = @"";
    if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
         lbl_DisplayRecord.text = @"Displaying Records: 0";
    }
    else{
        lbl_DisplayRecord.text = @"0 :عرض الأرشيف";
    }
    // [self GetHOmesummury];
}

-(void)viewDidAppear:(BOOL)animated {
    [[UtillClass sharedInstance]HideLoader];
    [self HideSlideMenu];
    [[UtillClass sharedInstance] showbottom_items];
 
    [self Gettype];
//    for (UIView * view in Scroll_View.subviews) {
//        if ([view isKindOfClass:[UIImageView class]]) {
//            if (view.alpha == 0 && view.autoresizingMask == UIViewAutoresizingFlexibleBottomMargin) {
//                if (view.frame.size.width < 10 && view.frame.size.height > view.frame.size.width) {
//                    if (Scroll_View.frame.size.height < Scroll_View.contentSize.height) {
//                        // Swizzle class for found imageView, that should be scroll indicator
//                        object_setClass(view, [AlwaysOpaqueImageView class]);
//
//                        break;
//                    }
//                }
//            }
//        }
//    }
    // Ask to flash indicator to turn it on
    [Scroll_View flashScrollIndicators];
}

-(void)Gettype{
    [self.view endEditing:YES];
    
   
    
    if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        };
        
    }
    else{
        [[UtillClass sharedInstance] ShowLoaderOn:self.parentViewController withtext:@"Please wait"];
        
      
        
        [[UtillClass sharedInstance] getresponsefromservice:[NSString stringWithFormat:@"Admin/GetFixedResourcesMobile?CultureId=%@&ResourceMstId=9",[[UtillClass sharedInstance] getcultureID]] withbearer:[[UtillClass sharedInstance] getapp_sessiontoken] response:^(NSDictionary * dict_response) {
            [[UtillClass sharedInstance] HideLoader];
            if (dict_response ==nil) {
                [[UtillClass sharedInstance] showerrortoastmsg:@"Server error, Please try after sometime."];
                
            }
            else{
                if ([[dict_response objectForKey:@"Status"] integerValue] == 1) {
                   
                     NSArray *Arr = [dict_response objectForKey:@"ListData"];
                    
                    if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                        [arrdrkey addObject:@"0"];
                        [arrdrval addObject:@"All"];
                    }
                   else{
                        [arrdrkey addObject:@"0"];
                        [arrdrval addObject:@"الكل"];
                        
                    }
                        for (int i=0; i<[Arr count]; i++) {
                            
                            [arrdrkey addObject:[[Arr objectAtIndex:i]objectForKey:@"Key"]];
                            [arrdrval addObject:[[Arr objectAtIndex:i]objectForKey:@"Value"]];
                         
                            
                        }
                    [tf_AllType setTitle:[NSString stringWithFormat:@"%@",arrdrval[0]] forState:UIControlStateNormal];
                    tf_Date2.text = @"";
                    tf_Date1.text = @"";
                   [self GetHOmesummury];
                }
                else{
                   
                    [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
                }
            }
        }];
    }
    
    
}
-(void)GetHOmesummury{
    [self.view endEditing:YES];
    
    NSString *FromDateStr = [NSString stringWithFormat:@"%@",tf_Date1.text];
    NSString *ToDateStr = [NSString stringWithFormat:@"%@",tf_Date2.text];
    NSString *strintval = [NSString stringWithFormat:@"%@",tf_AllType.titleLabel.text];
    
//    NSString *TypeStr=@"";
//    if ([strintval isEqualToString:@"All"]) {
//        TypeStr= @"0";
//    }
//
//    else if ([strintval isEqualToString:@"Earn"]) {
//        TypeStr= @"602";
//    }
//    else if ([strintval isEqualToString:@"Spend"]) {
//        TypeStr= @"601";
//    }
//    else{
//        TypeStr= @"603";
//    }
    
    if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
        if (FromDateStr.length) {
            if (!ToDateStr.length) {
                [[UtillClass sharedInstance] showerrortoastmsg:@"Please enter To date"];
                return;
            }
        }
        else if (ToDateStr.length) {
            if (!FromDateStr.length) {
                [[UtillClass sharedInstance] showerrortoastmsg:@"Please enter From date"];
                return;
            }
        }
    }
    else{
        if (FromDateStr.length) {
            if (!ToDateStr.length) {
                [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء أدخال التاريخ"];
                return;
            }
        }
        else if (ToDateStr.length) {
            if (!FromDateStr.length) {
                [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء أدخال التاريخ"];
                return;
            }
        }
    }
    
    
    if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        };
        
    }
    else{
        [[UtillClass sharedInstance] ShowLoaderOn:self.parentViewController withtext:@"Please wait"];
        
//        data.put("FromDate", txtfromdate.getText().toString());
//        data.put("ToDate", txttodate.getText().toString());
//        data.put("Type", String.valueOf(idType));
        
        [[UtillClass sharedInstance] getresponsefromservice:[NSString stringWithFormat:@"Statement/getFilterStatment?CultureId=%@&CustomerId=%@&altId=0&FromDate=%@&ToDate=%@&Type=%@",[[UtillClass sharedInstance] getcultureID],[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"Login Details"]objectForKey:@"CustomerId"],FromDateStr,ToDateStr,TypeStr] withbearer:[[UtillClass sharedInstance] getapp_sessiontoken] response:^(NSDictionary * dict_response) {
            [[UtillClass sharedInstance] HideLoader];
            if (dict_response ==nil) {
                [[UtillClass sharedInstance] showerrortoastmsg:@"Server error, Please try after sometime."];
                
            }
            else{
                if ([[dict_response objectForKey:@"Status"] integerValue] == 1) {
//                    _lbl_profilename.text= [NSString stringWithFormat:@"Welcome %@!",[dict_response objectForKey:@"Name"]];
                    \
                    Array_TableData = [[NSMutableArray alloc] init];
                    Array_TableData = [[dict_response valueForKey:@"lstStatement"] mutableCopy];
                    
                  
                    
                    if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                     
                         lbl_DisplayRecord.text = [NSString stringWithFormat:@"Displaying Records: %lu",(unsigned long)Array_TableData.count];
                    }
                    else{
                        lbl_DisplayRecord.text = [NSString stringWithFormat:@"%lu :عرض الأرشيف",(unsigned long)Array_TableData.count];
                    }
                    
                    
                    lbl_DisplayRecord.hidden=NO;
                    btn_download.hidden = NO;
                    [Table_ViewAct reloadData];
                    
                    [self downloadstatement];
                }
                else{
                    lbl_DisplayRecord.hidden=YES;
                    btn_download.hidden = YES;
                    
                    [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
                }
            }
        }];
    }
    
   
}
-(void)downloadstatement{
    [self.view endEditing:YES];
    
    NSString *FromDateStr = [NSString stringWithFormat:@"%@",tf_Date1.text];
    NSString *ToDateStr = [NSString stringWithFormat:@"%@",tf_Date2.text];
    NSString *strintval = [NSString stringWithFormat:@"%@",tf_AllType.titleLabel.text];
    
//    NSString *TypeStr=@"";
//    if ([strintval isEqualToString:@"All"]) {
//        TypeStr= @"0";
//    }
//
//    else if ([strintval isEqualToString:@"Earn"]) {
//        TypeStr= @"602";
//    }
//    else if ([strintval isEqualToString:@"Spend"]) {
//        TypeStr= @"601";
//    }
//    else{
//        TypeStr= @"603";
//    }
    
    if (FromDateStr.length) {
        if (!ToDateStr.length) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please enter To date"];
            return;
        }
    }
    
    
    if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        };
        
    }
    else{
        [[UtillClass sharedInstance] ShowLoaderOn:self.parentViewController withtext:@"Please wait"];
        
       
        
        [[UtillClass sharedInstance] getresponsefromservice:[NSString stringWithFormat:@"Task/GetDownloadPDF?CultureId=%@&CustomerId=%@&altId=0&FromDate=%@&ToDate=%@&Type=%@&docType=2",[[UtillClass sharedInstance] getcultureID],[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"Login Details"]objectForKey:@"CustomerId"],FromDateStr,ToDateStr,TypeStr] withbearer:[[UtillClass sharedInstance] getapp_sessiontoken] response:^(NSDictionary * dict_response) {
            [[UtillClass sharedInstance] HideLoader];
            if (dict_response ==nil) {
                [[UtillClass sharedInstance] showerrortoastmsg:@"Server error, Please try after sometime."];
                
            }
            else{
                if ([[dict_response objectForKey:@"Status"] integerValue] == 1) {
                    
//                    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[dict_response objectForKey:@"PDF_URL"]]]) {
                    strurlpdf =[dict_response objectForKey:@"PDF_URL"];
                  //
                    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
                    [tracker set:kGAIScreenName value:@"ios  View Activity"];
                    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Download Statement"
                                                                          action:@"button_press"
                                                                           label:[[[UtillClass sharedInstance] getDictUserdefaultwithKayname:@"Login Details"]objectForKey:@"CustomerId"]
                                                                           value:nil] build]];
                //}
                }
                else{
                 
                    
                    [[UtillClass sharedInstance] showerrortoastmsg:[dict_response objectForKey:@"msg"]];
                }
            }
        }];
    }
    
    
}
-(IBAction)pdfdownl:(id)sender{
    
    
    NSString *strurl = [strurlpdf stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strurl]];
    
    
}
- (IBAction)Action_Back:(id)sender {
    [self dismissViewControllerAnimated:false completion:nil];
}

- (IBAction)Action_Menu:(id)sender {
    if (slideViewMenu.view) {
        [self HideSlideMenu];
    } else {
        slideViewMenu = [[UtillClass sharedInstance] CallSlideMenuView:self yVal:0.0f];
    }
}


#pragma mark - slideView Delegate

-(void) slideViewControllerDone:(SlidingViewController *)sender
{
    if (sender) {
        [self HideSlideMenu];
    }
}
-(void) HideSlideMenu
{
    [UIView animateWithDuration:0.50 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        //code with animation
        // slideViewMenu.view.frame = CGRectMake(-self.view.frame.size.width, 0.0f, self.view.frame.size.width, self.view.frame.size.height - 0.0f);
        [slideViewMenu.view removeFromSuperview];
    } completion:^(BOOL finished) {
        //code for completion
        [slideViewMenu.view removeFromSuperview];
        slideViewMenu = nil;
        
    }];
}

 

- (IBAction)Action_Search:(id)sender {
    [dropDown hideDropDown:tf_AllType];
    dropDown = nil;
    Array_TableData = [[NSMutableArray alloc] init];
    [Table_ViewAct reloadData];
    
    [self GetHOmesummury];
}

- (IBAction)Action_Reset:(id)sender {
    [dropDown hideDropDown:tf_AllType];
    dropDown = nil;
    [self onClickReset];
    [self GetHOmesummury];
}

- (IBAction)Action_DownLoadSat:(id)sender {
    UIButton *btn = (UIButton *) sender;
    NSString *urlStr = [NSString stringWithFormat:@"%@",[[Array_TableData objectAtIndex:btn.tag] valueForKey:@"VoucherURL"]];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
    
    /*
    NSArray *arr=[urlStr componentsSeparatedByString:@"/"];
    
    NSURL* url = [[NSURL alloc] initWithString:urlStr];
    NSURL* documentsUrl = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask].firstObject;
    NSURL* destinationUrl = [documentsUrl URLByAppendingPathComponent:[arr objectAtIndex:[arr count]-1]];
    
    // Actually download
    NSError* err = nil;
    NSData* fileData = [[NSData alloc] initWithContentsOfURL:url options:NSDataReadingUncached error:&err];
    if (!err && fileData && fileData.length && [fileData writeToURL:destinationUrl atomically:true])
    {
        // Downloaded and saved, now present the document controller (store variable, or risk garbage collection!)
        UIDocumentInteractionController* document = [UIDocumentInteractionController interactionControllerWithURL:destinationUrl];
        document.delegate = self;
        [document presentPreviewAnimated:YES];
    }
    */
}

- (IBAction)Action_AllType:(id)sender {
     [self OpenDropDown:sender Array:[arrdrval copy]];
}

- (void) niDropDownDelegateMethod: (NSString *)btntext withbtntag:(NSString *)btntag dropdown: (NIDropDown *) sender {
    dropDown = nil;
    [tf_AllType setTitle:[NSString stringWithFormat:@"%@",[[btntext componentsSeparatedByString:@","]objectAtIndex:0]] forState:UIControlStateNormal];
    TypeStr = [arrdrkey objectAtIndex:[[[btntext componentsSeparatedByString:@","]objectAtIndex:1] intValue]];
}
#pragma mark - DropDown Delegate
- (void)OpenDropDown:(UIButton *)sender Array:(NSArray *) arr
{
    [self.view endEditing:YES];
    //    arr = [[NSArray alloc] init];
    
    NSArray * arrImage = [[NSArray alloc] init];
    
   // arrImage = [arr_imgflag mutableCopy];
    
    
    if(dropDown == nil) {
        CGFloat f = 200;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :arrImage :@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        dropDown = nil;
    }
}

#pragma mark - TableView methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return Array_TableData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
        CellActTableViewCellEng *cell = [tableView dequeueReusableCellWithIdentifier:@"CellActTableViewCellEng"];
        
        if (cell ==nil)
        {
            [Table_ViewAct registerClass:[CellActTableViewCellEng class] forCellReuseIdentifier:@"CellActTableViewCellEng"];
            
            cell = [Table_ViewAct dequeueReusableCellWithIdentifier:@"CellActTableViewCellEng"];
        }
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.view_BG.backgroundColor = [UIColor clearColor];
        
      
        
        cell.lbl_ActivityDate.text = [NSString stringWithFormat:@"  %@",[[Array_TableData objectAtIndex:indexPath.row] valueForKey:@"ActivityDate"]];
        cell.lbl_Points.text = [NSString stringWithFormat:@"%@",[[UtillClass sharedInstance] getpointformatofint:[[[Array_TableData objectAtIndex:indexPath.row] valueForKey:@"Points"] intValue]]];
        cell.lbl_Balance.text = [NSString stringWithFormat:@"%@",[[UtillClass sharedInstance] getpointformatofint:[[[Array_TableData objectAtIndex:indexPath.row] valueForKey:@"ClosingBalance"]intValue]]];
        
        //    cell.lbl_Voucher.text = [NSString stringWithFormat:@"%@",[[Array_TableData objectAtIndex:indexPath.row] valueForKey:@"Voucher"]];
        cell.lbl_Type.text = [NSString stringWithFormat:@"%@",[[Array_TableData objectAtIndex:indexPath.row] valueForKey:@"ActivityType"]];
        
        if (cell.btn_Voucher) {
            [cell.btn_Voucher removeFromSuperview];
        }
        
        cell.btn_Voucher = [[UIButton alloc] initWithFrame:cell.lbl_Voucher.frame];
        cell.lbl_Voucher.hidden = YES;
        cell.btn_Voucher.tag = indexPath.row;
        [cell.btn_Voucher setTitle:[NSString stringWithFormat:@"%@",[[Array_TableData objectAtIndex:indexPath.row] valueForKey:@"Voucher"]] forState:UIControlStateNormal];
        [cell.btn_Voucher setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        [cell.btn_Voucher.titleLabel setFont:cell.lbl_Balance.font];
        [cell.btn_Voucher setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [cell.contentView addSubview:cell.btn_Voucher];
        [cell.btn_Voucher addTarget:self action:@selector(ClickForDownloadURL:) forControlEvents:UIControlEventTouchUpInside];
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else{
    CellActTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellActTableViewCell"];
    
    if (cell ==nil)
    {
        [Table_ViewAct registerClass:[CellActTableViewCell class] forCellReuseIdentifier:@"CellActTableViewCell"];
        
        cell = [Table_ViewAct dequeueReusableCellWithIdentifier:@"CellActTableViewCell"];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.view_BG.backgroundColor = [UIColor clearColor];
    
    /*
     ActivityDate = "21-03-2018";
     ActivityType = Spend;
     ClosingBalance = 56600;
     OpeningBalance = 60600;
     Points = 4000;
     Voucher = "Home Centre";
     VoucherURL = "http://gotagift.co/B8vCxg7";
     
     */
    cell.lbl_ActivityDate.text = [NSString stringWithFormat:@"  %@",[[Array_TableData objectAtIndex:indexPath.row] valueForKey:@"ActivityDate"]];
    cell.lbl_Points.text = [NSString stringWithFormat:@"%@",[[UtillClass sharedInstance] getpointformatofint:[[[Array_TableData objectAtIndex:indexPath.row] valueForKey:@"Points"] intValue]]];
    cell.lbl_Balance.text = [NSString stringWithFormat:@"%@",[[UtillClass sharedInstance] getpointformatofint:[[[Array_TableData objectAtIndex:indexPath.row] valueForKey:@"ClosingBalance"]intValue]]];
    
//    cell.lbl_Voucher.text = [NSString stringWithFormat:@"%@",[[Array_TableData objectAtIndex:indexPath.row] valueForKey:@"Voucher"]];
    cell.lbl_Type.text = [NSString stringWithFormat:@"%@",[[Array_TableData objectAtIndex:indexPath.row] valueForKey:@"ActivityType"]];
    
    if (cell.btn_Voucher) {
        [cell.btn_Voucher removeFromSuperview];
    }
    
    cell.btn_Voucher = [[UIButton alloc] initWithFrame:cell.lbl_Voucher.frame];
    cell.lbl_Voucher.hidden = YES;
    cell.btn_Voucher.tag = indexPath.row;
    [cell.btn_Voucher setTitle:[NSString stringWithFormat:@"%@",[[Array_TableData objectAtIndex:indexPath.row] valueForKey:@"Voucher"]] forState:UIControlStateNormal];
    [cell.btn_Voucher setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [cell.btn_Voucher.titleLabel setFont:cell.lbl_Balance.font];
    [cell.btn_Voucher setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
    [cell.contentView addSubview:cell.btn_Voucher];
    [cell.btn_Voucher addTarget:self action:@selector(ClickForDownloadURL:) forControlEvents:UIControlEventTouchUpInside];
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 35;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    app.strAppSelectedMainColor = [NSString stringWithFormat:@"%@",[[Array_TableData objectAtIndex:indexPath.row] valueForKey:@"ColorName"]];
    
    [CommonClass withAnimationNavigateVC:@"FinalSelYourColorViewController" stName:@"Main" pView:self ts:ARTransitionStyleRightToLeft];
    */
}

-(IBAction)ClickForDownloadURL:(id)sender
{
    UIButton *btn = (UIButton *) sender;
    NSString *urlStr = [NSString stringWithFormat:@"%@",[[Array_TableData objectAtIndex:btn.tag] valueForKey:@"VoucherURL"]];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
    
    
    
}
- (UIViewController*)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller
{
    return self;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    NSString * language = [[NSLocale preferredLanguages] firstObject];
    if (textField == tf_Date1) {
        NSDate *Seldate = nil;
        if (textField.text.length) {
            NSDateFormatter *formate = [[NSDateFormatter alloc] init];
            [formate setDateFormat:@"dd/MM/yyyy"];
            if([[language substringToIndex:2] isEqualToString:@"ar"] && [[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]){
                [formate setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];;
            }
            Seldate = [formate dateFromString:textField.text];
        }
        [self ShowDatePickerMin:nil max:[NSDate date] sel:Seldate tag:1];
        
        return NO;
    } else if (textField == tf_Date2) {
        
        NSDate *Seldate = nil;
        if (textField.text.length) {
            NSDateFormatter *formate = [[NSDateFormatter alloc] init];
            [formate setDateFormat:@"dd/MM/yyyy"];
            if([[language substringToIndex:2] isEqualToString:@"ar"] && [[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]){
                [formate setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];;
            }
            
            Seldate = [formate dateFromString:textField.text];
        }
        [self ShowDatePickerMin:nil max:[NSDate date] sel:Seldate tag:2];
        return NO;
    }
    
    return YES;
}

-(void) ShowDatePickerMin:(NSDate *) minDate max:(NSDate *) maxDate sel:(NSDate *) cDate tag:(NSInteger) tagVal
{
    [self.view endEditing:YES];
    if (datePickerView.view) {
        [datePickerView.view removeFromSuperview];
        datePickerView = nil;
    }
    
    datePickerView = [[DatePickerViewController alloc] init];
    [self addChildViewController:datePickerView];
    datePickerView.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    datePickerView.view.tag = tagVal;
    //    if (tagVal == 2) {
    //        datePickerView.datePicker_MyDate.datePickerMode = UIDatePickerModeTime;
    //    } else {
    datePickerView.datePicker_MyDate.datePickerMode = UIDatePickerModeDate;
    //    }
    
    datePickerView.datePicker_MyDate.backgroundColor = [UIColor whiteColor];
    if (minDate) {
        datePickerView.datePicker_MyDate.minimumDate = minDate;
    }
    
    if (maxDate) {
        datePickerView.datePicker_MyDate.maximumDate = maxDate;
    }
    
    if (cDate) {
        datePickerView.datePicker_MyDate.date = cDate;
    }
    
    [self.view addSubview:datePickerView.view];
    [datePickerView didMoveToParentViewController:self];
    
    datePickerView.delegate = self;
}

-(void) DatePickerViewControllerDone:(DatePickerViewController *)sender
{
    if (sender.view.tag == 1) {
        tf_Date1.text = [NSString stringWithFormat:@"%@",sender.SelectedDateStr];
    } else {
        tf_Date2.text = [NSString stringWithFormat:@"%@",sender.SelectedDateStr];
    }
    
    if (datePickerView.view) {
        [datePickerView.view removeFromSuperview];
        datePickerView = nil;
    }
}
-(void) DatePickerViewControllerCancel:(DatePickerViewController *)sender
{
    if (datePickerView.view) {
        [datePickerView.view removeFromSuperview];
        datePickerView = nil;
    }
}

@end
