//
//  AbhiLoaderView.m
//  LoaderAnimation
//
//  Created by 9Dim on 28/12/17.
//  Copyright © 2017 9Dim. All rights reserved.
//

#import "AbhiLoaderView.h"
#import "MBProgressHUD.h"
@interface AbhiLoaderView ()

@end

@implementation AbhiLoaderView
@synthesize lbl_text,str_text;

- (void)viewDidLoad {
    [super viewDidLoad];

   // lbl_text.textColor =[UIColor colorWithRed:25.0/255.0 green:38.0/255.0 blue:91.0/255.0 alpha:1];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    lbl_text.font = [UIFont fontWithName:@"ClanPro-Medium" size:12.9];
   
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Private Methods

- (void)startProgressAnimation {
    __weak AbhiLoaderView *wSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5f * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        __strong AbhiLoaderView *sSelf = wSelf;
        if (sSelf) {
            [sSelf progressAnimationStep];
            [sSelf startProgressAnimation];
        }
    });
}

- (void)progressAnimationStep {
    progress += 0.01f;

}

- (void)startColoringAnimation {
    [testLoader setColoringAnimationWithColors:@[[UIColor colorWithRed:25.0/255.0 green:38.0/255.0 blue:91.0/255.0 alpha:1],
                                                 [UIColor colorWithRed:25.0/255.0 green:38.0/255.0 blue:91.0/255.0 alpha:1],
                                                 [UIColor colorWithRed:25.0/255.0 green:38.0/255.0 blue:91.0/255.0 alpha:1],
                                                 [UIColor colorWithRed:25.0/255.0 green:38.0/255.0 blue:91.0/255.0 alpha:1],
                                                 [UIColor colorWithRed:25.0/255.0 green:38.0/255.0 blue:91.0/255.0 alpha:1]]];
}



-(void) DismisSelfClass{
    [self dismissViewControllerAnimated:NO completion:nil];
}
*/
@end
