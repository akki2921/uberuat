//
//  DropdownTableViewCell.h
//  Uber Reward
//
//  Created by Arvind Seth on 06/03/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DropdownTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *img_flag;
@property (strong, nonatomic) IBOutlet UILabel *lbl_code;

@end
