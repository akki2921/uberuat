//
//  DropdownTableViewCell.m
//  Uber Reward
//
//  Created by Arvind Seth on 06/03/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import "DropdownTableViewCell.h"

@implementation DropdownTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
