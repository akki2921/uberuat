//
//  MenuTableViewCell.h
//  Kiseki
//
//  Created by 9Dim on 01/06/17.
//  Copyright © 2017 9Dim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *img_title;
@property (strong, nonatomic) IBOutlet UILabel *lbl_title;
@property (strong, nonatomic) IBOutlet UILabel *lbl_line;

@end
