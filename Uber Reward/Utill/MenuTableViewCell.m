//
//  MenuTableViewCell.m
//  Kiseki
//
//  Created by 9Dim on 01/06/17.
//  Copyright © 2017 9Dim. All rights reserved.
//

#import "MenuTableViewCell.h"

@implementation MenuTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
