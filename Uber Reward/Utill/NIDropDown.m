//
//  NIDropDown.m
//  NIDropDown
//
//  Created by Bijesh N on 12/28/12.
//  Copyright (c) 2012 Nitor Infotech. All rights reserved.
//

#import "NIDropDown.h"
#import "QuartzCore/QuartzCore.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "DropdownTableViewCell.h"


@interface NIDropDown ()
@property(nonatomic, strong) UITableView *table;
@property(nonatomic, strong) UIButton *btnSender;
@property(nonatomic, retain) NSArray *list;
@property(nonatomic, retain) NSArray *imageList;
@end

@implementation NIDropDown
@synthesize table;
@synthesize btnSender;
@synthesize list;
@synthesize imageList;
@synthesize delegate;
@synthesize animationDirection;
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)viewDidLayoutSubviews
{
  
    
    if ([table respondsToSelector:@selector(setSeparatorInset:)]) {
        [table setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([table respondsToSelector:@selector(setLayoutMargins:)]) {
        [table setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (id)showDropDown:(UIButton *)b :(CGFloat *)height :(NSArray *)arr :(NSArray *)imgArr :(NSString *)direction {
    
    [[NSUserDefaults standardUserDefaults] setObject:NSStringFromCGRect(b.frame) forKey:@"BtnFrameDrop"];
    
   
    
    btnSender = b;
    animationDirection = direction;
    self.table = (UITableView *)[super init];
    if (self) {
        // Initialization code
        CGRect btn = b.frame;
        self.list = [NSArray arrayWithArray:arr];
        self.imageList = [NSArray arrayWithArray:imgArr];
        if ([direction isEqualToString:@"up"]) {
            self.frame = CGRectMake(btn.origin.x, btn.origin.y, btn.size.width, 0);
            self.layer.shadowOffset = CGSizeMake(-5, -5);
        }else if ([direction isEqualToString:@"down"]) {
            self.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height, btn.size.width, 0);
            self.layer.shadowOffset = CGSizeMake(-5, 5);
        }
        
        self.layer.masksToBounds = NO;
        self.layer.cornerRadius = 8;
        self.layer.shadowRadius = 5;
        self.layer.shadowOpacity = 0.5;
        
        table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, btn.size.width, 0)];
        table.delegate = self;
        table.dataSource = self;
        table.layer.cornerRadius = 5;
        table.backgroundColor = [UIColor clearColor];//[UIColor colorWithRed:0.239 green:0.239 blue:0.239 alpha:1];
        table.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        table.separatorColor = [UIColor grayColor];
        
        table.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
         [table registerNib:[UINib nibWithNibName:@"DropdownTableViewCell" bundle:nil] forCellReuseIdentifier:@"DropdownTableViewCell"];
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        if ([direction isEqualToString:@"up"]) {
            self.frame = CGRectMake(btn.origin.x, btn.origin.y-*height, btn.size.width, *height);
        } else if([direction isEqualToString:@"down"]) {
            self.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height, btn.size.width, *height);
        }
        table.frame = CGRectMake(0, 0, btn.size.width, *height);
        [UIView commitAnimations];
        
        [b.superview addSubview:self];
        [self addSubview:table];
    }
    return self;
}

-(void)hideDropDown:(UIButton *)b {
    CGRect btn = b.frame;
    
    btn = CGRectFromString([[NSUserDefaults standardUserDefaults] valueForKey:@"BtnFrameDrop"]);
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    if ([animationDirection isEqualToString:@"up"]) {
        self.frame = CGRectMake(btn.origin.x, btn.origin.y, btn.size.width, 0);
    }else if ([animationDirection isEqualToString:@"down"]) {
        self.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height, btn.size.width, 0);
    }
    table.frame = CGRectMake(0, 0, btn.size.width, 0);
    [UIView commitAnimations];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    NSString *chkStr = [NSString stringWithFormat:@"%@",[list objectAtIndex:0]];
    if ([chkStr isEqualToString:@"English"] || [chkStr isEqualToString:@"الإنجليزية"]) {
        return 25;
    }
        return 40;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.list count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (self.imageList.count) {
        static NSString *CellIdentifier = @"DropdownTableViewCell";
        
        DropdownTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier forIndexPath: indexPath];
         cell.lbl_code.text =[NSString stringWithFormat:@"%@",[list objectAtIndex:indexPath.row]];
        NSString*strurl =[imageList objectAtIndex:indexPath.row];
        if (strurl.length>9) {
            [cell.img_flag sd_setImageWithURL:[NSURL URLWithString:[imageList objectAtIndex:indexPath.row]]
                             placeholderImage:[UIImage imageNamed:@"kent_default.png"]];
        }
       
        else{
            cell.img_flag.image= [UIImage imageNamed:@"close.png"];
        }
        return cell;
        
    }
    else{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.textLabel.font = [UIFont fontWithName:@"ClanPro-Book" size:12.9];
        cell.textLabel.textAlignment =  NSTextAlignmentCenter;
        
        cell.textLabel.numberOfLines=2;
      
      
    }
   

    
    
//    if ([self.imageList count] == [self.list count]) {
     cell.textLabel.text =[NSString stringWithFormat:@"%@",[list objectAtIndex:indexPath.row]];

    cell.textLabel.textColor = [UIColor blackColor];
    UIFont *myFont = [UIFont fontWithName:@"ClanPro-Book" size:12.9];
        cell.textLabel.font  = myFont;
    
    UIView * v = [[UIView alloc] init];
    v.backgroundColor = [UIColor grayColor];
    cell.selectedBackgroundView = v;
    
    return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *tempStr = [NSString stringWithFormat:@"%@",[list objectAtIndex:indexPath.row]];
    
    if ([tempStr integerValue]) {
        NSString*str= [[NSUserDefaults standardUserDefaults] objectForKey:@"applanguage"];
        if ([str isEqualToString:@"English"]) {
            tempStr = [NSString stringWithFormat:@"+%@",[list objectAtIndex:indexPath.row]];
        }
        else if([str isEqualToString:@"Arabic"]) {
        
            
            if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"dob"] isEqualToString:@"yes"]) {
                tempStr = [NSString stringWithFormat:@"+%@",[list objectAtIndex:indexPath.row]];
            }else{
            tempStr = [NSString stringWithFormat:@"%@+",[list objectAtIndex:indexPath.row]];
            }
        }
    } else{
        tempStr = [NSString stringWithFormat:@"%@",[list objectAtIndex:indexPath.row]];
    }
    
    [btnSender setTitle:tempStr forState:UIControlStateNormal];
    strBtnText = [NSString stringWithFormat:@"%@,%@",tempStr,[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
    
    //    UITableViewCell *c = [tableView cellForRowAtIndexPath:indexPath];
    //    [btnSender setTitle:c.textLabel.text forState:UIControlStateNormal];
    //    NSLog(@"btnsender setttitle:  %@",c.textLabel.text);
    //
    //    strBtnText = [NSString stringWithFormat:@"%@,%@",c.textLabel.text,[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
    
    [self hideDropDown:btnSender];
    [self myDelegate];
}

- (void) myDelegate {
    
    NSString*btntag= [NSString stringWithFormat:@"%ld",(long)btnSender.tag];
    
    [self.delegate niDropDownDelegateMethod:strBtnText withbtntag:btntag dropdown: self];
}

-(void)dealloc {
    //    [super dealloc];
    //    [table release];
    //    [self release];
}

@end

