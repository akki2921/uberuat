//
//  SlidingViewController.h
//  Kiseki
//
//  Created by 9Dim on 01/06/17.
//  Copyright © 2017 9Dim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuTableViewCell.h"
#import "NIDropDown.h"
#import "UtillClass.h"
#import "AppDelegate.h"
#import "noLOginWebViewViewController.h"

@class SlidingViewController;

@protocol slideViewControllerDelegate
- (void) slideViewControllerDone: (SlidingViewController *) sender;

@end

@interface SlidingViewController : UIViewController<NIDropDownDelegate>
{
    IBOutlet UILabel *lbl_mob;
    NSMutableArray *array_Menu;
    NSDictionary *ProfiledataDic;
    IBOutlet UITableView *tableView_Menu;
    IBOutlet UILabel *Lbl_name;
    IBOutlet UILabel *lbl_email;
    IBOutlet UIButton *btn_profile;
     NIDropDown *dropDown;
    NSMutableArray*arr_custlist;
    NSMutableArray*arr_custcode;
}
@property (strong, nonatomic) IBOutlet UIImageView *img_tblcorres;
@property (strong, nonatomic) IBOutlet UIButton *btn_customer;
@property (weak, nonatomic) IBOutlet UIImageView *img_profile_bg;
@property (nonatomic, strong) NSString *Str_isLogIn; 

@property (weak, nonatomic) IBOutlet UILabel *lbl_headername;

@property (nonatomic, retain) id <slideViewControllerDelegate> delegate;
- (IBAction)Action_Home:(id)sender;

@end
