//
//  SlidingViewController.m
//  Kiseki
//
//  Created by 9Dim on 01/06/17.
//  Copyright © 2017 9Dim. All rights reserved.
//

#import "SlidingViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface SlidingViewController ()

@end

@implementation SlidingViewController
@synthesize Str_isLogIn;

- (void)viewDidLoad {
   
    
    [super viewDidLoad];
    _img_profile_bg.clipsToBounds =YES;
    _img_profile_bg.layer.cornerRadius = _img_profile_bg.frame.size.height/2;
    
    
    NSString*str= [[NSUserDefaults standardUserDefaults] objectForKey:@"applanguage"];
    if ([str isEqualToString:@"English"]) {
        
         if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"Userloginornot"] isEqualToString:@"Loggedin"]) {
            array_Menu = [[NSMutableArray alloc] initWithObjects:
                          @{@"title" : @"About Uber PLATINUM",
                            @"image" : @"installation.png"},
                          @{@"title" : @"Terms & Conditions",
                            @"image" : @"referal_icon.png"},
                          @{@"title" : @"FAQs",
                            @"image" : @"kentpart.png"},
                          nil];
        } else {
            array_Menu = [[NSMutableArray alloc] initWithObjects:
                          @{@"title" : @"Home",
                            @"image" : @"ic_profileicon.png"},
                          @{@"title" : @"About Uber PLATINUM",
                            @"image" : @"installation.png"},
                          @{@"title" : @"Account Summary",
                            @"image" : @"service.png"},
                          @{@"title" : @"Contact Us",
                            @"image" : @"service_history.png"},
                          @{@"title" : @"Account Settings",
                            @"image" : @"promotion.png"},
                          @{@"title" : @"FAQs",
                            @"image" : @"kentpart.png"},
                          @{@"title" : @"Terms & Conditions",
                            @"image" : @"referal_icon.png"},
                          @{@"title" : @"Notifications",
                            @"image" : @"referal_icon.png"},
                          @{@"title" : @"Logout",
                            @"image" : @"customer_care.png"},
                          nil];
        }
    }
    else{
        
        if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"Userloginornot"] isEqualToString:@"Loggedin"]) {
            array_Menu = [[NSMutableArray alloc] initWithObjects:
                          @{@"title" : @"معلومات عن أوبر بلاتينيوم",
                            @"image" : @"installation.png"},
                          @{@"title" : @"الشروط والأحكام",
                            @"image" : @"referal_icon.png"},
                          @{@"title" : @"الأسئلة الشائعة",
                            @"image" : @"kentpart.png"},
                          nil];
        } else {
            array_Menu = [[NSMutableArray alloc] initWithObjects:
                          @{@"title" : @"الصفحة الرئيسية",
                            @"image" : @"ic_profileicon.png"},
                          @{@"title" : @"معلومات عن أوبر بلاتينيوم",
                            @"image" : @"installation.png"},
                          @{@"title" : @"ملخّص الحساب",
                            @"image" : @"service.png"},
                          @{@"title" : @"اتصل بنا",
                            @"image" : @"service_history.png"},
                          @{@"title" : @"إعدادت الحساب",
                            @"image" : @"promotion.png"},
                          @{@"title" : @"الأسئلة الشائعة",
                            @"image" : @"kentpart.png"},
                          @{@"title" : @"الشروط والأحكام",
                            @"image" : @"referal_icon.png"},
                          @{@"title" : @"التنبيهات",
                            @"image" : @"customer_care.png"},
                          @{@"title" : @"تسجيل الخروج",
                            @"image" : @"customer_care.png"},
                          nil];
        }
   
    }
    
    [tableView_Menu registerNib:[UINib nibWithNibName:@"MenuTableViewCell" bundle:nil] forCellReuseIdentifier:@"MenuTableViewCell"];
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipe:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    
    [tableView_Menu setFrame:CGRectMake(tableView_Menu.frame.origin.x, tableView_Menu.frame.origin.y, tableView_Menu.frame.size.width, array_Menu.count*38)];
    
    if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
        
        if ([UIScreen mainScreen].bounds.size.height==568) {
            [tableView_Menu setFrame:CGRectMake(tableView_Menu.frame.origin.x, tableView_Menu.frame.origin.y, tableView_Menu.frame.size.width, array_Menu.count*38+50)];
        }
        else{
        [tableView_Menu setFrame:CGRectMake(tableView_Menu.frame.origin.x, tableView_Menu.frame.origin.y, tableView_Menu.frame.size.width, array_Menu.count*38)];
        
    }
    }
    else{
        
        if ([UIScreen mainScreen].bounds.size.height==568) {
        [tableView_Menu setFrame:CGRectMake(0, tableView_Menu.frame.origin.y, tableView_Menu.frame.size.width, array_Menu.count*38+50)];
        [_img_tblcorres setFrame:CGRectMake(0, _img_tblcorres.frame.origin.y, _img_tblcorres.frame.size.width, _img_tblcorres.frame.size.height)];
        }
        
        else{
            
            [tableView_Menu setFrame:CGRectMake(0, tableView_Menu.frame.origin.y, tableView_Menu.frame.size.width, array_Menu.count*38)];
         [_img_tblcorres setFrame:CGRectMake(0, _img_tblcorres.frame.origin.y, _img_tblcorres.frame.size.width, _img_tblcorres.frame.size.height)];
        }
        
    }
    
   if ([UIScreen mainScreen].bounds.size.height==812) {
          [tableView_Menu setFrame:CGRectMake(tableView_Menu.frame.origin.x, tableView_Menu.frame.origin.y, tableView_Menu.frame.size.width, array_Menu.count*38)];
     
   }
    [tableView_Menu reloadData];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 38;
}
-(void)viewDidAppear:(BOOL)animated{
    

}
-(void)viewWillAppear:(BOOL)animated{
    
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.delegate slideViewControllerDone:self];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didSwipe:(UISwipeGestureRecognizer*)swipe{
    
    if (swipe.direction == UISwipeGestureRecognizerDirectionLeft) {
        [self.delegate slideViewControllerDone:self];
    }
    
}
-(IBAction)close:(id)sender{
    
     [self.delegate slideViewControllerDone:self];
    
}
- (UIImage *)imageNamed:(NSString *)name withColor:(UIColor *)color
{
    // load the image
    //NSString *name = @"badge.png";
    UIImage *img = [UIImage imageNamed:name];
    
    // begin a new image context, to draw our colored image onto
    UIGraphicsBeginImageContext(img.size);
    
    // get a reference to that context we created
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // set the fill color
    [color setFill];
    
    // translate/flip the graphics context (for transforming from CG* coords to UI* coords
    CGContextTranslateCTM(context, 0, img.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    
    // set the blend mode to color burn, and the original image
    CGContextSetBlendMode(context, kCGBlendModeColorBurn);
    CGRect rect = CGRectMake(0, 0, img.size.width, img.size.height);
    CGContextDrawImage(context, rect, img.CGImage);
    
    // set a mask that matches the shape of the image, then draw (color burn) a colored rectangle
    CGContextClipToMask(context, rect, img.CGImage);
    CGContextAddRect(context, rect);
    CGContextDrawPath(context,kCGPathFill);
    
    // generate a new UIImage from the graphics context we drew onto
    UIImage *coloredImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //return the color-burned image
    return coloredImg;
}

#pragma mark ##### Table view delegate and data source ###



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1 ;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return array_Menu.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"MenuTableViewCell";
    
    MenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier forIndexPath: indexPath];
    
    cell.lbl_title.text = [NSString stringWithFormat:@"%@",[[array_Menu objectAtIndex:indexPath.row] valueForKey:@"title"]];
    
    NSString*str= [[NSUserDefaults standardUserDefaults] objectForKey:@"applanguage"];
    if ([str isEqualToString:@"English"]) {
        cell.lbl_title.textAlignment = NSTextAlignmentLeft;
    }
    else{
        cell.lbl_title.textAlignment = NSTextAlignmentRight;
    }
    
    UIImage *image= [UIImage imageNamed:[NSString stringWithFormat:@"%@",[[array_Menu objectAtIndex:indexPath.row] valueForKey:@"image"]]];
    cell.img_title.image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.img_title setTintColor:[UIColor darkGrayColor]];
    
    
   // cell.img_title.image =[self imageNamed:[NSString stringWithFormat:@"%@",[[array_Menu objectAtIndex:indexPath.row] valueForKey:@"image"]] withColor:[UIColor blueColor]];
    if (indexPath.row== [array_Menu count]-1) {
        cell.lbl_line.hidden=YES;
        
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
   
   if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"Userloginornot"] isEqualToString:@"Loggedin"]) {
        if (indexPath.row==0) {
            app.ViewWebTypeStr = @"About";

        }
        if (indexPath.row==2) {
            
            app.ViewWebTypeStr = @"FAQs";
          
        }
        if (indexPath.row==1) {
            
            app.ViewWebTypeStr = @"T&C";

        }
        noLOginWebViewViewController *controller = [[noLOginWebViewViewController alloc] initWithNibName:[[UtillClass sharedInstance] getnologinxib] bundle:nil];
         [self presentViewController:controller animated:false completion:nil];
        
    } else {
        
        if (indexPath.row==7) {
            app.ViewWebTypeStr=@"";
            UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"NotiViewController"];
            [self presentViewController:vc animated:false completion:nil];
        }
        if (indexPath.row==0) {
             app.ViewWebTypeStr=@"";
            UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
            [self presentViewController:vc animated:false completion:nil];
        }
        if (indexPath.row==2) {
            app.ViewWebTypeStr=@"";
            UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"ActivityViewController"];
            [self presentViewController:vc animated:false completion:nil];
        }
        if (indexPath.row==1) {
            app.ViewWebTypeStr = @"About";
            UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"FaqViewController"];
            [self presentViewController:vc animated:false completion:nil];
        }
        if (indexPath.row==3) {
             app.ViewWebTypeStr=@"";
            UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"ContactUsViewController"];
            [self presentViewController:vc animated:false completion:nil];
            
        }
        if (indexPath.row==4) {
             app.ViewWebTypeStr=@"";
            UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"AccSettingViewController"];
            [self presentViewController:vc animated:false completion:nil];
        }
        if (indexPath.row==5) {
            
            app.ViewWebTypeStr = @"FAQs";
            UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"FaqViewController"];
            [self presentViewController:vc animated:false completion:nil];
            
        }
        if (indexPath.row==6) {
            
            app.ViewWebTypeStr = @"T&C";
            UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"FaqViewController"];//@"T&CViewController"];
            [self presentViewController:vc animated:false completion:nil];
        }
        if (indexPath.row==8) {
             app.ViewWebTypeStr=@"";
            NSString *titleStr, *msgStr, *btn1Title, *btn2Title;
            NSString*str= [[NSUserDefaults standardUserDefaults] objectForKey:@"applanguage"];
            if ([str isEqualToString:@"English"]) {
                titleStr = @"Log out";
                msgStr = @"Do you really want to log out?";
                btn1Title = @"Ok";
                btn2Title = @"Cancel";
            }
            else{
                titleStr = @"الخروج";
                msgStr = @"هل تريد حقًا تسجيل الخروج؟";
                btn1Title = @"حسنا";
                btn2Title = @"إلغاء";
            }
            NSString *strAppNoti= [[NSUserDefaults standardUserDefaults] objectForKey:@"notificationid"];
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:titleStr
                                         message:msgStr
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:btn1Title
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                                            
                                            NSString*str= [[NSUserDefaults standardUserDefaults] objectForKey:@"applanguage"];
                                            NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
                                            [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
                                            
                                            [[NSUserDefaults standardUserDefaults] setObject:strAppNoti forKey:@"notificationid"];
                                            if ([str isEqualToString:@"English"]) {
                                                [[NSUserDefaults standardUserDefaults] setObject:@"English" forKey:@"applanguage"];
                                            } else {
                                                [[NSUserDefaults standardUserDefaults] setObject:@"Arabic" forKey:@"applanguage"];
                                            }
                                            
                                            UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance] getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"LoginViewController"];
                                            [self presentViewController:vc animated:false completion:nil];
                                        }];
            UIAlertAction* noButton = [UIAlertAction
                                       actionWithTitle:btn2Title
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           //Handle no, thanks button
                                       }];
            
            
            if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
                [alert addAction:yesButton];
                [alert addAction:noButton];
            }else{
                [alert addAction:noButton];
                [alert addAction:yesButton];
                
            }
          
            [self presentViewController:alert animated:YES completion:nil];
            
            
        }
    }
    
    
}
- (IBAction)Action_Home:(id)sender {
    
    [self.delegate slideViewControllerDone:self];
    
    CATransition *transition = [[CATransition alloc] init];
    transition.duration = 0.5;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:kCATransition];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil];
    UIViewController * vc = [sb instantiateViewControllerWithIdentifier:@"HomeViewController"];
    [self presentViewController:vc animated:NO completion:nil];
}
-(IBAction)btn_myprofView:(id)sender{
  
}

- (IBAction)action_show_custList:(id)sender {
 
    [self OpenDropDown:sender Array:[arr_custlist copy]] ;
    
}

#pragma mark - DropDown Delegate

- (void)OpenDropDown:(UIButton *)sender Array:(NSArray *) arr
{
    
    [self.view endEditing:YES];
    
    NSArray * arrImage = [[NSArray alloc] init];
    
    if(dropDown == nil) {
        CGFloat f = 200;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :arrImage :@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        dropDown = nil;
    }
}
- (void) niDropDownDelegateMethod: (NSString *)btntext withbtntag:(NSString *)btntag dropdown: (NIDropDown *) sender {
    dropDown = nil;
     _lbl_headername.text=@"";
    [_btn_customer setTitle:[NSString stringWithFormat:@"%@", [[btntext componentsSeparatedByString:@","]objectAtIndex:0]] forState:UIControlStateNormal] ;
   
}



@end
