//
//  UtillClass.h
//  Uber Reward
//
//  Created by Arvind Seth on 22/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SlidingViewController.h"
#import "MBProgressHUD.h"
#import "AbhiLoaderView.h"
#import "AbhiTxtFeildValidation.h"
#import "CustLabel.h"
@class SlidingViewController;

typedef void(^service_response)(NSDictionary *);

@interface UtillClass : NSObject<AbhiTxtFeildValidationDelegate>
{
    AbhiLoaderView* loaderActView;
   UIWindow* window;
    UIView *view_bottom;
    UIButton *btn_spend;
    UIButton *btn_privilege;
    UIButton *btn_walloffame;
    CustLabel *tostLbl;
    AbhiTxtFeildValidation *abhiValidation;
}
-(NSString *)getServiceURL;
-(NSString *)getwebviewURL;
+ (instancetype)sharedInstance;
@property(nonatomic, strong) UIWindow *window;
-(NSString *) getnologinxib;
-(void) ShowTfErrorMsg:(NSString*)text onTf:(UITextField*)tf;

-(void) HideTfErrorMsg;

-(void) AbhiTxtFeildValidationDelegateMethod:(AbhiTxtFeildValidation *)sender;


-(NSString *)getpointformatofintArb:(int)point;
-(NSString *)getpointformatofint:(int)point;
//-(NSString *)getDeviceID;
-(NSString *) getdeviceid;
-(void) hidebottom_items;
-(NSString*) getnotiid;
-(void)set_app_sessiontoken:(NSString*)sessiontoken;
-(NSString *)getapp_sessiontoken;
-(void) ShowLoaderOn:(UIViewController*)vc withtext:(NSString *)text;
-(void) HideLoader;
-(SlidingViewController *) CallSlideMenuView:(UIViewController *) myVC yVal:(float) yVal;
+ (void)dismissGlobalHUD;
+ (MBProgressHUD *)showGlobalProgressHUDWithTitle:(NSString *)title;
-(NSString *) getcultureID;
-(void)setUserdefaultofDict:(NSDictionary *)dict withKayname:(NSString *)strkey;
-(NSDictionary*)getDictUserdefaultwithKayname:(NSString *)strkey;
-(BOOL)checkNetworkReachability;
-(void)showerrortoastmsg:(NSString *)msg;
- (BOOL)validateEmailWithString:(NSString*)email;
-(void) showbottom_items;
- (BOOL) MobileNumberValidate:(NSString*)number;
-(UIView *) Setlayeronview:(UIView *)view;
-(NSString *)converted_date_string:(NSString*)strdate;
-(NSString *)removespace:(NSString *)str;
-(void)showtoastmsg:(NSString *)msg;
-(void)set_app_language:(NSString*)language;
-(NSString *)getselected_language;
-(NSString *)getstoryboardname;
-(void)setUserdefaultofArray:(NSArray *)arr withKayname:(NSString *)strkey;
-(void)Postresponsetoservice:(NSDictionary *)dict withBearer:(NSString *)bearer withurl:(NSString*)url response:(service_response) compblock;
-(void)getresponsefromservice:(NSString *)string withbearer:(NSString *)bearer response:(service_response) compblock;
@end
