//
//  UtillClass.m
//  Uber Reward
//
//  Created by Arvind Seth on 22/02/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import "UtillClass.h"
#import "AFNetworking.h"
#import "Reachability.h"
#import "MBProgressHUD.h"
@implementation UtillClass

+ (instancetype)sharedInstance
{
    static UtillClass *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[UtillClass alloc] init];
    });
    return sharedInstance;
    
}

-(NSString *)getwebviewURL{
//    return @"http://me-platinum.com/"; //Pruduction
      return @"http://mscportalbeta.me-platinum.com/"; //UAT
//                                                      //Local
}
-(NSString *)getServiceURL{
//   return @"http://services.me-platinum.com/"; //Production
     return @"http://betaservices.me-platinum.com/"; //UAT
//   return  @"http://103.18.72.141/Savola.Service/";//Local
}

+ (MBProgressHUD *)showGlobalProgressHUDWithTitle:(NSString *)title {
    UIWindow *window = [[[UIApplication sharedApplication] windows] lastObject];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:window animated:YES];
    hud.labelText = title;
    return hud;
}

+ (void)dismissGlobalHUD {
    UIWindow *window = [[[UIApplication sharedApplication] windows] lastObject];
    [MBProgressHUD hideHUDForView:window animated:YES];
}
-(SlidingViewController *) CallSlideMenuView:(UIViewController *) myVC yVal:(float) yVal

{
    SlidingViewController *slideView = [[SlidingViewController alloc] init];
    [myVC addChildViewController:slideView];
    slideView.view.frame = CGRectMake(0, -200, myVC.view.frame.size.width, myVC.view.frame.size.height - yVal);// myVC.view.frame;
    [myVC.view addSubview:slideView.view];
    [slideView didMoveToParentViewController:myVC];
    slideView.delegate = myVC;
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        //code with animation
        slideView.view.frame = CGRectMake(0, yVal, myVC.view.frame.size.width, myVC.view.frame.size.height - yVal);
    } completion:^(BOOL finished) {
        //code for completion
    }];
    
    return slideView;
}
-(void) hidebottom_items{
    
    [view_bottom removeFromSuperview];
}

-(void) showbottom_items{
    NSString*str= [[NSUserDefaults standardUserDefaults] objectForKey:@"applanguage"];
    
    window = [UIApplication sharedApplication].keyWindow;
    if (!window) {
        window = [[UIApplication sharedApplication].windows objectAtIndex:0];
    }
    if ([UIScreen mainScreen].bounds.size.height==568) {
        CGFloat height =46;
        view_bottom = [[UIView alloc]initWithFrame:CGRectMake(0, window.frame.size.height-height, window.frame.size.width, height)];
        
        btn_spend = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 87, height)];
        btn_privilege = [[UIButton alloc]initWithFrame:CGRectMake(89,  0, 141, height)];
        btn_walloffame = [[UIButton alloc]initWithFrame:CGRectMake(232, 0, 88, height)];
        if ([str isEqualToString:@"English"]) {
        }
        else{
            btn_spend = [[UIButton alloc]initWithFrame:CGRectMake(232, 0, 88, height)];
            btn_walloffame = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 87, height)];
        }
    }
    else if (([UIScreen mainScreen].bounds.size.height==667) ||([UIScreen mainScreen].bounds.size.height==812) ){
        CGFloat height =53;
        view_bottom = [[UIView alloc]initWithFrame:CGRectMake(0, window.frame.size.height-height, window.frame.size.width, height)];
        
        btn_spend = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 102, height)];
        btn_privilege = [[UIButton alloc]initWithFrame:CGRectMake(104,  0, 166, height)];
        btn_walloffame = [[UIButton alloc]initWithFrame:CGRectMake(272, 0, 103, height)];
        if ([str isEqualToString:@"English"]) {
        }
        else{
            btn_spend = [[UIButton alloc]initWithFrame:CGRectMake(272, 0, 103, height)];
            btn_walloffame = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 102, height)];
        }
    }
    else if ([UIScreen mainScreen].bounds.size.height==736){
        CGFloat height =58;
        view_bottom = [[UIView alloc]initWithFrame:CGRectMake(0, window.frame.size.height-height, window.frame.size.width, height)];
        
        btn_spend = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 113, height)];
        btn_privilege = [[UIButton alloc]initWithFrame:CGRectMake(115,  0, 183, height)];
        btn_walloffame = [[UIButton alloc]initWithFrame:CGRectMake(300, 0, 114, height)];
        if ([str isEqualToString:@"English"]) {
        }
        else{
            btn_spend = [[UIButton alloc]initWithFrame:CGRectMake(300, 0, 114, height)];
            btn_walloffame = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 113, height)];
        }
    }
    
    
    [view_bottom setBackgroundColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1]];
    
    [btn_spend setBackgroundColor:[UIColor colorWithRed:197/255.0f green:21/255.0f blue:74/255.0f alpha:1]];
    [btn_privilege setBackgroundColor:[UIColor colorWithRed:197/255.0f green:21/255.0f blue:74/255.0f alpha:1]];
    [btn_walloffame setBackgroundColor:[UIColor colorWithRed:197/255.0f green:21/255.0f blue:74/255.0f alpha:1]];
    
    btn_spend.tag=1;
    btn_privilege.tag=2;
    btn_walloffame.tag=3;
    
    
    if ([str isEqualToString:@"English"]) {
        [btn_spend setTitle:@"Spend Points" forState:UIControlStateNormal];
        [btn_privilege setTitle:@"Privileges & Offers" forState:UIControlStateNormal];
        [btn_walloffame setTitle:@"Wall of Fame" forState:UIControlStateNormal];
    }
    else{
        [btn_spend setTitle:@"استبدال النقاط" forState:UIControlStateNormal];
        [btn_privilege setTitle:@"امتياز وعروض" forState:UIControlStateNormal];
        [btn_walloffame setTitle:@"حائط الشهرة" forState:UIControlStateNormal];
    }
    
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    if ([app.ViewWebTypeStr isEqualToString:@"Privilage"]) {
        [btn_privilege setBackgroundColor:[UIColor colorWithRed:164.0/255.0f green:164.0/255.0f blue:164.0/255.0f alpha:1]];
    } else if ([app.ViewWebTypeStr isEqualToString:@"sped"]) {
        [btn_spend setBackgroundColor:[UIColor colorWithRed:164.0/255.0f green:164.0/255.0f blue:164.0/255.0f alpha:1]];
    } else if ([app.ViewWebTypeStr isEqualToString:@"Walloffame"]) {
        [btn_walloffame setBackgroundColor:[UIColor colorWithRed:164.0/255.0f green:164.0/255.0f blue:164.0/255.0f alpha:1]];
    }
    
    btn_spend.titleLabel.textColor=[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1];
    btn_privilege.titleLabel.textColor=[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1];
    btn_walloffame.titleLabel.textColor=[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1];
    
    
    btn_spend.titleLabel.font = [UIFont fontWithName:@"ClanPro-Book" size:10.6];
    btn_privilege.titleLabel.font = [UIFont fontWithName:@"ClanPro-Book" size:10.6];
    btn_walloffame.titleLabel.font = [UIFont fontWithName:@"ClanPro-Book" size:10.6];
    
    [btn_spend addTarget:self action:@selector(select_btn_spend:) forControlEvents:UIControlEventTouchUpInside];
    [btn_privilege addTarget:self action:@selector(select_btn_privilege:) forControlEvents:UIControlEventTouchUpInside];
    [btn_walloffame addTarget:self action:@selector(select_btn_walloffame:) forControlEvents:UIControlEventTouchUpInside];
    
    [view_bottom addSubview:btn_spend];
    [view_bottom addSubview:btn_privilege];
    [view_bottom addSubview:btn_walloffame];
    [window addSubview:view_bottom];
}


-(void)select_btn_spend:(UIButton*)sender{
    NSLog(@"spend");
    
    self.window = [[UIWindow alloc]initWithFrame:[[UIScreen mainScreen] bounds]];
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    app.ViewWebTypeStr= @"sped";
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"SpedPontViewController"];
    self.window.rootViewController =vc;
    
    [self.window makeKeyAndVisible];
}
-(void)select_btn_privilege:(UIButton*)sender{
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    app.ViewWebTypeStr= @"Privilage";
    self.window = [[UIWindow alloc]initWithFrame:[[UIScreen mainScreen] bounds]];
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"PrivilageViewController"];
    self.window.rootViewController =vc;
    
    [self.window makeKeyAndVisible];
    
}
-(void)select_btn_walloffame:(UIButton*)sender{
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    app.ViewWebTypeStr= @"Walloffame";
    self.window = [[UIWindow alloc]initWithFrame:[[UIScreen mainScreen] bounds]];
    UIViewController * vc = [[UIStoryboard storyboardWithName:[[UtillClass sharedInstance]getstoryboardname] bundle:nil] instantiateViewControllerWithIdentifier:@"WalloffameViewController"];
    self.window.rootViewController =vc;
    
    [self.window makeKeyAndVisible];
}

- (BOOL) MobileNumberValidate:(NSString*)number
{
    NSString *numberRegEx = @"[0-9]";
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegEx];
    
    return [numberTest evaluateWithObject:number];
}
-(UIView *) Setlayeronview:(UIView *)view{

    view.clipsToBounds=YES;
    view.layer.cornerRadius=2.0f;
    view.layer.borderColor=[UIColor colorWithRed:217.0f/255.0f green:215.0f/255.0f blue:217.0f/255.0f alpha:1].CGColor;
    view.layer.borderWidth=2.0f;
    
    return view;
}

-(NSString *)converted_date_string:(NSString*)strdate{
    
    NSDateFormatter *formate = [[NSDateFormatter alloc] init];
    NSDate *date;
    if (strdate.length<12) {
        [formate setDateFormat:@"dd-MM-yyyy"];
        
        
        date   = [formate dateFromString:strdate];
    }
    else{
        
        [formate setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
        date = [formate dateFromString:strdate];
    }
    
    
    [formate setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *str =[formate stringFromDate:date];
    return str;
}
-(NSString *)removespace:(NSString *)str{
    
    NSString*strtemp=[str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    return strtemp;
    
}

-(void)showtoastmsg:(NSString *)msg{
    
    window = [UIApplication sharedApplication].keyWindow;
    
    if (!window) {
        window = [[UIApplication sharedApplication].windows objectAtIndex:0];
    }
    [[UtillClass sharedInstance] HideLoader];
    [tostLbl removeFromSuperview];
    
   tostLbl = [[CustLabel alloc] initWithFrame:CGRectMake(5, window.frame.size.height-65, window.frame.size.width-10, 60)];
    tostLbl.backgroundColor = [UIColor whiteColor];
    tostLbl.alpha = 0.9;
    tostLbl.textColor = [UIColor colorWithRed:52.0f/255.0f green:108.0f/255.0f blue:54.0f/255.0f alpha:1];
   tostLbl.layer.borderWidth=1;
    tostLbl.layer.borderColor=[UIColor darkGrayColor].CGColor;
    tostLbl.text = msg;
    tostLbl.textAlignment = NSTextAlignmentCenter;
    tostLbl.layer.cornerRadius = 8;
    tostLbl.clipsToBounds = YES;
    tostLbl.numberOfLines=3;
    [tostLbl setFont:[UIFont fontWithName:@"ClanPro-NarrNews" size:10.1]];
    [window addSubview:tostLbl];
    if (@available(iOS 10.0, *)) {
        [NSTimer scheduledTimerWithTimeInterval:3.0 repeats:NO block:^(NSTimer * _Nonnull timer) {
            [tostLbl removeFromSuperview];
        }];
    } else {
        [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(hidemsg) userInfo:nil repeats:NO];
        
    }
    
}
-(void)showerrortoastmsg:(NSString *)msg{
    
    window = [UIApplication sharedApplication].keyWindow;
    
    if (!window) {
        window = [[UIApplication sharedApplication].windows objectAtIndex:0];
    }
    [[UtillClass sharedInstance] HideLoader];
    [tostLbl removeFromSuperview];
    tostLbl = [[CustLabel alloc] initWithFrame:CGRectMake(5, window.frame.size.height-65, window.frame.size.width-10, 60)];
    tostLbl.backgroundColor = [UIColor whiteColor];
    tostLbl.alpha = 0.9;
    tostLbl.textColor = [UIColor redColor];
    tostLbl.layer.borderWidth=1;
    tostLbl.layer.borderColor=[UIColor darkGrayColor].CGColor;
    tostLbl.text = msg;
    tostLbl.textAlignment = NSTextAlignmentCenter;
    tostLbl.layer.cornerRadius = 8;
    tostLbl.clipsToBounds = YES;
    tostLbl.numberOfLines=3;
    [tostLbl setFont:[UIFont fontWithName:@"ClanPro-NarrNews" size:10.1]];
    [window addSubview:tostLbl];
    if (@available(iOS 10.0, *)) {
        [NSTimer scheduledTimerWithTimeInterval:3.0 repeats:NO block:^(NSTimer * _Nonnull timer) {
            [tostLbl removeFromSuperview];
        }];
    } else {
        [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(hidemsg) userInfo:nil repeats:NO];
        
    }
    
}

-(void)hidemsg{
    
[tostLbl removeFromSuperview];
}
-(void)set_app_language:(NSString*)language{
    
    [[NSUserDefaults standardUserDefaults]setValue:language forKey:@"applanguage"];
    
}
-(NSString *)getselected_language{
    
    NSString*str= [[NSUserDefaults standardUserDefaults] objectForKey:@"applanguage"];
    return str;
}
-(void)set_app_sessiontoken:(NSString*)sessiontoken{
    
    [[NSUserDefaults standardUserDefaults]setValue:sessiontoken forKey:@"appsessiontoken"];
    
}
-(NSString *)getapp_sessiontoken{
    
    NSString*str= [[NSUserDefaults standardUserDefaults] objectForKey:@"appsessiontoken"];
    return str;
}
-(NSString *)getstoryboardname{
    
    NSString*str= [[NSUserDefaults standardUserDefaults] objectForKey:@"applanguage"];
    if ([str isEqualToString:@"English"]) {
        str =@"Main";
        }
    else{
        str= @"mainarabic";
    }
    return str;
}

-(void)setUserdefaultofDict:(NSDictionary *)dict withKayname:(NSString *)strkey{
    
    [[NSUserDefaults standardUserDefaults] setObject: [NSKeyedArchiver archivedDataWithRootObject:dict] forKey:strkey];
    
}
-(NSDictionary*)getDictUserdefaultwithKayname:(NSString *)strkey{
    NSDictionary *archiveddict = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:strkey]] ;
    return archiveddict;
    
}

-(NSString *) getcultureID{
    
    if ([[self getstoryboardname] isEqualToString:@"Main"]) {
        return @"1033";
    }
    else{
        
        return @"1025";
    }
}


-(NSString *) getnologinxib{
    
    if ([[self getstoryboardname] isEqualToString:@"Main"]) {
        return @"noLOginWebViewViewController";
    }
    else{
        
        return @"noLOginWebViewViewControllerArabic";
    }
}

-(NSString*) getnotiid{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString*str =[defaults objectForKey:@"notificationid"];
    if (str.length) {
         [defaults synchronize];
        return [NSString stringWithFormat:@"^%@",str];
    }
   [defaults synchronize];
    return @"^7832e3fcc63e8753f1a110c2a57d0eb08f4100fd6f97aa317f220da6457cdf0e" ;
   
    
}
-(NSString *) getdeviceid{
   
   return @"9fac4f95a2d418e";
}
-(void)getresponsefromservice:(NSString *)string withbearer:(NSString *)bearer response:(service_response) compblock{
    
    NSString *strurl=[[NSString stringWithFormat:@"%@api/%@",[self getServiceURL],string] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    NSURL *url = [NSURL URLWithString:strurl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    if (bearer.length) {
        [request setValue:[NSString stringWithFormat:@"Bearer %@ %@ 1",bearer,[self getdeviceid]] forHTTPHeaderField:@"Authorization"];
    }
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSMutableIndexSet* codes = [[NSMutableIndexSet alloc] init];
    [codes addIndex: 200];
    [codes addIndex: 400];
  
    operation.responseSerializer.acceptableStatusCodes=codes;
   
    
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        id json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        
        compblock(json);
        NSLog(@"JSON------4%@",json);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    
        id json= nil;
        compblock(json);
         NSLog(@"JSON------4%@",error.userInfo);
        
    }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
}

#pragma mark --==-=-=-=-==-= Loader Methods  -=-=-=-=-=-=-=-=-=-==-=-=-=-----

-(void) ShowLoaderOn:(UIViewController*)vc withtext:(NSString *)text{
    
    window = [UIApplication sharedApplication].keyWindow;
    if (!window) {
        window = [[UIApplication sharedApplication].windows objectAtIndex:0];
    }
    loaderActView = [[AbhiLoaderView  alloc] initWithNibName:@"AbhiLoaderView" bundle:nil];
    loaderActView.view.frame = CGRectMake(0, 0, window.frame.size.width, window.frame.size.height);
    loaderActView.lbl_CenterAbhi.text = @"";
    loaderActView.lbl_text.text=text;
    [vc addChildViewController:loaderActView];
    [window addSubview:loaderActView.view];
    [loaderActView didMoveToParentViewController:vc];
    
    
    
}

-(void) HideLoader{
    if (loaderActView.view) {
        [loaderActView.view removeFromSuperview];
        loaderActView = nil;
    }
}
#pragma mark- NetworkReachability

-(BOOL)checkNetworkReachability
{
    Reachability *rm = [Reachability reachabilityWithHostName:@"www.google.com"];
    
    NetworkStatus internetStatus = [rm currentReachabilityStatus];
    
    if ((internetStatus != ReachableViaWiFi) && (internetStatus != ReachableViaWWAN))
    {
        //        UIAlertView*    aAlert = [[UIAlertView alloc] initWithTitle:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        //        [aAlert show];
        return NO;
    }
    else{
        return YES;
    }
    
}
-(void)Postresponsetoservice:(NSDictionary *)dict withBearer:(NSString *)bearer withurl:(NSString*)url response:(service_response) compblock{


    
    NSError *error;
    NSData *dataFromDict = [NSJSONSerialization dataWithJSONObject:dict
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSString*strurl= [NSString stringWithFormat:@"%@api/%@",[self getServiceURL],url];
    NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:strurl parameters:nil error:nil];
    
     req.timeoutInterval = 30;
    [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    if (bearer.length) {
         [req setValue:[NSString stringWithFormat:@"Bearer %@ %@ 1",bearer,[self getdeviceid]] forHTTPHeaderField:@"Authorization"];
    }
 
    
    [req setHTTPBody:dataFromDict];
    
    [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        if (!error) {
            NSLog(@"%@", responseObject);
            
             NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            NSString*strsess =[[httpResponse allHeaderFields] objectForKey:@"SessionToken"];
            if (strsess.length>8) {
                [[UtillClass sharedInstance] set_app_sessiontoken:[[httpResponse allHeaderFields] objectForKey:@"SessionToken"]];
                
            }
        
            
            if ([responseObject isKindOfClass:[NSDictionary class]]) {
               
                compblock(responseObject);
                NSLog(@"JSON------4%@",responseObject);
            }
            else{
          id json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
           compblock(json);
           NSLog(@"JSON------4%@",json);
            }
            
        } else {
            id json= nil;
            compblock(json);
            NSLog(@"Error: %@, %@, %@", error, response, responseObject);
        }
    }] resume];
    
}
//-(NSString *)getDeviceID{
//    NSString* Identifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
//    return Identifier;
//}
- (BOOL)validateEmailWithString:(NSString*)email
{
    
  email=  [email stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
-(NSString *)getpointformatofint:(int)point{
    NSString *str = [NSString stringWithFormat:@"%i",point];
    
    if (![[self getcultureID] isEqualToString:@"1033"]) {
        NSNumberFormatter *formatter = [NSNumberFormatter new];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        formatter.locale = [NSLocale localeWithLocaleIdentifier:@"ar"];
        for (NSInteger i = 0; i < 10; i++) {
            NSNumber *num = @(i);
            str = [str stringByReplacingOccurrencesOfString:num.stringValue withString:[formatter stringFromNumber:num]];
        }
        NSNumber *amount = [formatter numberFromString:str];
        
        return [formatter stringFromNumber:amount];
    }
    
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    NSNumber *amount = [formatter numberFromString:str];
   
  return [formatter stringFromNumber:amount];
}
-(NSString *)getpointformatofintArb:(int)point{
    NSString *str = [NSString stringWithFormat:@"%i",point];
    
   NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    NSNumber *amount = [formatter numberFromString:str];
    
    return [formatter stringFromNumber:amount];
}
-(void) ShowTfErrorMsg:(NSString*)text onTf:(UITextField*)tf{
    
    if (abhiValidation) {
        
        abhiValidation.hidden = YES;
        
        abhiValidation = nil;
        
    }
    
    
    
    abhiValidation = [[AbhiTxtFeildValidation alloc] showAbhiTxtFeildValidation:text txtFeild:tf];
    
    abhiValidation.delegate = self;
    
    
    
    
    
}

-(void) HideTfErrorMsg{
    
    if (abhiValidation) {
        
        abhiValidation.hidden = YES;
        
    }
    
    
    
}

-(void) AbhiTxtFeildValidationDelegateMethod:(AbhiTxtFeildValidation *)sender

{
    
    if (sender) {
        
        abhiValidation = nil;
        
    }
    
}
@end
