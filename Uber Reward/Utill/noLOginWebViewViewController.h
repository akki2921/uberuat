//
//  noLOginWebViewViewController.h
//  Uber Reward
//
//  Created by 9Dim on 21/03/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UtillClass.h"
@class SlidingViewController;

@interface noLOginWebViewViewController : UIViewController
{
    IBOutlet UIImageView *imgHideShow;
    SlidingViewController* slideViewMenu;
    
}
@property (weak, nonatomic) IBOutlet UIWebView *webView;

- (IBAction)Action_Back:(id)sender;

@end
