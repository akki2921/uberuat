//
//  noLOginWebViewViewController.m
//  Uber Reward
//
//  Created by 9Dim on 21/03/18.
//  Copyright © 2018 Arvind Seth. All rights reserved.
//

#import "noLOginWebViewViewController.h"

@interface noLOginWebViewViewController ()

@end

@implementation noLOginWebViewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    if ([[UtillClass sharedInstance] checkNetworkReachability] == NO) {
        if ([[[UtillClass sharedInstance] getcultureID] isEqualToString:@"1033"]) {
            [[UtillClass sharedInstance] showerrortoastmsg:@"Please check your Internet Connection."];
        }
        else{
            [[UtillClass sharedInstance] showerrortoastmsg:@"الرجاء التحقق من اتصال الانترنت الخاص بك."];
        };
        
    }
    else{
    
       // [[UtillClass sharedInstance] ShowLoaderOn:[self parentViewController] withtext:@"Please wait..."];
       // [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
        
        NSString*str= [[NSUserDefaults standardUserDefaults] objectForKey:@"applanguage"];
        if ([str isEqualToString:@"English"]) {
            imgHideShow.hidden = YES;
        }
        else{
            imgHideShow.hidden = NO;
        }
        
        NSURL *targetURL = [NSURL URLWithString:@"https://www.google.co.in"];
        
        
        if ([app.ViewWebTypeStr isEqualToString:@"FAQs"]) {
            if ([str isEqualToString:@"English"]) {
                targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@Account/ContentPageMobile/70",[[UtillClass sharedInstance] getwebviewURL]]];
            }
            else{
                targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@Account/ContentPageMobile/72",[[UtillClass sharedInstance] getwebviewURL]]];
            }
        }
        else if ([app.ViewWebTypeStr isEqualToString:@"Welcome"]) {
            if ([str isEqualToString:@"English"]) {
                targetURL = [NSURL URLWithString:@"https://www.uber.com/a/join?exp=70801c"];
            }
            else{
                targetURL = [NSURL URLWithString:@"https://www.uber.com/a/join?exp=70801c"];
            }
        }
        else if ([app.ViewWebTypeStr isEqualToString:@"About"]) {
            if ([str isEqualToString:@"English"]) {
                targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@Account/ContentPageMobile/74",[[UtillClass sharedInstance] getwebviewURL]]];
            }
            else{
                targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@Account/ContentPageMobile/76",[[UtillClass sharedInstance] getwebviewURL]]];
            }
        } else {
            if ([str isEqualToString:@"English"]) {
                targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@Account/ContentPageMobile/65",[[UtillClass sharedInstance] getwebviewURL]]];            }
            else{
                targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@Account/ContentPageMobile/67",[[UtillClass sharedInstance] getwebviewURL]]];
            }
        }
        
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:targetURL cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15.0];
        [_webView loadRequest:theRequest];
        
        
        [_webView loadRequest:theRequest];
        
    }
    
    // Do any additional setup after loading the view.
}
- (IBAction)select_side_menu:(id)sender {
    slideViewMenu.delegate =self;
    if (slideViewMenu.view) {
        
        [self HideSlideMenu];
        
    } else {
        slideViewMenu = [[UtillClass sharedInstance] CallSlideMenuView:self yVal:0.0f];
        
        //slideViewMenu.Str_isLogIn = @"noLogin";
        
        
    }
}


#pragma mark - slideView Delegate

-(void) slideViewControllerDone:(SlidingViewController *)sender
{
    if (sender) {
        [self HideSlideMenu];
    }
}
-(void) HideSlideMenu
{
    [UIView animateWithDuration:0.50 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        //code with animation
        // slideViewMenu.view.frame = CGRectMake(-self.view.frame.size.width, 0.0f, self.view.frame.size.width, self.view.frame.size.height - 0.0f);
        [slideViewMenu.view removeFromSuperview];
    } completion:^(BOOL finished) {
        //code for completion
        [slideViewMenu.view removeFromSuperview];
        slideViewMenu = nil;
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}
- (void)webViewDidStartLoad:(UIWebView *)webView{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{ [MBProgressHUD hideHUDForView:self.view animated:YES];
    //[[UtillClass sharedInstance]HideLoader];
    //[[UtillClass sharedInstance]showerrortoastmsg:@"Something went wrong, please retry!!"];
}

- (IBAction)Action_Back:(id)sender {
    [self dismissViewControllerAnimated:false completion:nil];
}

@end
